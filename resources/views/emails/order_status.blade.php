<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}

    .social-media-icons{
        margin: 5px;
    }

    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
    <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>

<body>
    <div class="es-wrapper-color">
        <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#555555"></v:fill>
			</v:background>
		<![endif]-->
<table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="esd-email-paddings" valign="top">
<table cellpadding="0" cellspacing="0" class="es-content esd-header-popover" align="center">
<tbody>
<tr>
    <td class="esd-stripe" align="center" esd-custom-block-id="88701">
        <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
            <tbody>
                <tr>
                    <td class="esd-structure es-p5b es-p10r es-p10l" esd-general-paddings-checked="false" align="left">
                        <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="280" valign="top"><![endif]-->
                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td class="es-m-p0r es-m-p20b esd-container-frame" width="280" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-infoblock" align="left">
                                                        {{-- <p>Put your preheader text here</p> --}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td><td width="20"></td><td width="280" valign="top"><![endif]-->
                        <table cellspacing="0" cellpadding="0" align="right">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="280" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    {{--  <td align="right" class="esd-block-text es-infoblock">
                                                        <p><a href="https://viewstripo.email" target="_blank" class="view" style="line-height: 150%;">SEE THIS EMAIL ONLINE</a></p>
                                                    </td>  --}}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<table class="es-content" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
    <td class="esd-stripe" align="center">
        <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" align="center">
            <tbody>
                <tr>
                    <td class="esd-structure es-p20t es-p20b es-p10r es-p10l" style="background-color: #ffffff;" esd-general-paddings-checked="false" bgcolor="#191919" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="580" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-image" align="center" style="font-size:0"><a target="_blank" href="{{url('/')}} "><img class="adapt-img" src="{{asset('assets/images/logo.png')}}"  alt width="105"></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="esd-structure es-p20t es-p20b es-p20r es-p20l" esd-general-paddings-checked="false" style="background-color: #e7e4da;" bgcolor="#ffcc99" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-p15t es-p15b" align="center">
                                                        <div class="esd-text">
                                                            <h2 style="color: #242424;"><span style="font-size:30px;">
                                                                @if(isset($order))
                                                                <strong>Your order is {{ucwords($order->status) }}. </strong></span>
                                                                @endif
                                                                <br></h2>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text es-p10l" align="center">
                                                        <p style="color: #242424;">
                                                            {!! $msg !!}    
                                                        </p>
                                                    </td>
                                                </tr>
                                                {{--  <tr>
                                                    <td class="esd-block-button es-p15t es-p15b es-p10r es-p10l" align="center"><span class="es-button-border" style="border-radius: 20px; background: #191919 none repeat scroll 0% 0%; border-style: solid; border-color: #2cb543; border-width: 0px;"><a href="https://viewstripo.email/" class="es-button" target="_blank" style="border-radius: 20px; font-family: lucida sans unicode,lucida grande,sans-serif; font-weight: normal; font-size: 18px; border-width: 10px 35px; background: #191919 none repeat scroll 0% 0%; border-color: #191919; color: #ffffff;">View your order details</a></span></td>
                                                </tr>  --}}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="esd-structure es-p15t es-p10b es-p10r es-p10l" style="background-color: #f8f8f8;" esd-general-paddings-checked="false" bgcolor="#f8f8f8" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="580" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text" align="center">
                                                        <h2 style="color: #191919;">Items ordered<br></h2>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                @if(isset($order))
                @foreach($order->carts as $cart)
                <tr style="border-bottom: 1px solid black">
                    <td class="esd-structure es-p25t es-p5b es-p20r es-p20l" esd-general-paddings-checked="false" style="background-color: #f8f8f8; " bgcolor="#f8f8f8" align="left">
                        <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]-->
                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr  >
                                    <td class="es-m-p20b esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    {{--  min-height: 200px; display: flex; justify-content: center; align-items: center  --}}
                                                    <td class="esd-block-image es-infoblock made_with" align="center" style="font-size:0"><a target="_blank" href="{{url('/')}}"><img src="{{ asset('/files/' . $cart->image) }}" alt style="max-height: 180px; max-width:120px; width:auto;  display: flex; justify-content: center; align-items: center" ></a></td>
                                                    {{-- <td class="" align="center" style="font-size:0;"><a target="_blank" href="{{url('/')}}"><img class="" src="{{ asset('/files/'.$cart->image)}}" alt width="auto" style="max-height: 180px;  display: flex; justify-content: center; align-items: center"></a></td> --}}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]-->
                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text" align="left">
                                                        <p><span style="font-size:16px;"><strong style="line-height: 150%;">{{$cart->name}}</strong></span></p>
                                                        <p>Description: {{$cart->description}} </p>
                                                        {{--  <p>Color: Space Gray</p>  --}}
                                                        {{--  <p>New iSight 12-megapixel camera</p>  --}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text es-p20t" align="left">
                                                        <p><span style="font-size:15px;"><strong style="line-height: 150%;">Item Price:</strong> QAR {{$cart->price}}</span></p>
                                                        <p><span style="font-size:15px;"><strong>Qty: </strong>{{$cart->quantity}}</span></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                </tr>
                
                @endforeach
                <tr>
                    <td class="esd-structure es-p10t es-p10b es-p10r es-p10l" style="background-color: #f8f8f8;" esd-general-paddings-checked="false" bgcolor="#f8f8f8" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="580" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-spacer es-p20t es-p20b es-p10r es-p10l" bgcolor="#f8f8f8" align="center" style="font-size:0">
                                                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-bottom: 1px solid #191919; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; height: 1px; width: 100%; margin: 0px;"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text es-p15b" align="center">
                                                        <table class="cke_show_border" width="240" height="101" cellspacing="1" cellpadding="1" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td><strong>Subtotal:</strong></td>
                                                                    <td style="text-align: right;">QAR {{  array_sum ($order->carts->pluck('price')->toArray()) }}</td>
                                                                </tr>
                                                                <tr>

                                                                    @php
                                                                        $shipping = App\shippings::first();
                                                                    @endphp
                                                                    <td><strong>Shipping:</strong></td>
                                                                    <td style="text-align: right;">QAR {{ isset($shipping) ? $shipping->fee : ''}}</td>
                                                                </tr>
                                                                {{--  <tr>
                                                                    <td><strong>Sales Tax:</strong></td>
                                                                    <td style="text-align: right;">$1.00</td>
                                                                </tr>  --}}
                                                                <tr>
                                                                    <td><span style="font-size: 18px; line-height: 200%;"><strong>Order Total:</strong></span></td>
                                                                    <td style="text-align: right;"><span style="font-size: 18px; line-height: 200%;"><strong>QAR {{$order->total_price}}</strong></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="esd-structure es-p15t es-p10b es-p10r es-p10l" style="background-color: #eeeeee;" esd-general-paddings-checked="false" bgcolor="#eeeeee" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="580" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text" align="center">
                                                        <h2 style="color: #191919;">Order & shipping info</h2>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style="padding:20px" >
                    <td class="esd-structure es-p10t es-p30b es-p20r es-p20l" esd-general-paddings-checked="false" style="background-color: #eeeeee;" bgcolor="#eeeeee" align="left">
                        <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]-->
                        <table style="margin-left: 20px" class="es-left" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                    <td class="es-m-p20b esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-p10t es-p10b" align="left">
                                                        <h3 style="color: #242424;">Order details</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text" align="left">
                                                        <p><strong>Order №:</strong> {{ $order->id }}</p>
                                                        {{--  <p><strong>Member №:</strong> 213983</p>  --}}
                                                        <p><strong>Shipping Method:</strong> Standard</p>
                                                        <p><strong>Order date:</strong> {{$order->created_at->toDateString()}}</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]-->
                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-p10t es-p10b" align="left">
                                                        <h3 style="color: #242424;">Shipping Address</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text" align="left">

                                                          

                                                        {{--  @dd($order->customer)  --}}
                                                        <p>{{$order->customer->full_name}}<strong></strong></p>

                                                        <p>Building: {{ $order->customer->building }},</p>
                                                        <p>Street: {{ $order->customer->street }},</p>
                                                        <p>Zone: {{ $order->customer->zone }},</p>
                                                        <p>{{ $order->customer->address }}</p>
                                                        
                                                        {{--  <p>96 Wiilmedrie St</p>
                                                        <p>San jose, CA 95987<br></p>
                                                        <p>Estimated delivery: 04/10/16 - 04/14/16<br></p>  --}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                </tr>
                @endif
                @php
                $contact = App\contactuses::first();
                @endphp
                @if(isset($contact))
                <tr>
                    <td class="esd-structure es-p25t es-p30b es-p20r es-p20l" esd-general-paddings-checked="false" style="background-color: #f8f8f8;" bgcolor="#f8f8f8" align="left">
                        <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]-->
                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                            <tbody>
                                <tr>
                                   
                                    <td class="es-m-p20b esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-p10b" align="center">
                                                        <h3 style="color: #242424;">We are here to help</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text" align="center">
                                                        <p style="line-height: 150%; color: #242424;">Call <a target="_blank" style="line-height: 150%; " href="{{url('/')}}">{{$contact->phone}}</a> or <a target="_blank" href="{{url('/')}}">visit us online</a></p>
                                                        <p style="line-height: 150%; color: #242424;">for expert assistance.</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]-->
                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="270" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-text es-p10b" align="center">
                                                        <h3 style="color: #242424;">Our guarantee</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text" align="center">
                                                        <p style="line-height: 150%; color: #242424;">Your satisfaction is 100% guaranteed.</p>
                                                        <p style="line-height: 150%; color: #242424;">See our <a target="_blank" href="{{url('/')}}">Returns and Exchanges policy.</a></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<table cellpadding="0" cellspacing="0" class="es-footer" align="center">
<tbody>
<tr>
    <td class="esd-stripe" align="center" esd-custom-block-id="88703">
        <table class="es-footer-body" width="600" cellspacing="0" cellpadding="0" align="center">
            <tbody>
                <tr>
                    <td class="esd-structure es-p20" esd-general-paddings-checked="false" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-social es-p10t es-p20b" align="center" style="font-size:0">
                                                        <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" style="margin-top: 10px" >
                                                            <tbody>
                                                                <tr>
                                                                    <td class="es-p15r " valign="top" align="center"><a href class="social-media-icons" style="margin : 5px"><img title="Twitter" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/twitter-circle-gray.png" alt="Tw" width="32" height="32"></a></td>
                                                                    <td class="es-p15r" valign="top" align="center"><a href  class="social-media-icons"  style="margin : 5px"> <img title="Facebook" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/facebook-circle-gray.png" alt="Fb" width="32" height="32"></a></td>
                                                                    <td class="es-p15r" valign="top" align="center"><a href  class="social-media-icons"  style="margin : 5px"><img title="Youtube" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/youtube-circle-gray.png" alt="Yt" width="32" height="32"></a></td>
                                                                    <td valign="top" align="center"><a href  class="social-media-icons" style="margin : 5px"><img title="Linkedin" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/linkedin-circle-gray.png" alt="In" width="32" height="32"></a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-block-text" align="center">
                                                        <p><strong><a target="_blank" style="line-height: 150%;" href="{{route('product')}}">Browse all products</a>&nbsp;</strong>•<strong></strong></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="esd-block-text es-p20t es-p20b">
                                                        <p style="line-height: 120%;">Electro, Inc.</p>
                                                        <p style="line-height: 120%;">62 N. Gilbert, CA 99999</p>
                                                        <p style="line-height: 120%;"><a target="_blank" style="line-height: 120%;" href="tel:{{$contact->phone}}">{{$contact->phone}}</a></p>
                                                        <p style="line-height: 120%;"><a target="_blank" href="mailto:{{$contact->email}}" style="line-height: 120%;">{{$contact->email}}</a></p>
                                                    </td>
                                                </tr>
                                                {{--  <tr>
                                                    <td align="center" class="esd-block-text">
                                                        <p><strong><a target="_blank" style="line-height: 150%;" class="unsubscribe" href>Unsubscribe</a> • <a target="_blank" style="line-height: 150%;" href="https://viewstripo.email">Update Preferences</a> • <a target="_blank" style="line-height: 150%;" href="https://viewstripo.email">Customer Support</a></strong></p>
                                                    </td>
                                                </tr>  --}}
                                                <tr>
                                                    <td class="esd-block-text es-p10t es-p10b" align="center">
                                                        <p><em><span style="font-size: 11px; line-height: 150%;">You are receiving this email because you have purchased products from our site</span></em></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
<table class="esd-footer-popover es-content" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
    <td class="esd-stripe" align="center">
        <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
            <tbody>
                <tr>
                    <td class="esd-structure es-p30t es-p30b es-p20r es-p20l" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-block-image es-infoblock made_with" align="center" style="font-size:0"><a target="_blank" href="{{url('/')}}"><img src="{{asset('assets/images/logo.png')}}" alt width="125"></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</body>

</html>

