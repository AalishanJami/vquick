<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>



    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #666666;
          color: white;
        }

        .fafoo{
            margin:10px;
            text-decoration: none;
            color: #666666;
        }
        .text-dark{
            text-decoration: none;
        }
        </style>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>

    

    <div style="width: 100%; text-align: center; margin-top:20px; ">

    <img src="{{asset('assets/images/logo.png')}}" style="width: auto; height: 50px;" alt="logo "/>

    </div>

    
    <div style="width: 80%; margin-left: 10%; margin-top:50px; min-height: 340px; " >
        
        {!! $msg !!}

        {{--  @dd($order)  --}}
        @if(isset($order))

        <h3>Order Details</h3>
        
        <table id="customers" style="margin-top: 20px; margin-bottom: 40px;" >
        <tr>
          <th>Product Name</th>
          <th>Qunatity</th>
          <th>Price</th>
        </tr>
        @foreach($order->carts as $cart)
        <tr @if($loop->last) style="border-bottom: 2px solid #666666;" @endif  >
          <td>{{$cart->name}}</td>
          <td>{{$cart->quantity}} </td>
          <td>{{$cart->price}} </td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td style="text-align: right" > Sub Total </td>
            <td>QAR: {{ array_sum ($order->carts->pluck('price')->toArray()) }} </td>
          </tr>
        @if(isset($shipping))
        <tr>
            <td></td>
            <td style="text-align: right" >Shipping Charges </td>
            <td>QAR: {{$shipping->fee}} </td>
          </tr>
        @endif
          
          <tr>
            <td></td>
            <td style="text-align: right" >Grand Price </td>
            <td>QAR: {{$order->total_price}} </td>
          </tr>
      
      </table>

      <a href="{{route('orderstatuses' , $order->id)}}">View Now</a>
      @endif
    </div>



    <hr>
    <div style="width: 100%; text-align: center ">

    <div class="">
        <div class="row">
          <div class="col-md-12 text-center">
      <img src="{{asset('assets/images/vv.jpg')}}" class="img-fluid pb-3" style="width: 80px;" alt="">
            
            <div class="text-center mt-2">
             <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i>
      
       </a>          <a class="text-dark" href="">
      <i class="fa fa-instagram fafoo"></i>
       </a> 
                <a class="text-dark" href="">
      <i class="fa fa-twitter fafoo"></i>
       </a>      </div>
            <h4 class="text-center mt-3 fh4  color_grey">&copy; vequcick.com,Inc. <br> All rights reserved.Privacy Policy</h4>
          </div>
        </div>
      </div>
    </div>



</body>
</html>

