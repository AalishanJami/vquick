@extends('regusers.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New Customer</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('regusers.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  

  <form action="{{ route('regusers.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Email:</strong>
          <input type="text" class="form-control" name="email" placeholder="Enter Email" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Password:</strong>
          <input type="password" class="form-control" placeholder="Enter Password" name="password" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Name:</strong>
          <input type="text" class="form-control" name="full_name" placeholder="Enter Name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Mobile:</strong>
          <input type="text" class="form-control" name="phone" placeholder="Enter Mobile" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Address:</strong>
          <input type="text" class="form-control" name="address" placeholder="Enter Address" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Zone:</strong>
          <input type="text" class="form-control" placeholder="Enter Zone No" name="zone" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Street:</strong>
          <input type="text" class="form-control" placeholder="Enter Street No" name="street" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Bilding:</strong>
          <input type="text" class="form-control" name="building" placeholder="Enter Building No" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>
  @endsection