@extends('orderstatuses.layout') 
@section('content')


<style>
  .badge-danger{
    background-color: red;
    color:white;
  }
  .badge-success{

  }
  .badge-primary{

  }
  .badge-warning{

  }
</style>


    <?php use App\regusers;?>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table class="table table-bordered">
        <tr>
          <th>Order Number</th>
          <th>Total Price</th>
          <th>Payment Status</th>
          <th>Status</th>
          <th>Customer Name</th>
          <th>Address</th>
          <th>Date Time</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($customer->orders->sortByDesc('id') as $orderstatus)
        <tr>
          <td>{{ $orderstatus->id }}</td>
          <td>{{ $orderstatus->total_price }}</td>
          <td>{{ $orderstatus->payment_method }}</td>
          <td>
            @if($orderstatus->status == 'pending')
            <span class="label label-danger">Pending</span>
            
            @elseif($orderstatus->status == 'confirmed')

            <span class="label label-primary">Confirmed</span>
            
            @elseif($orderstatus->status == 'out of delivery')
            
            <span class="label label-warning">Out Of Delivery</span>
            
            @elseif($orderstatus->status == 'delivered')
            
            <span class="label label-success">Delivered</span>

            @elseif($orderstatus->status == 'cancelled')
            
            <span class="label label-danger">Cancelled</span>
            
            @endif
          
          </td>
          <?php $cid=$orderstatus->cid;
          $regusers = regusers::all()->where('id',$cid); ?>
          @foreach ($regusers as $reguser)
          <?php $cname=$reguser->full_name;
          $caddress=$reguser->address;
          $czone=$reguser->zone;
          $cstreet=$reguser->street;
          $cbuilding=$reguser->building;
          $cdate=$reguser->created_at; ?>
          @endforeach
          <td>{{ $cname }}</td>
          <td>Building: {{ $cbuilding }}, Street: {{ $cstreet }}, Zone: {{ $czone }}, {{ $caddress }} </td>
          <td>{{ $orderstatus->created_at }}</td>
          <td>
            <form id="delete-form-{{$orderstatus->id}}" action="{{ route('orderstatuses.destroy',$orderstatus->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('orderstatuses.show',$orderstatus->id) }}">View</a>
              <a class="btn btn-primary" href="{{ route('orderstatuses.edit',$orderstatus->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$orderstatus->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endforeach
    </table>

  

@endsection