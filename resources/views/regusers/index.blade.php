@extends('regusers.layout') 

@section('content')

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<form action="{{route('filter-users')}}" method="GET">

  <div class="col-12" style="display:grid;grid-template-columns:25% 25% 25% 20%;grid-column-gap:3%;">
      <div class="form-group mt-2">
        <input type="text" class="form-control" name="name" placeholder="Name"  @if(isset($filters) && isset($filters['name']) ) value="{{$filters['name']}}" @endif />
      </div>
      <div class="form-group mt-2">
        <input type="text" name="phone" class="form-control" placeholder="Phone" @if(isset($filters) && isset($filters['phone']) ) value="{{$filters['phone']}}" @endif  />
      </div>
      
      <div class="form-group mt-2">
        <input type="email" name="email" class="form-control" placeholder="Email" @if(isset($filters) && isset($filters['email']) ) value="{{$filters['email']}}" @endif  />
      </div>
      
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Search</button>
        <a class="btn btn-danger" href="{{route('regusers.index')}}">X Clear</a>
      </div>
      <div class="form-group">
      </div>
    </div>
  </form>

  @php
    if(isset($filtered_regusers)){
      $regusers = $filtered_regusers;
    }
@endphp

<div class="row">
        <div class="col-lg-12 margin-tb mb-3">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('export-customers' , json_encode ( $regusers->pluck('id')->toArray()) ) }}"> Export</a>
                <a class="btn btn-success ml-2" href="{{ route('regusers.create') }}"> Create New Customer</a>
            </div>
        </div>
      
       
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bregusered">
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Email</th>
          <th>Mobile Number</th>
          <th>Address</th>
          <th>Zone</th>
          <th>Street</th>
          <th>Building</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($regusers as $k => $reguser)
        <tr>
          <td>{{ $k+1 }}</td>
          <td>{{ $reguser->full_name }}</td>
          <td> {{ $reguser->email }} </td>
          <td>{{ $reguser->phone }}</td>
          <td>{{ $reguser->address }}</td>
          <td>{{ $reguser->zone }}</td>
          <td>{{ $reguser->street }}</td>
          <td>{{ $reguser->building }}</td>
          <td>
            <a href="javascript:;"onclick="ask_delete({{$reguser->id}})" class="text-danger "> <i class="fa fa-trash fa-lg"></i> </a>

            <a href="{{ route('regusers.edit',$reguser->id) }}" class=""> <i class="fa fa-edit fa-lg"></i> </a>

            <form style="display: inline" id="delete-form-{{$reguser->id}}" action="{{ route('regusers.destroy',$reguser->id) }}" method="POST">
              <a class="btn btn-info btn-sm" href="{{ route('regusers.show',$reguser->id) }}">Show</a>
              {{-- <a class="btn btn-primary" href="{{ route('regusers.edit',$reguser->id) }}">Edit</a> --}}
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}

            </form>
            <a href="{{route('reguser-orders' , $reguser->id )}}"  class="btn btn-success btn-sm">Orders </a>



          </td>
        </tr>
        @endforeach
    </table>

</div>




<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

    @if(
      $regusers instanceof \Illuminate\Pagination\Paginator ||
      $regusers instanceof Illuminate\Pagination\LengthAwarePaginator
    )
  
      {{$regusers->links() ?? ''}}
  @endif
  

 
      
@endsection