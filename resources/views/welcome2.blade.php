<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>VQuick</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

      <!-- Styles -->
      <style>
         html, body {
               background-color: #fff;
               color: #636b6f;
               font-family: 'Nunito', sans-serif;
               font-weight: 200;
               height: 100vh;
               margin: 0;
         }

         .full-height {
               height: 100vh;
         }

         .flex-center {
               align-items: center;
               display: flex;
               justify-content: center;
         }

         .position-ref {
               position: relative;
         }

         .top-right {
               position: absolute;
               right: 10px;
               top: 18px;
         }

         .content {
               text-align: center;
         }

         .title {
               font-size: 84px;
         }

         .links > a {
               color: #636b6f;
               padding: 0 25px;
               font-size: 13px;
               font-weight: 600;
               letter-spacing: .1rem;
               text-decoration: none;
               text-transform: uppercase;
         }

         .m-b-md {
               margin-bottom: 30px;
         }

         .img-fluid {
            width: 340px !important;
            height: 330px !important;
         }
         .view-category-form-button{
            cursor: pointer;
         }

         @media only screen and (max-width: 500px){
         .mobile-view-gallery{
            display: none;
         } }
      </style>
        
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- StyleSheet -->
      <link rel="stylesheet" href="assets/css/style.css">
      <!-- GoogleFonts -->
      <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">
      <!-- Slick slider -->
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
      <!-- Link Swiper s CSS -->
      <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	  <title>Home</title>
	  <link rel="stylesheet" href="assets/css/own-style.css">
   
   </head>
   <body>
      <!-- Header -->    
      <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
                	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a>  
			   <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
               <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
               </li>
               <li class="li2 " style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
               <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
               </li>
               <li class="li2 font-weight-bold ml-1 mr-3" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}">
                  <i class="fa fa-history" aria-hidden="true"></i></span></a>
               </li>
               <?php if($_SESSION['language'] == 'arabic') { ?>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?english">EN</a></li>

               
               <?php }
               else { ?>
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?arabic">AR</a></li>
               <?php } ?>
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2  mr-3" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>
               <?php if($_SESSION['language'] == 'arabic') { ?>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?english">EN</a></li>
               <?php }
               else { ?>
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?arabic">AR</a></li>
               <?php } ?></ul>
            <?php } ?>
         </nav>
      </div>
      <!-- Navbar -->
      
	  <!-- navbar mbl view -->

<div class="container py-2 sideeen">
   <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a class="sidea" href="{{ route ('product')}}">Products</a>
      <a class="sidea" href="{{ route ('about')}}">About</a>
      <a class="sidea" href="{{ route ('contact')}}">Gallery</a>
      <a class="sidea" href="{{ route ('contact')}}">Contact Us</a>
   </div>
<div class="row">
   <div id="sidebar">
      <span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
         <div class="bar"></div>
         <div class="bar"></div>
         <div class="bar"></div>
      </span>
   </div>
<div  id="Logoleft">
<a href="/">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="    position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{route('userlogin')}}">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="{{route('checkout')}}">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>
	<!-- navbar mbl view ends-->
	  
<div class="container my-5 index-mobile-view">
<div class="row">
	<div class="col-lg-12">
	<h1 class="font-weight-bold text-uppercase text-center our-product" >Our Products</h1>
	<h1 class="font-weight-bold text-uppercase our-product-mobile" >Our Products</h1>
	</div>
   <?php use App\galleries;
   $galleries = galleries::all(); ?>
	<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 mobile-view-gallery ">
	@foreach ($galleries as $gallery)
      <img src="files/{{ $gallery->image}}" style="width: 100%;border-radius: 20px;" class="img-fluid" alt="">
   @endforeach
	<div class="row ">
      <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
         <h3 class="slh3 mt-3 ">Colombia</h3>
         <p class="slp3 mt-2 ">If you are going to use a passage of Lorem Ipsum,You need to be sure .</p>
         <h3 class="slh4 mt-3  price-product" ><b>250 QAR</b></h3>
      </div>
	<div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
	<div class='counter1'  style="margin-top:1px;">
	<div class='down' onclick='decreaseCount(event, this)'>-</div>
	<input type='text' class="counter_1" value='1'  >
	<div class='up' onclick='increaseCount(event, this)'>+</div>
	</div>
	</div>
	<div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >
	<button class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
	</div>
	</div>
	</div>


   @php
   
   $productsection = App\productsections::first(); 
   $products = App\products::all()->where('is_approved', '1');
   $categories = App\categories::all();
   @endphp
   {{--  mobile products here  --}}
   @if($productsection->content == 'product' )
   @foreach ($products->take(3) as $product)

	<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">
      <img src="assets/images/banner1.PNG" style="width: 100%;border-radius: 20px;" class="img-fluid" alt="">
      <div class="row ">
         <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
         <h3 class="slh3 mt-3 "> {{$_SESSION['language'] == 'english' ? $product->name : $product->arabic_name }} </h3>
         <p class="slp3 mt-2 ">{{$_SESSION['language'] == 'english' ? Str::limit($product->description , 200) : Str::limit($product->description_ar , 200) }} .</p>
         <h3 class="slh4 mt-3  price-product" ><b>{{$product->price}} QAR</b></h3>
      </div>
      
      <form class="" style="width: 100%; display: contents; " method="get" action="/carts">
         <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
             <div class='counter1'  style="margin-top:1px;">
               <div class='down' onclick='decreaseCount(event, this)'>-</div>
               <input type='text' id="quantity" name="quantity" class="counter_1" value='1'  >
               <div class='up' onclick='increaseCount(event, this)'>+</div>
             </div>
           </div>
           <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
             <input type="hidden" name="category" value="{{ $product->category }}" >
             <input type="hidden" name="image" value="{{ $product->image }}" >
             <input type="hidden" name="pid" value="{{ $product->id }}" >
             <input type="hidden" name="id" value="{{ $id ?? null }}" >
             <input type="hidden" name="name" value="{{ $product->name }}" >
             <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
             <input type="hidden" name="description" value="{{ $product->description }}" >
             <input type="hidden" name="price" value="{{ $product->price }}" >
             <button type="submit" name="submit" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
           </div>
         </form>
   
	</div>
   </div>
   @endforeach
   
   @else
   @foreach ($categories->take(3) as $category)

	<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">

      <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
         <img src="assets/images/banner1.PNG" style="width: 100%;border-radius: 20px;" class="img-fluid" alt=""> </a>
         
         <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
           <input type="hidden" name="category" value="{{ $category->name}}">
           <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
               </div>
           </form>

      <div class="row ">
         <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
         <h3 class="slh3 mt-3 "> {{$_SESSION['language'] == 'english' ? $category->name : $category->arabic_name }} </h3>
         <p class="slp3 mt-2 ">{{$_SESSION['language'] == 'english' ? Str::limit($category->description , 200) : Str::limit($category->description_ar , 200) }} .</p>
	   </div>
	
	</div>
   </div>
   @endforeach
   @endif
{{--  
	<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">
	<img src="assets/images/banner1.PNG" style="width: 100%;border-radius: 20px;" class="img-fluid" alt="">
	<div class="row ">
	<div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
	<h3 class="slh3 mt-3 ">Colombia</h3>
	<p class="slp3 mt-2 ">If you are going to use a passage of Lorem Ipsum,You need to be sure .</p>
	<h3 class="slh4 mt-3  price-product" ><b>250 QAR</b></h3>
	</div>
	<div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
	<div class='counter1'  style="margin-top:1px;">
	<div class='down' onclick='decreaseCount(event, this)'>-</div>
	<input type='text' class="counter_1" value='1'  >
	<div class='up' onclick='increaseCount(event, this)'>+</div>
	</div>
	</div>
	<div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >
	<button class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
	</div>
	</div>
	</div>  --}}

	
	<div class="col-lg-12">
      @if(isset($categories) && $categories->count() > 3 || isset($products) && $products->count() > 3 )
      <button type="button" onclick="window.location='/product';" class="btn-block btn-sli py-3  text-white show-more-home-mobile" id="imgwidth" style="color: #eee2da;"> SHOW MORE</button>
      @endif
	</div>

</div>
</div>

<!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->
<div class="container mb-3 mt-5 chotip border_top">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>	  
	  
	  <!-- navbar mbl view -->
      <!-- Header -->
      <!-- Banner -->
      <section class="web">
         <div class="container-fluid  mt-3">
           <div class="row ">
              <div class="col-md-6 col-sm-12">
                 <img src="assets/images/banner1.PNG" class="imgban" alt="">
              </div>
              <div class="col-md-6 col-sm-12 ban1">
                 <div style="margin-left: 140px;">
                    <h1 class="banh1" >May the coffee <br> be with you</h1>
                    <p class="banp1">If you are using a passage of Lorem Ipsum,You need to be sure <br> . </p>
                    <a href="{{url('/about')}} " class="btn-ban1 px-5 py-3" id="know-more" >KNOW MORE</a>
                 </div>
              </div>
           </div>
           <!-- Banner -->
           <div class="container mt-5 mb-5">
              <div class="row">
                 <div class="col-lg-12">
                 <?php use App\productsections;
                  $productsections = productsections::all(); 
                  $i1=0;?>
                  @foreach ($productsections as $productsection)
                     <?php $i1++; ?>
                  @endforeach
                  <?php
                  if($i1==0) { ?>
                     <h1 class="font-weight-bold text-uppercase text-center our-product" >Our Products</h1>
                  <?php }
                  else { ?>
                  @foreach ($productsections as $productsection)
                     <h1 class="font-weight-bold text-uppercase text-center our-product" > {{ $productsection->title }} </h1>
                  @endforeach
                  <?php }?>
                 </div>
              </div>
           </div>
           <!-- Swiper -->
           <div class="container mt-5">
           <div class="swiper-container" style="z-index: 1; max-width: 100%;">
              <div class="swiper-wrapper">
                
                 
                 
                  <!-- slide -->
                  
                  <?php use App\categories;
                  $categories = categories::all();
                  use App\products;
                  $products = products::all()->where('is_approved', '1'); ?>
                  @foreach ($productsections as $productsection)
                  <?php if($productsection->content == 'product') { ?>
                     @foreach ($products as $product)
                     {{--  products catalog  --}}
                     <div class="swiper-slide">
                           <div>
                              <img src="files/{{ $product->image}}" class="img-fluid" alt="">
                              <div class="row" style="padding: 0 ;background-color: transparent;" >
                                 <div class="col-ms-12 px-3" style="background-color: transparent;padding: 0!important ;">
                                  
                                    
                                    <h3 class="slh3 mt-3 text-left">@if($_SESSION['language'] == 'english') {{ $product->name}} @else {{ $product->arabic_name}} @endif </h3>

                                    <p class="slp3 mt-2 text-left" style="height: 60px;width: 325px;"> {{ $_SESSION['language'] == 'english' ? substr($product->description,0,100)  : substr($product->description_ar,0,100)  }} ...</p>
                                    <h3 class="slh3 mt-3 text-left" style="margin-bottom: 31px;">{{ $product->price}} QAR</h3>
                                 </div>
                                 
                                 <form class="" style="width: 100%; display: contents; " method="get" action="/carts">
                                    <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
                                        <div class='counter1'  style="margin-top:1px;">
                                          <div class='down' onclick='decreaseCount(event, this)'>-</div>
                                          <input type='text' id="quantity" name="quantity" class="counter_1" value='1'  >
                                          <div class='up' onclick='increaseCount(event, this)'>+</div>
                                        </div>
                                      </div>
                                      <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
                                        <input type="hidden" name="category" value="{{ $product->category }}" >
                                        <input type="hidden" name="image" value="{{ $product->image }}" >
                                        <input type="hidden" name="pid" value="{{ $product->id }}" >
                                        <input type="hidden" name="id" value="{{ $id ?? null }}" >
                                        <input type="hidden" name="name" value="{{ $product->name }}" >
                                        <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
                                        <input type="hidden" name="description" value="{{ $product->description }}" >
                                        <input type="hidden" name="price" value="{{ $product->price }}" >
                                        <button type="submit" name="submit" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
                                      </div>
                                    </form>
                                 {{--  <?php }
                                 else { ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 counter-div" >
                                       <div class='counter'>
                                          <div class='down' onclick='decreaseCount(event, this)'>-</div>
                                          <input type='text' value='1' class="counter">
                                          <div class='up' onclick='increaseCount(event, this)'>+</div>
                                       </div>
                                    </div>
                                    <div class="col-lg-8  col-md-7 col-sm-7 col-7 product-button" >  
                                       <button onclick="myFunction()" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
                                    </form>
                                 </div>
                                 <?php } ?>  --}}
                              </div>                       
                           </div>
                     </div>
                     @endforeach
                  <?php }
                  else { ?>
                     @foreach ($categories as $category)

                     <div class="swiper-slide">
                        <div>

                           <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
                              <input type="hidden" name="category" value="{{ $category->name}}">
                              <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
                                  </div>
                              </form>

                           <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
                           <img src="files/{{ $category->image}}" class="img-fluid" alt="">
                           </a>
                           
                           <div class="row" style="padding: 0 ;background-color: transparent;" >
                              <div class="col-ms-12 px-3" style="background-color: transparent;padding: 0!important ;">
                               
                                 <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
                                 <h3 class="slh3 mt-3 text-left">@if($_SESSION['language'] == 'english') {{ $category->name}} @else {{ $category->arabic_name}} @endif </h3></a>
                                 
                                 <p class="slp3 mt-2 text-left" style="height: 60px;width: 325px;"> {{ $_SESSION['language'] == 'english' ? substr($category->description,0,100)  : substr($category->description_ar,0,100)  }} ...</p>
                              </div>
                              
                             
                           </div>                       
                        </div>
                  </div>
                     @endforeach
                  <?php } ?>
                  @endforeach
               </div>
              <!-- Add Arrows -->  
           </div>
         </div>
       </div>
       @foreach ($productsections as $productsection)
      <?php if($productsection->content == 'product') { ?>
       <div class="row">
          <div class="col-md-12 mgp" style="background-color: #e7e4da; margin-top: -366px;margin-bottom: 3.5%;">
				<div class="swiper-button-next" ><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
				<div class="swiper-button-prev" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>
          </div>
       </div>
       <?php }
       else { ?>
       <div class="row">
          <div class="col-md-12 mgp" style="background-color: #e7e4da; margin-top: -288px;margin-bottom: 0%;">
				<div class="swiper-button-next" ><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
				<div class="swiper-button-prev" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>
          </div>
       </div>
       <?php } ?>
         @endforeach
      </section>
      
	 
      <section class="web">
         <div class="container my-5">
            <section class="variable slider">
               <?php for($i=0;$i<4;$i++) { ?>
               @foreach ($galleries as $gallery)
                  <div>
                  <img src="files/{{ $gallery->image}}">
                  </div>
               @endforeach
               <?php } ?>
            </section>
         </div>

         <div class="container-fluid formdiv px-5 mb-3" id="contus">
            <div class="container">
            <?php use App\contactuses;
            $contactuses = contactuses::all();?>
            @foreach ($contactuses as $contactus)
               <?php $contact_email=$contactus->email;  ?>
            @endforeach
            <form action="/contact-form" method="POST">
               @csrf
               <div class="row">
                  <div class="col-md-3">
                        <div class="form-group">
                           <input type="text" name="name" class="form-control text-left inpf" placeholder="Name">
                        </div>
                  </div>
                  <div class="col-md-3">
                        <div class="form-group">
                           <input type="email" name="email" class="form-control text-left inpf" placeholder="Email Address">
                        </div>
                  </div>
                  <div class="col-md-3">
                        <div class="form-group">
                           <input type="text" name="message" class="form-control text-left inpf" placeholder="Message">
                        </div>
                  </div>
                  <div class="col-md-3">
                     <input type="hidden" value="<?= $contact_email ?? '' ?>" name="contact_email">
                     <button type="submit" name="submit" class="btn-block btn-sli py-2 px-5 text-white " style="width:60%; margin-left: 15%;">SEND</button>
                  </div>
               </div>
            </form>
            </div>
         </div>
      </section>
     
      <!-- footer -->
      <div class="container badip mb-3 mt-5">
      <div class="row">
         <div class="col-md-12">
            <h4 class="text-center fh4">&copy; 2020  vequick.com, Inc. All Rights Reserved.  <a class="p-p" href="#">Privacy Policy</a></h4>
            <div class="text-center mt-3">
               <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
               <a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
               </a>        
               <a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
               </a>      
            </div>
         </div>
      </div>
      </div>

    @include('layouts.partials.errors')

      <script>
         function myFunction() {
            swal("Please login to proceed with your order!");
            //window.location.href = "/userlogin";
         }
      </script>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script src="assets/slick/slick-master/slick/slick.js"></script>
      <!-- Swiper JS -->
      <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
      <!-- Initialize Swiper -->
      <script>
         $('.slider').slick({
         centerMode: true,
         centerPadding: '10px',
         slidesToShow: 3,
         adaptiveHeight: false
         });
         
         console.log(screen.width)
         
         var swiper = new Swiper('.swiper-container', {
            
            
           slidesPerView: screen.width < 500 ? 1 : 3 ,
           spaceBetween: 50,
           slidesPerGroup: screen.width < 500 ? 1 : 3 ,
           loop: true,
           loopFillGroupWithBlank: true,
           pagination: {
             el: '.swiper-pagination',
             clickable: true,
           },
           navigation: {
             nextEl: '.swiper-button-next',
             prevEl: '.swiper-button-prev',
           },
         });
      </script>
      <script>
         function increaseCount(e, el) {
         var input = el.previousElementSibling;
         var value = parseInt(input.value, 10);
         value = isNaN(value) ? 0 : value;
         value++;
         input.value = value;
         }
         function decreaseCount(e, el) {
         var input = el.nextElementSibling;
         var value = parseInt(input.value, 10);
         if (value > 1) {
         value = isNaN(value) ? 0 : value;
         value--;
         input.value = value;
         }
         }
      </script>
      <script>
         $(".variable").slick({
          dots: true,
          infinite: true,
          autoplay:true,
          time:200,
          pagination:false,
          arrows:true,
          variableWidth: true
         });
      </script>
      <script>
         function openNav(){
           document.getElementById("mySidenav").style.width="300px";
         }
         function closeNav(){
           document.getElementById("mySidenav").style.width="0px";
         }
      </script>
      <?php
         if(isset($_SESSION['message'])) 
         { 
            $login_message= $_SESSION['message'];?>
            <script>
               swal("{{ $login_message }}");
               setTimeout(() => {  window.location.href = "{{ url('/') }}"; }, 1000);
            </script><?php
            unset($_SESSION['message']);
         }
      ?>
      </script>
   </body>
</html>
