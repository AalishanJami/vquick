@extends('homepages.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Home Page Content</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('homepages.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $homepage->title }}<br>
                <strong>Description:</strong>
                {{ $homepage->description }}<br>
                <strong>Image:</strong>
                {{ $homepage->image }}
            </div>
        </div>
    </div>
@endsection