@extends('layouts.main')




@section('title')
<title>vQuick-Home</title>
@endsection

@section('styles')
    <style>
      .swiper-container{
         padding-left: 5px !important;
      }
      .counter_1{
         background-color: transparent;
      }
      .categories-title-space{
         margin-top: -20px;
      }
    </style>
@endsection

@section('content')


{{--  <?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}
?>  --}}

@php
   $productsection = App\productsections::first(); 
@endphp



<div class="container my-5 index-mobile-view">
   <div class="row" id="products-container-row">
      <div class="col-lg-12">
      {{--  <h1 class="font-weight-bold text-uppercase text-center our-product" >{{__('Our Products')}}</h1>  --}}
      @if(isset($productsection))
      <h1 class="font-weight-bold text-uppercase our-product-mobile" > {{$productsection->proper_title}} </h1>
      @endif
      </div>
      <?php use App\galleries;
      $galleries = galleries::all(); ?>
      <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 mobile-view-gallery ">
      @foreach ($galleries as $gallery)
         <img src="files/{{ $gallery->image}}" style="width: 100%;border-radius: 20px;" class="img-fluid" alt="">
      @endforeach
      <div class="row ">
         <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
            <h3 class="slh3 mt-3 ">Colombia</h3>
            <p class="slp3 mt-2 ">If you are going to use a passage of Lorem Ipsum,You need to be sure .</p>
            <h3 class="slh4 mt-3  price-product" ><b>250 QAR</b></h3>
         </div>
      <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
      <div class='counter1'  style="margin-top:1px;">
      <div class='down' onclick='decreaseCount(event, this)'>-</div>
      <input type='text' class="counter_1" value='0'  >
      <div class='up' onclick='increaseCount(event, this)'>+</div>
      </div>
      </div>
      <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >
      <button class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
      </div>
      </div>
      </div>
   
   
      @php
      
      $products = App\products::orderBy('created_at' , 'desc')->where('is_approved', '1')->get() ;
      $categories = App\categories::all();
      @endphp
      {{--  mobile products here  --}}
      @if(isset($productsection) && $productsection->content == 'product' )
      @foreach ($products->take(4) as $product)
   
      <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 product-box">
         <img src="/files/{{$product->image}}" style="width: 100%;border-radius: 20px; width: 138px; height: 137px; " class="" alt="">
         <div class="row ">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
            <h3 class="slh3 mt-3 "> {{  $product->proper_name }} </h3>
            <p class="slp3 mt-2 ">{{ Str::limit($product->proper_description , 100, '') }}</p>
            <h3 class="slh4 mt-3  price-product" ><b>{{$product->price}} QAR</b></h3>
         </div>
         
         @if($product->stock_available > 0)
         
         <form class="" style="width: 100%; display: contents; " method="get" action="/carts">
            <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
                <div class='counter1'  style="margin-top:1px;">
                  <div class='down' onclick='decreaseCount(event, this)'>-</div>
                  <input type='text' id="quantity" name="quantity" class="counter_1" value='0'  >
                  <div class='up' onclick='increaseCount(event, this)'>+</div>
                </div>
              </div>
              <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
                <input type="hidden" name="category" value="{{ $product->category }}" >
                <input type="hidden" name="image" value="{{ $product->image }}" >
                <input type="hidden" name="pid" value="{{ $product->id }}" >
                <input type="hidden" name="id" value="{{ $id ?? null }}" >
                <input type="hidden" name="name" value="{{ $product->name }}" >
                <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
                <input type="hidden" name="description" value="{{ $product->description }}" >
                <input type="hidden" name="price" value="{{ $product->price }}" >
                <button type="submit" class="btn-block btn-sli py-3  text-white add-to-cart-button-{{$product->id}} uncfocused-item" id="imgwidth" style="color: #eee2da;">{{__('ADD TO CART')}}</button>
              </div>
            </form>


            @else
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 mt-1 couuu" style="border:none" class="marbtn">
               
               <button type="button" onclick="out_of_stock()" class="btn-block btn-sli py-3  text-white uncfocused-item "  style="background-color: #627976; color: #ffffff !important;" >{{__('OUT OF STOCK')}}</button>

            </div>
            @endif
      
      </div>
      </div>
      @endforeach
      
      @else
      @foreach ($categories->take(4) as $category)
   
      <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 category-item-box">
   
         <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
            <img src="/files/{{$category->image}}" style="width: 100%;border-radius: 20px;" class="" alt=""> </a>
            
            <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
              <input type="hidden" name="category" value="{{ $category->name}}">
              <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
                  </div>
              </form>
   
         <div class="row ">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
            <h3 class="slh3 mt-3 "> {{ $category->proper_name }} </h3>
            <p class="slp3 mt-2 ">{{ Str::limit($category->proper_description , 100) }} .</p>
         </div>
      
      </div>
      </div>
      @endforeach
      @endif
   {{--  
      <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">
      <img src="assets/images/banner1.PNG" style="width: 100%;border-radius: 20px;" class="img-fluid" alt="">
      <div class="row ">
      <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
      <h3 class="slh3 mt-3 ">Colombia</h3>
      <p class="slp3 mt-2 ">If you are going to use a passage of Lorem Ipsum,You need to be sure .</p>
      <h3 class="slh4 mt-3  price-product" ><b>250 QAR</b></h3>
      </div>
      <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
      <div class='counter1'  style="margin-top:1px;">
      <div class='down' onclick='decreaseCount(event, this)'>-</div>
      <input type='text' class="counter_1" value='0'  >
      <div class='up' onclick='increaseCount(event, this)'>+</div>
      </div>
      </div>
      <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >
      <button class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
      </div>
      </div>
      </div>  --}}
   
   </div>
   <div class="col-lg-12">
    
      @if(isset($categories) || isset($products))
      <button  type="button" onclick="@if( isset($productsection) && $productsection->content == 'categories') load_more_categories() @else load_more_products() @endif"  class="btn-block btn-sli py-3  text-white show-more-home-mobile show-more-button-control" id="imgwidth" style="color: #eee2da;"> {{__('SHOW MORE')}}</button>

      
      {{--  <button type="button" onclick="window.location='/product';" class="btn-block btn-sli py-3  text-white show-more-home-mobile" id="imgwidth" style="color: #eee2da;"> {{__('SHOW MORE')}}</button>  --}}
      @endif
   </div>

</div>

@php
   $productsection = App\productsections::first(); 
@endphp

{{--  @if($productsection->content == 'categories' ) style="margin-bottom: -80px" @endif  --}}
<section class="web" >
   <div class="container-fluid  mt-3">
     <div class="row ">
        <div class="col-md-6 col-sm-12">
           <img src="assets/images/banner1.PNG" class="imgban" alt="">
        </div>
        <div class="col-md-6 col-sm-12 ban1">
           <div style="margin-left: 140px;">
              <h1 class="banh1" >{{__("May the coffee be with you")}} <br> </h1>
              <p class="banp1">{{__("People say money can’t buy happiness. They Lie. Money buys Coffee, Coffee makes Me Happy!")}} <br> {{__("Everyone should believe in something. I believe I will have another coffee.")}}. </p>
              <a href="{{url('/about')}} " class="btn-ban1 px-5 py-3" id="know-more" >{{__("KNOW MORE")}}</a>
           </div>
        </div>
     </div>
     <!-- Banner -->
     <div class="container mt-5 @if(isset($productsection) && $productsection->content == 'categories') mb-4 @else  mb-5 @endif">
        <div class="row">
           <div class="col-lg-12">
           <?php use App\productsections;
            $productsections = productsections::all(); 
            $i1=0;?>
            @foreach ($productsections as $productsection)
               <?php $i1++; ?>
            @endforeach
            <?php
            if($i1==0) { ?>
               <h1 class="font-weight-bold text-uppercase text-center our-product" >{{__("Our Products")}}</h1>
            <?php }
            else { ?>
            @foreach ($productsections as $productsection)
               <h1 class="font-weight-bold text-uppercase text-center our-product" > {{ $productsection->proper_title }} </h1>
            @endforeach
            <?php }?>
           </div>
        </div>
     </div>
     <!-- Swiper -->
     <div class="container mt-5">
     <div class="swiper-container" style="z-index: 1; max-width: 100%;">
        <div class="swiper-wrapper">
          
           
           
            <!-- slide -->
            
            <?php use App\categories;
            $categories = categories::all();
            use App\products;
            $products = products::orderBy('created_at' , 'desc')->where('is_approved', '1')->get() ; ?>
            @foreach ($productsections as $productsection)
            <?php if($productsection->content == 'product') { ?>
               @foreach ($products as $product)
               {{--  products catalog  --}}
               <div class="swiper-slide">
                     <div>
                        <img src="files/{{ $product->image}}" class="img-fluid" alt="">
                        <div class="row" style="padding: 0 ;background-color: transparent;" >
                           <div class="col-ms-12 px-3" style="background-color: transparent;padding: 0!important ;">
                            
                              
                              <h3 class="slh3 mt-3 text-left">  {{ $product->proper_name}} </h3>

                              <p class="slp3 mt-2 text-left" style="height: 60px;width: 325px;"> {{  Str::limit($product->proper_description , 100)  }} </p>
                              <h3 class="slh3 mt-3 text-left" style="margin-bottom: 31px;">{{ $product->price}} QAR</h3>
                           </div>
                           
                           <form class="" style="width: 100%; display: contents; " method="get" action="/carts">

                              @if($product->stock_available > 0)

                              <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
                                  <div class='counter1'  style="margin-top:1px;">
                                    <div class='down' onclick='decreaseCount(event, this)'>-</div>
                                    <input type='text' id="quantity" name="quantity" class="counter_1" value='0'  >
                                    <div class='up' onclick='increaseCount(event, this)' disabled >+</div>
                                  </div>
                                </div>
                                <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
                                  <input type="hidden" name="category" value="{{ $product->category }}" >
                                  <input type="hidden" name="image" value="{{ $product->image }}" >
                                  <input type="hidden" name="pid" value="{{ $product->id }}" >
                                  <input type="hidden" name="id" value="{{ $id ?? null }}" >
                                  <input type="hidden" name="name" value="{{ $product->name }}" >
                                  <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
                                  <input type="hidden" name="description" value="{{ $product->description }}" >
                                  <input type="hidden" name="price" value="{{ $product->price }}" >
                                  <button type="submit" class="btn-block btn-sli py-3 uncfocused-item text-white add-to-cart-button-{{$product->id}}" id="imgwidth" style="color: #eee2da;">{{__("ADD TO CART")}}</button>

                                  
                                </div>

                                @else
                                {{--  border:1px solid #d3aea6;border-radius: 5px;height:36px  --}}
                                 <div class="col-lg-12  col-md-12 col-sm-12 col-12 mt-1 couuu" style="" class="marbtn">

                                    <button type="button" onclick="out_of_stock()" class="btn-block btn-sli py-3 uncfocused-item text-white " id="imgwidth" style="background-color: #627976; color: #ffffff !important;">{{__("OUT OF STOCK")}}</button>

                                 </div>
                                  @endif

                              </form>
                           
                        </div>                       
                     </div>
               </div>
               @endforeach
            <?php }
            else { ?>
               @foreach ($categories as $category)

               <div class="swiper-slide">
                  <div>

                     <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
                        <input type="hidden" name="category" value="{{ $category->name}}">
                        <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
                            </div>
                        </form>

                     <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
                     <img src="files/{{ $category->image}}" class="img-fluid" alt="">
                     </a>
                     
                     <div class="row" style="padding: 0 ;background-color: transparent;" >
                        <div class="col-ms-12 px-3" style="background-color: transparent;padding: 0!important ;">
                         
                           <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
                           <h3 class="slh3 mt-3 text-left"> {{ $category->proper_name}} </h3></a>
                           {{--  mt-2   --}}
                           <p class="slp3 text-left" style="height: 60px;width: 325px;"> {{ Str::limit($category->proper_description, 100) }} </p>
                        </div>
                        
                       
                     </div>                       
                  </div>
            </div>
               @endforeach
            <?php } ?>
            @endforeach
         </div>
        <!-- Add Arrows -->  
     </div>
   </div>
 </div>
 @foreach ($productsections as $productsection)
<?php if($productsection->content == 'product') { ?>
 <div class="row">
    <div class="col-md-12 mgp" style="background-color: #e7e4da; margin-top: -366px;margin-bottom: 3.5%;">
      <div class="swiper-button-next uncfocused-item" ><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
      <div class="swiper-button-prev uncfocused-item" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>
    </div>
 </div>
 <?php }
 else { ?>
 <div class="row">
    <div class="col-md-12 mgp" style="background-color: #e7e4da; margin-top: -288px;margin-bottom: 2%;">
      <div class="swiper-button-next uncfocused-item" ><i class="fa fa-long-arrow-right" aria-hidden="true"></i></div>
      <div class="swiper-button-prev uncfocused-item" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i></div>
    </div>
 </div>
 <?php } ?>
   @endforeach
</section>






@php
$galleries = App\galleries::all();
@endphp
<section class="web">
   <div class="container my-5">
      <section class="variable slider">
         <?php for($i=0;$i<4;$i++) { ?>
         @foreach ($galleries as $gallery)
            <div>
            <img src="files/{{ $gallery->image}}">
            </div>
         @endforeach
         <?php } ?>
      </section>
   </div>

   <div class="container-fluid formdiv px-5 mb-3" id="contus">
      <div class="container">
      <?php use App\contactuses;
      $contactuses = contactuses::all();?>
      @foreach ($contactuses as $contactus)
         <?php $contact_email=$contactus->email;  ?>
      @endforeach
      <form action="/contact-form" method="POST">
         @csrf
         <div class="row">
            <div class="col-md-3">
                  <div class="form-group">
                     <input type="text" name="name" class="form-control text-left inpf" placeholder="{{__("Name")}}" required>
                  </div>
            </div>
            <div class="col-md-3">
                  <div class="form-group">
                     <input type="email" name="email" class="form-control text-left inpf" placeholder="{{__("Email Address")}}" required>
                  </div>
            </div>
            <div class="col-md-3">
                  <div class="form-group">
                     <input type="text" name="message" class="form-control text-left inpf" placeholder="{{__("Message")}}" required>
                  </div>
            </div>
            <div class="col-md-3">
               <input type="hidden" value="<?= $contact_email ?? '' ?>" name="contact_email">
               <button type="submit" class="btn-block btn-sli py-2 px-5 text-white  uncfocused-item" style="width:60%; margin-left: 15%;">{{__("SEND")}}</button>
            </div>
         </div>
      </form>
      </div>
   </div>
</section>





@endsection


@section('scripts')
    


<script>


   $('.slider').slick({
   centerMode: true,
   centerPadding: '10px',
   slidesToShow: 3,
   adaptiveHeight: false
   });
   
   
   var swiper = new Swiper('.swiper-container', {
      
      
     slidesPerView: 3 ,
     spaceBetween: 50,
     slidesPerGroup: 3 ,
     loop: true,
     loopFillGroupWithBlank: true,
     pagination: {
       el: '.swiper-pagination',
       clickable: true,
     },
     navigation: {
       nextEl: '.swiper-button-next ',
       prevEl: '.swiper-button-prev',
     },
   });
</script>
<script>
   function increaseCount(e, el) {
   var input = el.previousElementSibling;
   var value = parseInt(input.value, 10);
   value = isNaN(value) ? 0 : value;
   value++;
   input.value = value;
   }
   function decreaseCount(e, el) {
   var input = el.nextElementSibling;
   var value = parseInt(input.value, 10);
   if (value > 1) {
   value = isNaN(value) ? 0 : value;
   value--;
   input.value = value;
   }
   }
</script>
<script>
  {{--   $(".variable").slick({
   dots: true,
   infinite: true,
   autoplay:true,
   time:200,
   pagination:false,
   arrows:true,
   variableWidth: true
  });  --}}
</script>
<script>
   function openNav(){
     document.getElementById("mySidenav").style.width="300px";
   }
   function closeNav(){
     document.getElementById("mySidenav").style.width="0px";
   }
</script>
<?php
   if(isset($_SESSION['message'])) 
   { 
      $login_message= $_SESSION['message'];?>
      <script>
         swal( { text: "{{ $login_message }}" , timer: 3000, });
         setTimeout(() => {  window.location.href = "{{ url('/') }}"; }, 1000);
      </script><?php
      unset($_SESSION['message']);
   }
?>

<script>

var user_id = {{$_SESSION['id'] ?? 'null'  }};


function load_more_categories(){
  var _totalCurrentCategories= $(".category-item-box").length;
  
  console.log(_totalCurrentCategories);

  $.ajax({
    url: '/product' ,
    type:'get',
    dataType:'json',
    data:{
        skip:_totalCurrentCategories
    },
    beforeSend:function(){
        $(".load-more").html('Loading...');
    },
    success:function(response){

      
      if(response.categories.length == 0){
        swal('{{__("No More Categories")}}');
        $('.show-more-button-control').hide();
      }else{
        $('.show-more-button-control').show();
      }
      if(response.categories.length < 4 ){
        $('.show-more-button-control').hide();
      }

    response.categories.forEach(function(category){
      create_category(category);

    });
       
    },
    });

}


function create_category(category){


  $('#products-container-row').append(' <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 category-item-box" >  <a class="view-category-form-button" onclick="get_category_products('+category.id+')" > <img src="files/'+ category.image +'"  style="width: 100%;border-radius: 20px; width: 138px; height: 137px; " alt=""></a>  <form method="GET" id="category-'+category.id+'-products" action="/product-category"  >  <input type="hidden" name="category" value="'+ category.name +'"> <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;"> </div> </form>  <div class="row ">  <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" > <a class="view-category-form-button" onclick="get_category_products('+category.id+')" > <h3 class="slh3 mt-3 ">'+ category.proper_name +'</h3> </a> <p class="slp3 mt-2 "> '+ category.proper_description +' </p> </div> </div> </div>  ');
}

function get_category_products(id){
  $('#category-'+id+'-products').submit();
}




function load_more_products(){
  var _totalCurrentResult= $(".product-box").length;

  $.ajax({
    url: '/product' ,
    type:'get',
    dataType:'json',
    data:{
        skip:_totalCurrentResult
    },
    beforeSend:function(){
        $(".load-more").html('Loading...');
    },
    success:function(response){

      if(response.products.length == 0){
        swal('{{__("No More Products")}}');
        $('.show-more-button-control').hide();
      }else{
        $('.show-more-button-control').show();
      }

      if(response.products.length < 4){
        $('.show-more-button-control').hide();
      }
    response.products.forEach(function(product){
      create_product(product);
    });
       
    },
    });
}

function create_product(product){

  if(product.stock_available > 0){
    var add_to_cart_button = ' <form class="" style="width: 100%; display: contents; " method="get" action="/carts">  <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn"> <div class="counter1"  style="margin-top:1px;"> <div class="down" onclick="decreaseCount(event, this)">-</div> <input type="text" id="quantity" name="quantity" class="counter_1" value="0"  > <div class="up" onclick="increaseCount(event, this)">+</div> </div> </div> <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" > <input type="hidden" name="category" value="'+product.category+'" > <input type="hidden" name="image" value="'+product.image+'" > <input type="hidden" name="pid" value="'+product.id+'" > <input type="hidden" name="id" value="'+user_id +'" > <input type="hidden" name="name" value="'+product.name+'" > <input type="hidden" name="stock_available" value="'+product.stock_available+' " > <input type="hidden" name="description" value="'+product.description+' " > <input type="hidden" name="price" value="'+product.price+'" > <button type="submit" class="btn-block btn-sli py-3 uncfocused-item text-white add-to-cart-button-'+product.id+'" id="imgwidth" style="color: #eee2da;">{{__("ADD TO CART")}}</button> </div> </form>';
  }
  else{
    var add_to_cart_button = '<div class="col-lg-12  col-md-12 col-sm-12 col-12 mt-1 product-button" >   <button type="button" onclick="out_of_stock()" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="background-color: #627976; color: #ffffff !important;" >{{__("OUT OF STOCK")}}</button> </div>';
  }
  $('#products-container-row').append('<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 product-box">  <img src="files/'+product.image+'" style="width: 100%;border-radius: 20px; width: 138px; height: 137px; "  alt="" /> <div class="row ">  <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >  <h3 class="slh3 mt-3 ">'+product.proper_name+'</h3> <p class="slp3 mt-2 "> '+product.proper_description+' .</p> <h3 class="slh4 mt-3  price-product" ><b>'+product.price+' QAR</b></h3>  </div>  '+ add_to_cart_button +'  </div> </div>');
}




</script>


@endsection