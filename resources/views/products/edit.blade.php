@extends('products.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
         <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Name:</strong>
          {{--  <select class="form-control" name="name">
          <?php use App\products;
              $products = products::all(); ?>
            <option selected value="{{ $product->name }}">{{ $product->name }}</option>
              @foreach ($products as $product1)
                <option value="{{ $product1->name}}">{{ $product1->name}}</option>
              @endforeach
          </select>  --}}
          <input type="text" class="form-control" value="{{ $product->name }}" name="name" placeholder="Name" required="required"/>

        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Name:</strong>
          <input type="text" class="form-control" value="{{ $product->arabic_name }}" name="arabic_name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>English Description:</strong>
          <textarea class="form-control" name="description" placeholder="Enter Category English" >{{ $product->description }}</textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Description:</strong>
          <textarea  class="form-control" name="description_ar" placeholder="Enter Category Arabic" >{{ $product->description_ar }}</textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Price:</strong>
          <input type="text" class="form-control" name="price" value="{{ $product->price }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Stock Available:</strong>
          <input type="text" class="form-control" name="stock_available" value="{{ $product->stock_available }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Category:</strong>
          <select class="form-control" name="category">
          <?php use App\categories;
              $categories = categories::all(); ?>
            <option selected value="{{ $product->category }}">{{ $product->category }}</option>
              @foreach ($categories as $category)
                <option value="{{ $category->name}}">{{ $category->name}}</option>
              @endforeach
          </select>
        </div>
      </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Activate/Deactivate:</strong>
                    <select class="form-control" name="is_approved">
                    <?php
                    $a=0;$a1=0;$b=0;$b1=0;
                    if($product->is_approved == '1') {
                      $a="Enabled";
                      $a1='1';
                      $b="Diable";
                      $b1='0';
                    } else {
                      $a="Disabled";
                      $a1='0';
                      $b="Enable";
                      $b1='1';
                    }?>
                        <option selected value="<?= $a1 ?>"><?= $a ?></option>
                        <option value="<?= $b1 ?>"><?= $b ?></option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12" >
              <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" style="width:100%;" class="btn btn-success" >
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection