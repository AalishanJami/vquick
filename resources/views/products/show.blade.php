@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $product->name }}<br>
                <strong>Arabic Name:</strong>
                {{ $product->arabic_name }}<br>
                <strong>Description:</strong>
                {{ $product->description }}<br>
                <strong>Arabic Description:</strong>
                {{ $product->description_ar }}<br>
                <strong>Price:</strong>
                {{ $product->price }} QAR<br>
                <strong>Available Stock:</strong>
                {{ $product->stock_available }}<br>
                <strong>Category:</strong>
                {{ $product->category }}<br>
                <strong>Image:</strong>
                <?php $path= '/files/'. $product->image; ?>
                <img style="width:100px;height: 100px;" src="<?= $path ?>" /><br>
            </div>
        </div>
    </div>
@endsection