@extends('products.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New Products</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  

  <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Name:</strong>
          <input type="text" class="form-control" placeholder="Enter Product Name" name="name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>ArabicName:</strong>
          <input type="text" class="form-control" placeholder="Enter Product Name in Arabic" name="arabic_name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>English Description:</strong>
          <textarea class="form-control" name="description" placeholder="Enter English Description" ></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Description:</strong>
          <textarea  class="form-control" name="description_ar" placeholder="Enter Arabic Description" ></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Image:</strong>
          <input type="file" name="image" style="width:100%;" class="btn btn-success" required="required">
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Price:</strong>
          <input type="text" class="form-control" name="price" placeholder="Enter Price" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Stock Available:</strong>
          <input type="text" class="form-control" name="stock_available" placeholder="Enter Stock Available" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Category:</strong>
          <select class="form-control" name="category">
            <option selected disabled>Select Category</option>
            <?php use App\categories;
              $categories = categories::all(); ?>
              @foreach ($categories as $category)
                <option value="{{ $category->name}}">{{ $category->name}}</option>
              @endforeach
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Enable / Disable:</strong>
          <select class="form-control" name="is_approved">
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>
  @endsection