@extends('layouts.main')




@section('title')
<title>Order History</title>
@endsection

@section('styles')
    
<style>
  .vquick-grey-back{
    background-color: #666666;
    color : #ffffff;
  }
  
  .vquick-grey-back:hover {
    background-color: #666666 !important;
    color : #ffffff !important;
  }
</style>
@endsection

@section('content')

@php
    
if(!isset($_SESSION)) 
{ 
  session_start(); 
}
$id = $_SESSION["id"];
$price=0;
$z=0;

@endphp

<div class="container mt-5 badip">
    <div class="row no1">
      <div class="col-md-6">
        <h1 class="chch1 checkout-h1">{{__("Order History")}}</h1>
      </div>
      <div class="col-md-6">
      </div>
    </div>
  </div>
  {{--  badip -> web only  --}}
  <div class="container mb-4 mt-3 ">

  </div>
  <div class="container mb-4 mt-3 badip">
    <div class="row no2">
   
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">{{__('Order Number')}}</p>
      </div>
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">{{__('Status')}}</p>
      </div>
      <div class="col-md-3">
        <p class="chcp1 checkout-main-row">{{__('Payment Status')}}</p>
      </div>
    
      
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">{{__('Date')}}</p>
      </div> 
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">{{__('Price')}}</p>
      </div>
      <div class="col-md-1">
        <p class="chcp1 checkout-main-row"></p>
      </div>

    </div>

    <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
    @foreach ($orders as $order)

    <div class="row" style="height: 70px;">
      <div class="col-md-2 d-flex align-items-center">
        <h2 class="chch2 checkout-list-h2 ">{{ $order->id }}</h2>
      </div>
     
      <div class="col-md-2 d-flex align-items-center" >
          {{ ucwords($order->status) }}
      </div>
      
      <div class="col-md-3 d-flex align-items-center" >
          {{ $order->payment_method }}
      </div>

      {{--  <div class="col-md-1 pt-5" >
        <div class='counter-checkout'>


          <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
            <input type='text' value='{{ $order->quantity }}'  >
          <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
          </div>
        </div>  --}}
        <div class="col-md-2 d-flex align-items-center">  
            
          <p class="chcp1 checkout-list-p " >{{$order->created_at->toDateString()}}</p>
        </div>

        <div class="col-md-2 d-flex align-items-center" >
          <h2 class="chch2 c-price" >QAR {{ $order->total_price}}</h2>
        </div>
     
        
        <div class="col-md-1 d-flex align-items-center">  
            <a class="btn uncfocused-item" href="{{route('order-history.details' , $order->id)}}" style="background-color: #627976; color: #ffffff">{{__('Details')}}</a>            
        </div>
        
      </div>
      <hr class ="no3" style="background-color: #d9d9d9;">
      @endforeach
    </div>
  </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  <div class="col-lg-12">
      <h1 class="font-weight-bold text-uppercase our-product-mobile">{{__("Order History")}}</h1>
  </div>
{{--  @foreach ($carts as $cart)
  <div class="row no-gutters  margin-bottom-2 ">
      <div class="col-sm-5 col-5 checkout-mobile">
          <img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
      </div>
      <div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >

          <h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->name}}</h2>
          <p class="chcp1" style="font-size: 12px;">{{substr($cart->description,0,30)}} </p>
    <h2 class="chch2 m-price"><strong>QR {{ $cart->price}} </strong></h2>
    
    <h2 style="margin-top: -5px !important" class="chch2 m-price"><strong>Quantity {{ $cart->quantity}} </strong></h2>

    {{ $cart->order_status ? $cart->order_status->status : 'Removed'}}
    <br>
    {{ $cart->created_at->toDateString()}}
  
      </div>
</div>
  <hr style="background-color: #d9d9d9;">
@endforeach  --}}

@foreach ($orders as $order)
<div class="row no-gutters  px-2 py-4 mobile-checkout-container" style="height: 180px" >
  <div class="col-sm-5 col-5 checkout-mobile">
    {{__("Order Number")}}:
  </div>
  <div class="col-sm-7 col-7 checkout-mobile">
    {{$order->id}}
  </div>
  
  <div class="col-sm-5 col-5 checkout-mobile">
    {{__("Status")}}:
  </div>

  <div class="col-sm-7 col-7 checkout-mobile">
    {{ucwords($order->status)}}
  </div>
  
  <div class="col-sm-5 col-5 checkout-mobile">
    {{__("Payment Status")}}:
  </div>
  
  <div class="col-sm-7 col-7 checkout-mobile">
    {{$order->payment_method}}
  </div>
   <div class="col-sm-5 col-5 checkout-mobile">
    {{__("Date")}}:
  </div>
  
  <div class="col-sm-7 col-7 checkout-mobile">
    {{$order->created_at->toDateString()}}
  </div>
  <div class="col-sm-5 col-5 checkout-mobile">
    {{__("Price")}}:
  </div>
  
  <div class="col-sm-7 col-7 checkout-mobile">
    {{$order->total_price}} QAR
  </div>
  
  <div class="col-sm-12 col-12 ">
    <a class="btn w-100 mt-2 uncfocused-item" href="{{route('order-history.details' , $order->id)}}"  style="background-color: #627976; color:#ffffff; padding:0; " >{{__('Details')}} </a>
  </div>
  
</div>
<hr style="background-color: #d9d9d9;">
@endforeach

@foreach ($orders as $order)
  {{-- <div class="row no-gutters   px-2 py-4 mobile-checkout-container" style="height: 45px;"> --}}

      {{-- <div class="col-sm-1 col-1 checkout-mobile">
        {{ $order->id}}
      </div>
      <div class="col-sm-3 col-3 checkout-mobile">
        {{ $order->customer->full_name}}
      </div>
      
      <div class="col-sm-3 col-3 checkout-mobile">
        {{ $order->status}}
      </div>
      
      <div class="col-sm-2 col-2 checkout-mobile">
        {{ $order->total_price}}
      </div>

      <div class="col-sm-3 col-3 checkout-mobile">
        {{ $order->created_at->toDateString()}}
      </div> --}}
          {{--  <h2 class="chch2 font-weight-bold checkout-list-h2">{{ $order->customer->name}}</h2>  --}}
          {{--  <p class="chcp1" style="font-size: 12px;">{{substr($cart->description,0,30)}} </p>  --}}
    {{--  <h2 class="chch2 m-price"><strong>QR {{ $order->price}} </strong></h2>  --}}
    
    {{--  <h2 style="margin-top: -5px !important" class="chch2 m-price"><strong>Quantity {{ $order->id}} </strong></h2>  --}}

  
      {{-- </div> --}}

  {{-- <hr style="background-color: #d9d9d9;"> --}}
@endforeach


@endsection