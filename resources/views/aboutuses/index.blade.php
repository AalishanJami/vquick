@extends('aboutuses.layout') 
@section('content')
<?php $inc=0; ?>
@foreach ($aboutuses as $aboutus)
<?php $inc++; ?>
@endforeach
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <?php if($inc==0) { ?>
                <a class="btn btn-success" href="{{ route('aboutuses.create') }}"> Create New About Us</a>
                <?php } ?>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Title</th>
          <th>Description</th>
          <th>Image</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($aboutuses as $aboutus)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $aboutus->title }}</td>
          <td>{{ $aboutus->description }}</td>
          <td><img style="width:100px;height: 100px;" src="{{asset('files/'.$aboutus->image)}} " /></td>
          <td>
            <form id="delete-form-{{$aboutus->id}}" action="{{ route('aboutuses.destroy',$aboutus->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('aboutuses.show',$aboutus->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('aboutuses.edit',$aboutus->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              <button type="button" onclick="ask_delete({{$aboutus->id}})" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
    @if($aboutuses->links())
    
    {{$aboutuses->links()}}
    
    @endif
@endsection