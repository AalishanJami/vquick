@extends('layouts.main')


{{--  @dd(session('added-pid'))  --}}




@section('title')
<title>Products</title>
@endsection

{{--  @if(session('added-pid'))
  @dd(session('added-pid'))
@endif  --}}
@section('styles')



<style>

  @media only screen and (max-width: 500px){
    
.img-fluid{
  width: 137px !important;
  height: 138px !important;
  
}
}

@media only screen and (min-width: 992px){
  .product-mobile{
    margin-left:-15px;
  }
}

.counter_1{
  background-color: transparent;
}

  .main-heading{
    font-family: "Didot LT Std" !important;
    font-weight: bold !important;
  }
</style>
@endsection

@section('content')
{{--  
<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}
?>  --}}

<div class="container my-5">
  <div class="row" id="products-container-row" >
    <div class="col-lg-12">
      {{--  <h1 class="font-weight-bold text-uppercase text-center our-product" >  --}}
      <h1 class="font-weight-bold text-uppercase text-center our-product " >
        
        {{--  @dd($productsection)  --}}
        @if(isset($productsection))
        {{$productsection->proper_title}}
        @else
        {{__("Our Products")}}
        @endif
        
      </h1>
      <h1 class="font-weight-bold text-uppercase our-product-mobile" >
        
        @if(isset($productsection))
        {{$productsection->proper_title}}
        @else
        {{__("Our Products")}}
        @endif
        </h1>
    </div>

  
  @if(isset($categories))
  @foreach ($categories as $category)
  
  <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 category-item-box">
    
    <a class="view-category-form-button" onclick="get_category_products({{$category->id}})" >
    <img src="files/{{ $category->image}}"  class="img-fluid pr_img" alt=""></a>
    
    <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
      <input type="hidden" name="category" value="{{ $category->name}}">
      <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
          </div>
      </form>
   
   <div class="row ">
    
      <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
        <a class="view-category-form-button" onclick="get_category_products({{$category->id}})" >
        <h3 class="slh3 mt-3 ">{{ $category->proper_name}}</h3>
       
        </a>
        <p class="slp3 mt-2 ">
          {{ Str::limit($category->proper_description,0,100) }}</p>
      </div>

    </div>
  </div>

  @endforeach

  @elseif(isset($products))

  @foreach ($products as $product)
    
   <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 product-box">
      <img src="files/{{ $product->image}}"  class="img-fluid pr_img" alt="" />

      <div class="row ">
      
        <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
          <h3 class="slh3 mt-3 ">{{ $product->proper_name}}</h3>

          <p class="slp3 mt-2 ">
            {{ Str::limit($product->proper_description , 100) }}</p>
          <h3 class="slh4 mt-3  price-product" ><b>{{ $product->price}} QAR</b></h3>
          {{--  <h3 class="slh4 price-quantity mobile-mt-1" ><b> Quantity {{ $product->stock_available}}</b></h3>  --}}
        </div>

        <form id="add-to-cart-form-{{$product->id}}" class="" style="width: 100%; display: contents; " method="get" action="/carts">

          @if($product->stock_available > 0)

        <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
            <div class='counter1'  style="margin-top:1px;">
              <div class='down' onclick='decreaseCount(event, this)'>-</div>
              <input type='text' id="quantity-{{$product->id}}" name="quantity" class="counter_1" value='0'  >
              <div class='up' onclick='increaseCount(event, this)' disabled >+</div>
            </div>
          </div>
          <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
            <input type="hidden" name="category" value="{{ $product->category }}" >
            <input type="hidden" name="image" value="{{ $product->image }}" >
            <input type="hidden" name="pid" value="{{ $product->id }}" >
            <input type="hidden" name="id" value="{{ $id ?? null }}" >
            <input type="hidden" name="name" value="{{ $product->name }}" >
            <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
            <input type="hidden" name="description" value="{{ $product->description }}" >
            <input type="hidden" name="price" value="{{ $product->price }}" >

            <button type="submit" name="submit" class="btn-block btn-sli py-3  text-white add-to-cart-button-{{$product->id}} uncfocused-item" id="imgwidth" style="color: #eee2da;">{{__('ADD TO CART')}} </button>
            </div>

        @else

         <div class="col-lg-12  col-md-12 col-sm-12 col-12 mt-1 product-button" >  
            <button type="button" onclick="out_of_stock()" class="btn-block btn-sli py-3  text-white uncfocused-item" id="imgwidth" style="background-color: #627976; color: #ffffff !important;" >{{__("OUT OF STOCK")}}</button>
         </div>
            @endif
          
        </form>
          
        
      </div>
    </div>
    @endforeach
   
    @endif

  </div>
  <div class="row">

    <div class="col-lg-12">
      <button type="button" onclick="@if(isset($categories)) load_more_categories() @else load_more_products() @endif" class="btn-block btn-sli py-3  text-white show-more-home-mobile show-more-button-control" id="imgwidth" style="color: #eee2da;">{{__('SHOW MORE')}}</button>
    </div>
  </div>
  </div>




  <div class="container show-more-product">
  
    <div class="offset-lg-5 col-lg-3 text-center my-5 ">
      @if( isset($products) && !isset($products->show_all ) || isset($categories) && !isset($categories->show_all ) )
      <form id="show-all-form" action="/product" method="GET" >
        <input type="hidden" name="show_all" value="1" />
      </form>
      <button class="btnpro btn-block  py-2 uncfocused-item" type="button" onclick=" $('#show-all-form').submit() " style="border:1px solid #d3aea6;
            color: #d3aea6;
            background-color: transparent;
            font-size: 20px;
            margin-top: 50px;
            border-radius: 5px;">{{__('Show All')}}</button>
    
            @endif
    </div>
    
        </div>


        @endsection
        
        
        
@section('scripts')

@include('layouts.partials.errors')

<script>


  



  function increaseCount(e, el) {
  var input = el.previousElementSibling;
  var value = parseInt(input.value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  input.value = value;
  }
  function decreaseCount(e, el) {
  var input = el.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 1) {
  value = isNaN(value) ? 0 : value;
  value--;
  input.value = value;
  }
  }





  function openNav(){
    document.getElementById("mySidenav").style.width="300px";
  }
  function closeNav(){
    document.getElementById("mySidenav").style.width="0px";
  }


  var user_id = {{$id ?? 'null'  }};


  function load_more_categories(){
    var _totalCurrentCategories= $(".category-item-box").length;
    
    console.log(_totalCurrentCategories);

    $.ajax({
      url: '/product' ,
      type:'get',
      dataType:'json',
      data:{
          skip:_totalCurrentCategories
      },
      beforeSend:function(){
          $(".load-more").html('Loading...');
      },
      success:function(response){

        
        if(response.categories.length == 0){
          swal({text: '{{__("No More Categories")}}' , timer: 3000});
          $('.show-more-button-control').hide();
        }else{
          $('.show-more-button-control').show();
        }
        if(response.categories.length < 3 ){
          $('.show-more-button-control').hide();
        }

      response.categories.forEach(function(category){
        create_category(category);

      });
         
      },
      });

  }


  function create_category(category){


    $('#products-container-row').append(' <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 category-item-box" >  <a class="view-category-form-button" onclick="get_category_products('+category.id+')" > <img src="files/'+ category.image +'"  class="img-fluid pr_img" alt=""></a>  <form method="GET" id="category-'+category.id+'-products" action="/product-category"  >  <input type="hidden" name="category" value="'+ category.name +'"> <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;"> </div> </form>  <div class="row ">  <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" > <a class="view-category-form-button" onclick="get_category_products('+category.id+')" > <h3 class="slh3 mt-3 ">'+ category.proper_name +'</h3> </a> <p class="slp3 mt-2 "> '+ category.proper_description +' ...</p> </div> </div> </div>  ');
  }

  function get_category_products(id){
    $('#category-'+id+'-products').submit();
  }




  function load_more_products(){
    var _totalCurrentResult= $(".product-box").length;

    $.ajax({
      url: '/product' ,
      type:'get',
      dataType:'json',
      data:{
          skip:_totalCurrentResult
      },
      beforeSend:function(){
          $(".load-more").html('Loading...');
      },
      success:function(response){

        if(response.products.length == 0){
          swal({text: '{{__("No More Products")}}' , timer: 3000});

          $('.show-more-button-control').hide();
        }else{
          $('.show-more-button-control').show();
        }

        if(response.products.length < 3){
          $('.show-more-button-control').hide();
        }
      response.products.forEach(function(product){
        create_product(product);
      });
         
      },
      });
  }

  function create_product(product){

    if(product.stock_available > 0){
      var add_to_cart_button = ' <form class="" style="width: 100%; display: contents; " method="get" action="/carts">  <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn"> <div class="counter1"  style="margin-top:1px;"> <div class="down" onclick="decreaseCount(event, this)">-</div> <input type="text" id="quantity" name="quantity" class="counter_1" value="0"  > <div class="up" onclick="increaseCount(event, this)">+</div> </div> </div> <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" > <input type="hidden" name="category" value="'+product.category+'" > <input type="hidden" name="image" value="'+product.image+'" > <input type="hidden" name="pid" value="'+product.id+'" > <input type="hidden" name="id" value="'+user_id +'" > <input type="hidden" name="name" value="'+product.name+'" > <input type="hidden" name="stock_available" value="'+product.stock_available+' " > <input type="hidden" name="description" value="'+product.description+' " > <input type="hidden" name="price" value="'+product.price+'" > <button type="submit" name="submit" class="btn-block btn-sli py-3 uncfocused-item  text-white add-to-cart-button-'+product.id+' " id="imgwidth" style="color: #eee2da;">{{__("ADD TO CART")}}</button> </div> </form>';
    }
    else{
      var add_to_cart_button = '<div class="col-lg-12  col-md-12 col-sm-12 col-12 mt-1 product-button" >   <button type="button" onclick="out_of_stock()" class="btn-block btn-sli py-3  text-white uncfocused-item" id="imgwidth" style="background-color: #627976; color: #ffffff !important;" >{{__("OUT OF STOCK")}}</button> </div>';
    }
    $('#products-container-row').append('<div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4 product-box">  <img src="files/'+product.image+'"  class="img-fluid pr_img" alt="" /> <div class="row ">  <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >  <h3 class="slh3 mt-3 ">'+product.proper_name+'</h3> <p class="slp3 mt-2 "> '+product.proper_description+' .</p> <h3 class="slh4 mt-3  price-product" ><b>'+product.price+' QAR</b></h3>  </div>  '+ add_to_cart_button +'  </div> </div>');
  }




</script>
@endsection