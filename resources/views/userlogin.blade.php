



@extends('layouts.main')



@section('title')
<title>Login</title>
@endsection

@section('content')
    
<div class="container websi mt-5"  style="width: 78%;">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="assets/images/contact.PNG" class="sigdi1 w-100" alt="">
    </div>
    <div class="col-lg-6 px-5  sigdi2" style="padding-top: 18%;background-color: #F0E1DD;margin-left:-2px;">
    
      <form action="user-login" method="POST" enctype="multipart/form-data">
        @csrf
        
      <div style="margin-left: 46px; margin-right: 83px;">
		  <div class="form-group">
			<input type="email" class="form-control text-left inpf input_reg desktop-fields" name="email" placeholder="{{__('Email')}}" required >
		  </div>
		 
		  <div class="form-group mt-5">
		   <input type="password" class="form-control text-left inpf input_reg desktop-fields" name="password" placeholder="{{__('Password')}}" required >
		  </div>
		</div>
	<div class="row mt-5 button-signup-login">
	  <div class="col-lg-6 mgr">
		<button type="submit" class="logbt btn-block py-3 button-signup-login-buttons uncfocused-item" >{{__('Log In')}}</button>
	  </div>
	  
	  <div class="col-lg-6 mgl">
		<button class="logbt2 btn-block py-3 button-signup-login-buttons-1 uncfocused-item" type="button" ><a href="{{ url('/signup') }}">{{__('Sign up')}}</a></button>
	  </div>
  </div>
      </form>
<p class="text-center mt-5 forget-password"><a  href="{{route('reset.password')}} " >{{__('Forgot Password')}}?</a></p>
    </div>
  </div>
</div>




<section class="formview">
  <div class="container mblsig mt-5">
   <div class="row">
     <div class="col-lg-12 login-mobile">
       <h3 class="text-center  font-weight-bold pb-3" >{{__('Log In')}}</h3>
   <form action="user-login" method="POST" >
     @csrf
   <div class="form-group">
     <input  type="email" class="form-control inpf2 mobile-login " name="email" placeholder="{{__('Email')}}" required>
   </div>
 
   <div class="form-group">
     <input type="password" class="form-control  inpf2 mobile-login " name="password" placeholder="{{__('Password')}}" required>
   </div>
   <p class="text-right mt-3"><a href="{{route('reset.password')}}" style="color:#343a40ad">Forgot Password?</a></p>
   <button type="submit" class="btn-block" >{{__('Log In')}}</button>
   <p class="text-center mt-5 login-text">{{__('Do not have an account')}}?<a href="{{ url('/signup') }}" class="sigla">{{__("Register Now")}}</a></p>
 </form>
    </div>
   </div>
 </div>
<!-- <hr style="border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->

</section>


@endsection