



@extends('layouts.main')




@section('title')
<title>Register</title>
@endsection


@section('content')
    

    <!-- signup web view -->
<div class="container websi mt-5 mb-5" style="width:78%">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="assets/images/contact.PNG" class="sigdi1 w-100" alt="">
    </div>
    
      <div class="col-lg-6 px-5  sigdi2" style="padding: 5%;background-color: #F0E1DD;margin-left:-2px;">
      <form action="{{ route('regusers.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
        <div style="margin-left: 46px; margin-right: 83px;">
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="full_name" id="full_name" placeholder="{{__('Full Name')}}" required="required">
          </div>
        
          <div class="form-group mt-5">
          <input type="text" class="form-control text-left inpf input_reg" name="phone" id="mobile_no" placeholder="{{__('Mobile number')}}" required="required">
          </div>
          <div class="form-group">
          <input type="email" class="form-control text-left inpf input_reg" name="email" id="email_login" placeholder="{{__('Email')}}" required="required">
          </div>
          <div>
          <input type="text" class="form-control text-left inpf input_reg"  name="address" id="address" placeholder="{{__('Address')}}" required="required">
          </div>
          <div class="form-group mt-5">
          <input type="text" class="form-control text-left inpf input_reg" name="zone" id="zone" placeholder="{{__('Zone Number')}}" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg" name="street" id="street" placeholder="{{__('Street Number')}}" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg" name="building" id="building" placeholder="{{__('Building Number')}}" required="required">
          </div>

          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" name="password" id="password_login" placeholder="{{__('Password')}}" required="required">
          </div>
          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" id="re_password_login" placeholder="{{__('Re-enter Password')}}" >
          </div>
          <input type="hidden" value="1" name="page">
        </div>
        <div class="row mt-5 button-signup-login">
          <div class="col-lg-6 mgr">
          <button type="submit" class="logbt btn-block py-3 button-signup-login-buttons uncfocused-item" >{{__('Sign up')}}</button>
          </div>
          
          <div class="col-lg-6 mgl">
          <button type="button" class="logbt2 btn-block py-3 button-signup-login-buttons-1 uncfocused-item" ><a href="{{ url('/userlogin') }}">{{__("Log In")}}</a></button>
          </div>
        </div>
        </form>
      </div>
    </div>
</div>
  <!-- signup web view -->

  <!-- signup mobile view -->
  <section class="signview">

 

  <div class="container mblsig" id="mblsig">
    <div class="row">
      <div class="col-lg-12 login-mobile">
        <h3 class="text-center  font-weight-bold pb-3" >{{__('Sign up')}}</h3>

        @if($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach
        @endif

        <form action="{{ route('regusers.store') }}"  method="POST" >
          @csrf
          <div class="form-group">
            <input  type="text" class="form-control inpf2"  name="full_name" placeholder="{{__('Full Name')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" name="phone"  placeholder="{{__('Mobile number')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" name="address"   placeholder="{{__('Address')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" name="zone"  placeholder="{{__('Zone Number')}}" required>
          </div>
          
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" name="street"   placeholder="{{__('Street Number')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2"name="building"    placeholder="{{__('Building Number')}}" required>
          </div>
          <div class="form-group">
            <input  type="email" class="form-control  inpf2" name="email"  placeholder="{{__('Email address')}}" required>
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2" name="password"   placeholder="{{__('Password')}}" required>
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2" name="confirm_password"  placeholder="{{__('Re-enter password')}}" required>
          </div>

          <input type="hidden" value="1" name="page">

		
          <input class="text-right mt-3" id="terms_and_conditions" style="margin-left: 0px;"type="checkbox"/><lable style="margin-left: 8px;" for="terms_and_conditions" >I accept the terms and conditions</lable>
		
          <button type="submit" class="btn-block uncfocused-item" style="margin-top:1rem;" >{{__('Sign Up')}}</button>
          <p class="text-center mt-5 login-text ">Already have an account?<a href="{{route('userlogin')}}" class="sigla">{{__('Log In')}}</a></p>

          </form>
		
		</div>
    </div>
  </div>
 <!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->

</section>



@endsection
