{{--  <?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}
?>  --}}


<!DOCTYPE html>
<html lang="{{ session('locale') }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

      <!-- Styles -->
      <style>
               @font-face {
         font-family: 'Ooredoo Regular';
         src: url('/assets/fonts/OoredooArabic-Regular.eot');
         src: url('/assets/fonts/OoredooArabic-Regular.eot?#iefix') format('embedded-opentype'),
               url('/assets/fonts/OoredooArabic-Regular.woff') format('woff'),
               url('/assets/fonts/OoredooArabic-Regular.ttf') format('truetype'),
               url('https://www.ooredoo.com/businessinstinct/themes/default%20copy/fonts/OoredooArabic-Regular.svg') format('svg');
               
         font-weight: normal;
         font-style: normal;
         /* Better Font Rendering =========== */
         -webkit-font-smoothing: antialiased;
         -moz-osx-font-smoothing: grayscale;
      }


         html, body {
               background-color: #fff;
               color: #636b6f;
               font-weight: 200;
               height: 100vh;
               margin: 0;

         }

         @if(session('locale') == 'ar')
         body, div , button , p , h1 , h2 , h3 , h4 , h5 , h6 , a {
            font-family: 'Ooredoo Regular' , sans-serif !important;
         }
         button {
            letter-spacing: -1px !important;

         }
         
         @media only screen and (max-width: 500px){
         body, div , button , p , h1 , h2 , h3 , h4 , h5 , h6 , a {
            letter-spacing: -1px !important;
         }
         
         #mySidenav a{
            letter-spacing: -2px !important;
         }
         }         
         @endif

         .full-height {
               height: 100vh;
         }

         .flex-center {
               align-items: center;
               display: flex;
               justify-content: center;
         }

         .position-ref {
               position: relative;
         }

         .top-right {
               position: absolute;
               right: 10px;
               top: 18px;
         }

         .content {
               text-align: center;
         }

         .title {
               font-size: 84px;
         }

         .links > a {
               color: #636b6f;
               padding: 0 25px;
               font-size: 13px;
               font-weight: 600;
               letter-spacing: .1rem;
               text-decoration: none;
               text-transform: uppercase;
         }

         .m-b-md {
               margin-bottom: 30px;
         }
 
         .img-fluid {
            width: 340px !important;
            height: 330px !important;
         }
         .view-category-form-button{
            cursor: pointer;
         }

         @media only screen and (max-width: 500px){
         .mobile-view-gallery{
            display: none;
         } }

         .active-menu{
            border-bottom: 2px solid #627976;
            color: #627976 !important;
         }

         .swal-button{
            background-color: #F0E1DD;
            border: 1px solid #F0E1DD;
         }
         .swal-button:active {
            background-color: #F0E1DD !important;
            border: 1px solid #F0E1DD !important;
         }
         .swal-button:focus{
            background-color: #F0E1DD !important;
            border: 1px solid #F0E1DD !important;
            box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgb(240, 225, 221) !important;

            
         }
         .swal-button:not([disabled]):hover {
            background-color: #F0E1DD !important;
            border: 1px solid #F0E1DD !important;
          
         }
         .language-change-link{
            position: absolute;
            right: -20px;
            color: #666666;
            text-decoration: none;
         }
         
         .language-change-link:hover {
            color: #666666;
            text-decoration: none;
         }
         .cart-item-show{
            background-color: #d3aea6;
             color: #fff;
             text-decoration: none;
             border-radius: 200%;
             text-align: center;
             position: absolute;
             bottom: 25%;
             right: 4%;
             width: 13px;
             height: 13px;
             display: flex;
             justify-content: center;
             align-items: center;
             font-size: 10px;

         }

         @media only screen and (max-width: 500px){
         .cart-item-show{
            background-color: #d3aea6;
            color: #fff;
            text-decoration: none;
            border-radius: 200%;
            text-align: center;
            position: absolute;
            top: 17px;
            right: 45%;
            width: 20px;
            height: 22px;
         }
         }
         .product-added-sign{
            display: none;
         }
                     
            .uncfocused-item:focus {
               outline:none !important; 
               {{--  border: none;  --}}
               {{--  box-shadow: none;  --}}
            }
            .uncfocused-item {
               outline:none !important; 
               box-shadow: none !important;
               {{--  border: none;  --}}
            }

            .swal-footer{
               display: none;
            }
            .swal-text{
               margin-bottom: 30px;
            }
            .swal-modal{
               width: 360px;
            }
      </style>
      {{--  @if(session('locale') == 'ar')
      
      *{
         text-align: right;

      }
      @endif  --}}
      @yield('styles')

      @yield('tags')
        
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- StyleSheet -->
      <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
      <!-- GoogleFonts -->
      <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">
      <!-- Slick slider -->
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
      <!-- Link Swiper s CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/css/swiper.css">
      {{--  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  --}}
     
     @yield('title')
	  <link rel="stylesheet" href="{{asset('assets/css/own-style.css')}}">
   
   </head>
   <body>
      <!-- Header -->    
      <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
                	<a href="{{ url('/') }}" > 
					<img src="{{asset('assets/images/logo.png')}}" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
            </a>  

            <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase @if( strpos (url()->current() , 'product' ) ) active-menu @endif"  href="{{ url('/product') }}">{{__('Products')}}</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase @if( strpos (url()->current() , 'about' ) ) active-menu @endif"  href="{{ url('/about') }}">{{__('About')}}</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase @if( strpos (url()->current() , 'contact' ) ) active-menu @endif"  href="{{ url('/contact') }}">{{__('Contact')}}</a>
                  </li>
               </ul>
            </div>

            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               @if( isset($_SESSION["logged_in"]))
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}" title="{{__('Logout')}}" >
               <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
               </li>
               <li class="li2 mr-1 " style="display: inline-block;"><a href="{{ url('/edit-profile') }}" title="{{__('Edit Profile')}}" ><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;" ></i></a></li>
               @else
               <li class="li2 mr-1 " style="display: inline-block;"><a href="{{ url('/userlogin') }}" title="{{__('Login')}}" ><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;" ></i></a></li>
               @endif
               @if( isset($_SESSION["logged_in"]))
               <li class="li2 font-weight-bold ml-1" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}" title="{{__('Order History')}}" >
                  <i class="fa fa-history" aria-hidden="true"></i></span></a>
               </li>
               @endif
               
               {{--  ->count()    --}}
               @php
                   if(isset($_SESSION['id'])){
                      $cart_item_count = collect();

                     $cart_item_count1 = App\carts::where('is_approved' , 0)->where('session' , session()->getId())->get();

                     $cart_item_count2 = App\carts::where('is_approved' , 0)->where('cid' , $_SESSION['id'] )->get();

                     $cart_item_count = $cart_item_count1->merge($cart_item_count2);
                     $cart_item_count = $cart_item_count->unique();
                     $cart_item_count = $cart_item_count->count();
                   }else{
                     $cart_item_count = App\carts::where('session' , session()->getId())->where('cid' , null )->where('is_approved' , 0)->get()->count() ;
                   }
               @endphp
               {{--  @dd($cart_item_count->pluck('id'))  --}}
                              
               <li class="li2 ml-1" style="display: inline-block;"><a href="{{ url('/checkout') }}" title="{{__('Shopping Cart')}}"> 
                  @if($cart_item_count > 0)
                  <div class="cart-item-show">
                     {{$cart_item_count}}
                  </div>
                  @endif

                  <i class="fa fa-shopping-bag" aria-hidden="true"></i></span></a>
               </li>
              
               
               @if( session('locale') == 'ar')
               <li class="li2 font-weight-bold ml-3" style="display:inline"><a class="text-dark" href="{{route('change-language' , 'en')}}" title="{{__('Chnage Language')}}" >EN</a></li>
                  @else 
                  <li class="li2 font-weight-bold ml-3" style="display:inline"><a class="text-dark" href="{{route('change-language' , 'ar')}}"  title="Chnage Language" >AR</a></li>
                  @endif
               
               </ul>
            
            {{--  @else 
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               
               
                  
                  <div class="cart-item-show">
                     {{App\carts::where('session' , session()->getId())->where('is_approved' , 0)->get()->count()}}
                  </div>

                  <li class="li2 mr-3" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
                  </li>
                  @if( session('locale') == 'ar')
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{route('change-language' , 'en')}}">EN</a></li>
               @else
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{route('change-language' , 'ar')}}">AR</a></li>
               @endif
               </ul>
                  @endif  --}}
            </nav>
      </div>
      <!-- Navbar -->
      
	  <!-- navbar mbl view -->

<div class="container py-2 sideeen">
   <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a class="sidea" href="{{ route ('product')}}">{{__("Products")}}</a>
      <a class="sidea" href="{{ route ('about')}}">{{__("About")}}</a>
      <a class="sidea" href="{{ route ('gallery')}}">{{__("Gallery")}}</a>
      <a class="sidea" href="{{ route ('contact')}}">{{__("Contact Us")}}</a>
   </div>
<div class="row">
   <div id="sidebar">
      <span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
         <div class="bar"></div>
         <div class="bar"></div>
         <div class="bar"></div>
      </span>
   </div>
<div  id="Logoleft">
<a href="/">   <img src="{{asset('assets/images/New Project.svg')}}" id="Logo" class=" mblnavimg"  alt=""></a>
</div>
{{--  mobile navbar  --}}
<div id="colgal">
<ul class="ul2" id="icon"  style=" position: absolute; right : 40px ">
  
      
     
      
@if(!isset($_SESSION['logged_in']))

<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{route('userlogin')}}" title="{{__('Login')}}" >
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
@endif

@if(isset($_SESSION['logged_in']))


<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{route('order-history')}}" title="{{__('Order History')}}">
 {{--  -webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
   -webkit-text-stroke-color: black;   --}}
   
<i class="fa fa-history" onclick=" window.location = '{{route('order-history')}}' " style="color: black; font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
{{--  
webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  --}}
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:0PX">
 <a href="{{url('user-logout')}}" title="{{__('Logout')}}" >
 <i class="fa fa-sign-out" style="color: black; font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
@endif

<a class="language-change-link" @if( session('locale') == 'ar') href="{{route('change-language' , 'en' )}}" @else href="{{route('change-language' , 'ar' )}}" @endif > @if(session('locale') == 'ar' ) EN @else AR @endif </a>



{{--  -webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  --}}
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
   <a href="{{route('checkout')}}" title="{{__('Shopping Cart')}}" >

      @if($cart_item_count > 0)
   <div class="cart-item-show">
      {{$cart_item_count }}
   </div>
   @endif
	<i class="fa fa-shopping-bag fa-stack-1x " style="color: black;  font-size: 16px;margin-top:-2px;"></i>
 </a>
</li>




</li>
</ul>
</div>
</div>
</div>
	<!-- navbar mbl view ends-->
     
    @yield('content')


<!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->
<div class="container mb-3 mt-5 chotip border_top">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="{{asset('assets/images/vv.jpg')}}" class=" pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="https://www.instagram.com/vequick/">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>	  
	  
	  <!-- navbar mbl view -->
      <!-- Header -->
      <!-- Banner -->
      
      
	 
      
     
      <!-- footer -->
      <div class="container badip mb-3 mt-5">
      <div class="row">
         <div class="col-md-12">
            <h4 class="text-center fh4">&copy; 2020  vequick.com, Inc. All Rights Reserved.  <a class="p-p" href="#">Privacy Policy</a></h4>
            <div class="text-center mt-3">
               <a class="text-dark" href="#"><i class="fa fa-facebook fafoo"></i></a>
               <a class="text-dark" href="https://www.instagram.com/vequick/">        <i class="fa fa-instagram fafoo"></i>
               </a>        
               <a class="text-dark" href="#"><i class="fa fa-twitter fafoo"></i>
               </a>      
            </div>
         </div>
      </div>
      </div>


      <script>

         function myFunction() {
            swal("{{__('Please login to proceed with your order')}}!");
            //window.location.href = "/userlogin";
         }
      </script>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      {{--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>  --}}
      <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>  

      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
      
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
      
      <script src="assets/slick/slick-master/slick/slick.js"></script>
      <!-- Swiper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/js/swiper.min.js

      "></script>
      
      
      
      <!-- Initialize Swiper -->
      
      
      <script src="https://cdn.jsdelivr.net/npm/mobile-detect@1.4.4/mobile-detect.min.js"></script>

      
      @include('layouts.partials.errors')


      <script>
         //swal("{{__('Please login to proceed with your order')}}!");

        var md = new MobileDetect(
            'Mozilla/5.0 (Linux; U; Android 4.0.3; en-in; SonyEricssonMT11i' +
            ' Build/4.1.A.0.562) AppleWebKit/534.30 (KHTML, like Gecko)' +
            ' Version/4.0 Mobile Safari/534.30');

        // console.log( md.is('iPhone') );      // false
        // console.log( md.mobile() );          // 'Sony'


        $(document).ready(function(){
           if(md.is('iPhone')) {
                // $('body').css({ 'letter-spacing' : '-1px !important' });
                // $('button').css({ 'letter-spacing' : '-1px !important' });
                // $('div').css({ 'letter-spacing' : '-1px !important' });
                // $('h1').css({ 'letter-spacing' : '-1px !important' });
                // $('h2').css({ 'letter-spacing' : '-1px !important' });
                // $('h3').css({ 'letter-spacing' : '-1px !important' });
                // $('h4').css({ 'letter-spacing' : '-1px !important' });
                // $('h5').css({ 'letter-spacing' : '-1px !important' });
                // $('h6').css({ 'letter-spacing' : '-1px !important' });
                // $('a').css({ 'letter-spacing' : '-1px !important' });
                // $('p').css({ 'letter-spacing' : '-1px !important' });
           }
        });


         $(document).ready(function(){
            var product_id = {{session('added-pid') ?? 0 }};
            //$('.product-added-sign').show();
            //$('.add-to-cart-button-'+8).html('&#10004;');
            
            if(product_id > 0){
               $('.add-to-cart-button-'+product_id).html('&#10004;');
        
               //$('#product-added-'+product_id).show();
               setTimeout(() => { $('.add-to-cart-button-'+product_id).html('{{__("ADD TO CART")}}'); }, 3000);
            }
         });
        
         @php
             session()->forget('added-pid');
         @endphp
         


            //swal('asdasd');
         
            function out_of_stock(){
               swal({ text: '{{__("Sorry! This product is out of stock")}}' , timer: 3000});
             }

            function openNav(){
               document.getElementById("mySidenav").style.width="300px";
            }
            function closeNav(){
               document.getElementById("mySidenav").style.width="0px";
            }

      
            
      </script>
      
      @yield('scripts')
      
   </body>
</html>
