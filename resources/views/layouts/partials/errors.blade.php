<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

      <script>

{{--  
        swal({
          text: ' some gtsad ',
          timer: 2000,

        })  --}}
        @if(session('message'))
        swal({
          text: '{{__(session("message"))}}',
          timer: 3000,
        });

        @endif
        
        @if(session('success'))
        swal({
          text: '{{__(session("success"))}}',
          timer: 3000,
        });
        @endif

        @if(session('error'))
        swal({
          text: '{{__(session("error"))}}',
          timer: 3000,
        });
        @endif
        
        
        
        function myFunction() {
          swal("{{__('Please login to proceed with your order')}}!");
          setTimeout(() => {  window.location.href = "/userlogin"; }, 1000);
        }
      </script>
