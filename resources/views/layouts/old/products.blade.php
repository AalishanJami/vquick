<?php

use App\categories;

if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
  $id = $_SESSION["id"];
}
if(isset($_GET['arabic'])) {
  $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
  $_SESSION['language'] = 'english';
}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- StyleSheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- GoogleFonts -->
        <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">

    <!-- Slick slider -->
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
    <!-- Link Swiper s CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <title>Product</title>
	<link rel="stylesheet" href="assets/css/own-style.css">
  <style>
  .img-fluid {
    width: 340px !important;
    height: 330px !important;
  }
  .price-quantity{
    margin-top: -15px;
  }
  .view-category-form-button{
    cursor: pointer;
 }
  </style>
 
</head>
   <body>
      <!-- Header -->    
     <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
              	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a> 
			   <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}" style="border-bottom: 2px solid #627976;color: #627976;">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
            <ul class="ul2 ml-auto" id="Icons" style="margin-right: 0.5rem;">
              <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
                <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
              </li>
              <li class="li2" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                  <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
              </li>
              <li class="li2 font-weight-bold ml-1 mr-3" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}">
              <i class="fa fa-history" aria-hidden="true"></i></span></a>
              </li>

            
               <?php if($_SESSION['language'] == 'arabic') { ?>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?english">EN</a></li>
               <?php }
               else { ?>
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?arabic">AR</a></li>
               <?php } ?>
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
  
  
              <li class="li2  ml-1" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>

                  <li class="li2 mr-3" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
                  </li>
    
                  
               <?php if($_SESSION['language'] == 'arabic') { ?>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?english">EN</a></li>
               <?php }
               else { ?>
                  <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="?arabic">AR</a></li>
               <?php } ?>
               </ul>
            <?php } ?>
         </nav>
      </div>
    <!-- Navbar -->

    <!-- navbar mbl view -->
    <div class="container py-2 sideeen">
<div id="mySidenav" class="sidenav">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
<a class="sidea" href="Product.html">Products</a>
<a class="sidea" href="about.html">About</a>
<a class="sidea" href="gallery.html">Gallery</a>
<a class="sidea" href="contact.html">Contact Us</a>
</div>
<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="index.html">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="    position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="login.html">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="checkout.html">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>
<!-- navbar mbl view -->

<!-- Header -->

 <!-- section -->
 <div class="container my-5">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="font-weight-bold text-uppercase text-center our-product" >Our Products</h1>
        <h1 class="font-weight-bold text-uppercase our-product-mobile" >Our Products</h1>
      </div>

      {{--  @php
      $productsection = App\productsections::first();
      @endphp      
       
    @if( $productsection && $productsection->content == 'categories' )
    @php
        $categories = categories::all();
    @endphp  --}}
    @if(isset($categories))
    @foreach ($categories as $category)
    <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">
      
      <a class="view-category-form-button" onclick=" $('#category-{{$category->id}}-products').submit();" >
      <img src="files/{{ $category->image}}"  class="img-fluid pr_img" alt=""></a>
      
      <form method="GET" id="category-{{$category->id}}-products" action="/product-category" style="width:100%;">
        <input type="hidden" name="category" value="{{ $category->name}}">
        <div class="col-lg-12  col-md-7 col-sm-7 col-7 mt-1 product-button" style="margin:5% 0px;">
            </div>
        </form>
     
     <div class="row ">
      
        <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
          <?php if($_SESSION['language'] == 'english') { ?>
          <h3 class="slh3 mt-3 ">{{ $category->name}}</h3>
          <?php }
          else { ?>
            <h3 class="slh3 mt-3 ">{{ $category->arabic_name}}</h3>
          <?php } ?>
          <p class="slp3 mt-2 ">
            {{ $_SESSION['language'] == 'english' ?  substr($category->description,0,100)  :  substr($category->description_ar , 0 , 100)}} ...</p>
        </div>

      </div>
    </div>

    @endforeach
    {{--  @php
    $products = App\products::all()->where('is_approved', '1');
    @endphp  --}}
    @elseif(isset($products))

      @foreach ($products as $product)
	   <div class="col-lg-4 col-md-6 col-sm-6 col-6 mt-4">
        <img src="files/{{ $product->image}}"  class="img-fluid pr_img" alt="">
        <div class="row ">
        
          <div class="col-lg-12  col-md-12 col-sm-12 col-12 product-mobile" >
            <?php if($_SESSION['language'] == 'english') { ?>
            <h3 class="slh3 mt-3 ">{{ $product->name}}</h3>
            <?php }
            else { ?>
              <h3 class="slh3 mt-3 ">{{ $product->arabic_name}}</h3>
            <?php } ?>
            <p class="slp3 mt-2 ">
              {{ $_SESSION['language'] == 'english' ?  substr($product->description,0,100)  :  substr($product->description_ar , 0 , 100)}} ...</p>
            <h3 class="slh4 mt-3  price-product" ><b>{{ $product->price}} QAR</b></h3>
            <h3 class="slh4 price-quantity" ><b> Quantity {{ $product->stock_available}}</b></h3>
          </div>

          <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
            <form method="get" action="/carts">
              <div class='counter1'  style="margin-top:1px;">
                <div class='down' onclick='decreaseCount(event, this)'>-</div>
                <input type='text' id="quantity" name="quantity" class="counter_1" value='1'  >
                <div class='up' onclick='increaseCount(event, this)'>+</div>
              </div>
            </div>
            {{--  @dump($product->id)  --}}
            <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
              <input type="hidden" name="category" value="{{ $product->category }}" >
              <input type="hidden" name="image" value="{{ $product->image }}" >
              <input type="hidden" name="pid" value="{{ $product->id }}" >
              <input type="hidden" name="id" value="{{ $id ?? null }}" >
              <input type="hidden" name="name" value="{{ $product->name }}" >
              <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
              <input type="hidden" name="description" value="{{ $product->description }}" >
              <input type="hidden" name="price" value="{{ $product->price }}" >
              <button type="submit" name="submit" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
            </div>
          </form>
            
          {{--  <?php if( isset($_SESSION["logged_in"])) { ?>
          
            
            <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
            <form method="get" action="/carts">
              <div class='counter1'  style="margin-top:1px;">
                <div class='down' onclick='decreaseCount(event, this)'>-</div>
                <input type='text' id="quantity" name="quantity" class="counter_1" value='1'  >
                <div class='up' onclick='increaseCount(event, this)'>+</div>
              </div>
            </div>
            <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
              <input type="hidden" name="category" value="{{ $product->category }}" >
              <input type="hidden" name="image" value="{{ $product->image }}" >
              <input type="hidden" name="pid" value="{{ $product->id }}" >
              <input type="hidden" name="id" value="{{ $id }}" >
              <input type="hidden" name="name" value="{{ $product->name }}" >
              <input type="hidden" name="stock_available" value="{{ $product->stock_available }}" >
              <input type="hidden" name="description" value="{{ $product->description }}" >
              <input type="hidden" name="price" value="{{ $product->price }}" >
              <button type="submit" name="submit" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
            </div>
            </form>
            <?php }
            else { ?>
            <div class="col-lg-4  col-md-5 col-sm-5 col-5 mt-1 couuu" style="border:1px solid #d3aea6;border-radius: 5px;height:36px;" class="marbtn">
              <div class='counter1'  style="margin-top:1px;">
                <div class='down' onclick='decreaseCount(event, this)'>-</div>
                <input type='text' id="quantity[]" class="counter_1" value='1'  >
                <div class='up' onclick='increaseCount(event, this)'>+</div>
              </div>
            </div>
              <div class="col-lg-8  col-md-7 col-sm-7 col-7 mt-1 product-button" >  
                  <button onclick="myFunction()" class="btn-block btn-sli py-3  text-white " id="imgwidth" style="color: #eee2da;">ADD TO CART</button>
              </form>
            </div>
            <?php } ?>  --}}
        </div>
      </div>
      @endforeach
     
      @endif

      <div class="col-lg-12">
        <button class="btn-block btn-sli py-3  text-white show-more-home-mobile" id="imgwidth" style="color: #eee2da;">SHOW MORE</button>
      </div>
    </div>
  </div>

<div class="container show-more-product">
  
<div class="offset-lg-5 col-lg-3 text-center my-5 ">
  <button class="btnpro btn-block  py-2" style="        border:1px solid #d3aea6;
        color: #d3aea6;
        background-color: transparent;
        font-size: 20px;
        margin-top: 50px;
        border-radius: 5px;">Show All</button>
</div>

    </div>
  </div>
  <!-- section -->

   <!-- footer -->
      <!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->
<div class="container mb-3 mt-5 chotip border_top">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>
<!-- footer -->
<div class="container badip mb-3 mt-5">
  <div class="row">
    <div class="col-md-12">
      <h4 class="text-center fh4">&copy; 2020  vequick.com, Inc. All Rights Reserved.  Privacy Policy</h4>
      <div class="text-center mt-3">
        <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
<a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
</a>        
<a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
</a>      
    </div>
  </div>
</div>

    @include('layouts.partials.errors')

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
    function increaseCount(e, el) {
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    input.value = value;
    }
    function decreaseCount(e, el) {
    var input = el.nextElementSibling;
    var value = parseInt(input.value, 10);
    if (value > 1) {
    value = isNaN(value) ? 0 : value;
    value--;
    input.value = value;
    }
    }
    </script>
    <script>
    function openNav(){
      document.getElementById("mySidenav").style.width="300px";
    }
    function closeNav(){
      document.getElementById("mySidenav").style.width="0px";
    }
  </script>
    </script>
  </body>
</html>