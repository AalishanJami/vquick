<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- StyleSheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- GoogleFonts -->
       <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">

    <!-- Slick slider -->
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
    <!-- Link Swiper's CSS -->
  <!--   <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"> -->
    <title>About</title>
	<link rel="stylesheet" href="assets/css/own-style.css">
   </head>
   <body>
      <!-- Header -->    
      <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
                	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a>  
			   <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}" style="border-bottom: 2px solid #627976;color: #627976;">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
              <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
                <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2 font-weight-bold ml-1 mr-3" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}">
                <i class="fa fa-history" aria-hidden="true"></i></span></a>
                </li>
               
              
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2  mr-3" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="">AR</a></li>
               </ul>
            <?php } ?>
         </nav>
      </div>
    <!-- Navbar -->

    <!-- navbar mbl view -->
  <div class="container py-2 sideeen">
{{--  <div id="mySidenav" class="sidenav">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
<a class="sidea" href="Product.html">Products</a>
<a class="sidea" href="about.html">About</a>
<a class="sidea" href="gallery.html">Gallery</a>
<a class="sidea" href="contact.html">Contact Us</a>
</div>  --}}
@include('layouts.partials.mobile_nav')
<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="{{ url('/') }}">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{route('userlogin')}}">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="{{route("checkout")}}">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>

   <section class="head">
   <?php use App\aboutuses;
    $aboutuses = aboutuses::all(); ?>
    @foreach ($aboutuses as $aboutus)
   <h1 id="textright"><b>{{ $aboutus->title}}</b></h1>
   @endforeach
 </section>

    <!-- navbar mbl view -->

<!-- Header -->

    <!-- section -->
    @foreach ($aboutuses as $aboutus)
	  <div class="container text-center my-5">
      <div class="row">
        <div class="col-lg-6  col-md-6">
            <h1 class="text-left our-product about-heading" >{{ $aboutus->title}}</h1>
            <p class="about-para">{{ $aboutus->description}}</p>
        </div>
        <div class=" col-lg-6 col-md-6 px-3">
          <img style="width:80%" src="files/{{ $aboutus->image}}" alt="about-us-image">
        </div>
      </div>
    </div>
    @endforeach
    <div class="container-fluid abdiv mt-5" >
      <div class="container text-center about-container" class="divclr">
      <div class="row">
        <div class="col-md-4 about-div" >
          <img src="assets/images/ic1.PNG" class="img-fluid  h-50 aaaa" alt="">
          <img src="assets/images/ic3.PNG" class="img-fluid  bbbb"  style="width: 100px;height: auto;" alt="">
          <p class="ban2p2 mt-3 about-bottom-p" >
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi.
          </p>
        </div>
        <div class="col-md-4 about-div" >
          <img src="assets/images/ic2.PNG" class="img-fluid  h-50 aaaa" alt="">
          <img src="assets/images/ic4.PNG" class="img-fluid  bbbb" style="width: 140px;height: auto;" alt="">
          <p class="ban2p2 mt-3 about-bottom-p" >
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi.
          </p>
        </div>
        <div class="col-md-4 about-div" >
          <img src="assets/images/ic1.PNG" class="img-fluid  h-50 aaaa" alt="">
          <img src="assets/images/ic3.PNG" class="img-fluid bbbb"  style="width: 100px;height: auto;" alt="">
          <p class="ban2p2 mt-3 about-bottom-p">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi.
          </p>
        </div>
      </div>
    </div>
    </div>

      <!-- footer -->
      <div class="container mb-3 mt-5 chotip border_top">
  <div class="row">
    <div class="col-md-12 text-center">
<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">
      
      <div class="text-center mt-2">
       <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i>

 </a>          <a class="text-dark" href="">
<i class="fa fa-instagram fafoo"></i>
 </a> 
          <a class="text-dark" href="">
<i class="fa fa-twitter fafoo"></i>
 </a>      </div>
      <h4 class="text-center mt-3 fh4  color_grey">&copy; vequcick.com,Inc. <br> All rights reserved.Privacy Policy</h4>
    </div>
  </div>
</div>
<!-- footer -->
<div class="container badip mb-3 mt-5">
  <div class="row">
    <div class="col-md-12">
      <h4 class="text-center fh4">&copy; vequick.com,Inc.All rights reserved.Privacy Policy</h4>
      <div class="text-center mt-3">
        <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
<a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
</a>        
<a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
</a>      
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
    function increaseCount(e, el) {
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    input.value = value;
    }
    function decreaseCount(e, el) {
    var input = el.nextElementSibling;
    var value = parseInt(input.value, 10);
    if (value > 1) {
    value = isNaN(value) ? 0 : value;
    value--;
    input.value = value;
    }
    }
    </script>
    <script>
    function openNav(){
      document.getElementById("mySidenav").style.width="300px";
    }
    function closeNav(){
      document.getElementById("mySidenav").style.width="0px";
    }
  </script>
    </script>
  </body>
</html>