<aside class="main-sidebar" style="padding-top: 5%;">
    <section class="sidebar">

      <ul class="sidebar-menu" data-widget="tree">
        <li class="@if(strpos(url()->current() , 'home')) active @endif" ><a href="{{ url('/home') }}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
        <li class="@if(strpos(url()->current() , 'admins')) active @endif"><a href="{{ url('/admins') }}"><i class="fa fa-link"></i> <span>Admins</span></a></li>
        <li class="@if(strpos(url()->current() , 'regusers')) active @endif"><a href="{{ url('/regusers') }}"><i class="fa fa-link"></i> <span>Customers</span></a></li>
        <li class="@if(strpos(url()->current() , 'categories')) active @endif"><a href="{{ url('/categories') }}"><i class="fa fa-link"></i> <span>Categories</span></a></li>
        <li class="@if(strpos(url()->current() , 'products')) active @endif"><a href="{{ url('/products') }}"><i class="fa fa-link"></i> <span>Products</span></a></li>
        <!--<li><a href="{{ url('/orders') }}"><i class="fa fa-link"></i> <span>Orders</span></a></li>-->
        <li class="@if(strpos(url()->current() , 'orderstatuses') || strpos(url()->current() , 'order-filter')) active @endif"><a href="{{ url('/orderstatuses') }}"><i class="fa fa-link"></i> <span>Orders</span></a></li>

        <li class="treeview @if(strpos(url()->current() , 'productsections' ) || strpos(url()->current() , 'aboutuses' ) || strpos(url()->current() , 'galleries' ) || strpos(url()->current() , 'shippings' ) || strpos(url()->current() , 'contactuses' )  ) menu-open active @endif ">
          <a href="#"><i class="fa fa-link"></i> <span>Configurations</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu" @if(strpos(url()->current() , 'productsections' ) || strpos(url()->current() , 'aboutuses' ) || strpos(url()->current() , 'galleries' ) || strpos(url()->current() , 'shippings' ) || strpos(url()->current() , 'contactuses' ) || strpos(url()->current() , 'gallery-images' )  ) style="display:block" @endif  >
            <li class="@if(strpos(url()->current() , 'productsections')) active @endif" ><a href="{{ url('/productsections') }}"><i class="fa fa-link "></i> <span>Product Section</span></a></li>
            <!--<li><a href="{{ url('/homepages') }}"><i class="fa fa-link"></i> <span>Home Page</span></a></li>-->
            <li class="@if(strpos(url()->current() , 'aboutuses')) active @endif" ><a href="{{ url('/aboutuses') }}"><i class="fa fa-link"></i> <span>About Us</span></a></li>
            <li class="@if(strpos(url()->current() , 'contactuses')) active @endif" ><a href="{{ url('/contactuses') }}"><i class="fa fa-link"></i> <span>Contact Us</span></a></li>
            {{--  <li class="@if(strpos(url()->current() , 'galleries')) active @endif" ><a href="{{ url('/galleries') }}"><i class="fa fa-link"></i> <span>Slider Images</span></a></li>  --}}
            <li class="@if(strpos(url()->current() , 'gallery-images')) active @endif" ><a href="{{ url('/gallery-images') }}"><i class="fa fa-link"></i> <span>Slider Image Gallery</span></a></li>
            <li class="@if(strpos(url()->current() , 'shippings')) active @endif" ><a href="{{ url('/shippings') }}"><i class="fa fa-link"></i> <span>Shipping Fee</span></a></li>
          </ul>
        </li>
      </ul>
    </section>
  </aside>
