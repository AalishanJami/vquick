@extends('layouts.main')



@section('title')
<title>Checkout</title>
@endsection


@section('styles')
    
<style>

  .counter1 {
    width:90px !important;
  }

  .btn-danger:focus {
    border: none;
    box-shadow: none;

  }
  .remove-cart-button{
    background-color: white;
    border: none;
    box-shadow: none;
  }
  .remove-cart-button:focus {
    border: none;
    box-shadow: none;
  }
  .checkout-list-p{
    line-height: 18px !important;
  }

  .swal-footer{
    display: block;
  }
  .swal-footer{
    text-align: center;
  }
  .swal-button--guest{
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }
  
  .swal-button--login{
    color: #999999;
  }
  
 
  .swal-button--guest:focus {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
    box-shadow : none !important;
  }
  .swal-button--guest:hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }  

  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
  
  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
</style>
  @endsection

@section('content')
    

<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if(isset($_SESSION["id"])){
  $id = $_SESSION["id"];
}
$price=0;
$z=0;
?>
<!-- Header -->

       <div class="container mt-5 badip">
      <div class="row no1">
        <div class="col-md-6">
          <h1 class="chch1 checkout-h1">{{__('Checkout')}}</h1>
        </div>
        <div class="col-md-6">
          <h2 class="text-right"><a class="chch2 checkout-h2" href="/product">{{__('Continue Shopping')}}</a></h2>
        </div>
      </div>
    </div>
    <div class="container mb-4 mt-3 badip">
      <div class="row no2">
        <div class="col-md-6">
          <p class="chcp1 checkout-main-row">{{__('Items')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Quantity')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Price')}}</p>
        </div>

      </div>
      {{--  @dd($carts)  --}}
      <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
      @foreach ($carts as $cart)
      <div class="row">
        <div class="col-md-2">
          <img src="files/{{ $cart->image}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
        </div>
        <div class="col-md-4 pt-5">
          <h2 class="chch2 checkout-list-h2 ">{{ $cart->product->proper_name}}</h2>
          <p class="chcp1 checkout-list-p " style="line-height: 18px !important" >{{  Str::limit($cart->product->proper_description,100) }}</p>
        </div>
        <div class="col-md-3 pt-5" >
          <div class='counter-checkout'>
            <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
              {{--  <input type='text' value='{{ $cart->quantity }}'  >  --}}
              <div id="quantity-buttons" style="border:1px solid #d3aea6 ; border-radius: 5px; height: 36px; "
              <div class='counter1'  style="margin-top:1px;">
                <div id="down" class='down' onclick='decreaseCount(event, this ,  {{$cart->id }})'>-</div>
                <input type='text' value='{{ $cart->quantity }}' id="quantity" name="quantity" class="counter_1" value='1'  >
                <div class='up' id="up" onclick='increaseCount(event, this , {{$cart->id}} )'>+</div>
              </div>
              
            <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
            </div>
          </div>
          <div class="col-md-2 " style="padding-top: 62px;">
            <h2 class="chch2 c-price cart-price-{{$cart->id}}" >QR {{ $cart->price}}</h2>
          </div>
          <div class="col-md-1">  
            <form id="cart-{{$cart->id}}" action="{{ route('carts.destroy',$cart->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <a href="javascript:;" onclick=" $('#cart-{{$cart->id}}').submit(); " class="remove-cart-button"><i class="fa fa-close chchfa"></i></a>
            </form>
          </div>
        </div>
        <hr class ="no3" style="background-color: #d9d9d9;">
        <?php
        $cart_price=$cart->price;
        $price+= $cart_price;
        ?>
        @endforeach
      </div>
    </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  @foreach($carts as $cart)
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->product->proper_name}}</h2>
      <a href="#" onclick="delete_cart({{$cart->id}})" > <i class="fa fa-times-circle" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i></a>
      
      <form id="delete-cart-{{$cart->id}}"   action="{{ route('carts.destroy',$cart->id) }}" method="POST">
        @csrf
        @method('DELETE')
      </form>
      
        <button type="submit"  class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>

			{{--  <i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>  --}}
			<p class="chcp1" style="font-size: 12px;">{{ Str::limit($cart->product->proper_description , 200)}} </p>
			<h2 class="chch2 m-price "><strong class="cart-price-{{$cart->id}}" >QR {{$cart->price}}</strong></h2>

			<div class='counter mt-1'>
				<div class='decrease-counter'  onclick='decreaseCount(event, this , {{$cart->id}} )'>-</div>
				<input type='text' value='{{$cart->quantity}}' class="chotoo" >
				<div class='up' style="background-color: #dcdcdc;font-size: 20px;width: 30px;height: 30px;padding-top: 2px;padding-left: 9px !important;" onclick='increaseCount(event, this , {{$cart->id}})'>+</div>
			</div>
		</div>
  </div>
  @php
  $cart_price=$cart->price;
  $price+= $cart_price;

  @endphp
  <hr style="background-color: #d9d9d9;">
  @endforeach
	
  {{--  carts end here  --}}

</div>

@php
$total_price = $shippings->first()->fee + array_sum($carts->pluck('price')->toArray());
@endphp

  @if($carts->count() > 0)
  <div class="container px-4 mt-2">
    <div class="row mt-5">
      <div class="col-md-12">
        <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
        <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shippings->first()->fee}}</h2>
        
      </div>
      <div class="col-xs-12 col-12 col-sm-12 col-md-3 col-lg-3">
        <form method="get" action="/order-cart" class="place-order-form">
          <input type="hidden" name="total_price" class="total-amount" value="{{ $total_price }}" >
          <input type="hidden" name="id" value="{{ $id ?? session()->getId() }}" >
    
        <select class="form-control" id="payment_method" name="payment_method" required >
          <option disabled>{{__("Select Payment Option")}}</option>
          <option value="cash on delivery" >{{__('Cash On Delivery')}}</option>
          <option value="PayPal" >{{__("PayPal")}}</option>
        </select>
        </form>
      </div>
      
      @foreach ($shippings as $shipping)
      <div class="col-md-6 ">
        {{--  <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shipping->fee}}</h2>  --}}
      </div>
      @endforeach
    </div>
	  <hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right sub-total ">QR {{ array_sum($carts->pluck('price')->toArray()) }}</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v total-amount">QR  <span class="total-amount"> {{$total_price}}</span></span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v " >QR <span class="total-amount">{{$total_price}} </span> </h2>
		</div>
    <div class="col-md-12 mt-4" >  
      <button type="button" @if(isset($_SESSION['id'])) onclick="submit_order_form()"  @else  onclick="ask_how_to_checkout()" @endif  class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
    </div>
	</div>
</div>
@else
<div class="container px-4 mt-2">
	<div class="row mt-5">
		<div class="col-md-6">
		  <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
		</div>
		<div class="col-md-6 ">
		  <h2 id="ddddd" class="shiping-fee text-right">QR 0</h2>
		</div>
	</div>
	<hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right">QR 0</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v">QR 0</span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v">QR 0</h2>
		</div>
    <div class="col-md-12 mt-4" >
      @if($carts->count() > 0)
      <button class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
      @endif
      </form>
    </div>
	</div>
</div>
@endif

<!-- MOBILE DESIGN ENDS -->
<div class="col-md-12 mt-4">
        @if(isset($carts) && $carts->count() > 0)
          <button type="button" @if(isset($_SESSION['id'])) onclick="submit_order_form()"  @else  onclick="ask_how_to_checkout()" @endif  class="m-check-out-button uncfocused-item">{{__("PLACE ORDER")}}</button>
        @endif
        </div>



@if(isset($carts) && $carts->count() > 0)

<form action="{{route('guest-signup')}}" id="guest-signup-form" method="GET" >
  <input type="hidden" class="total-amount" name="total_price" value="{{$total_price}}" />
  <input type="hidden" id="payment_method_guest" name="payment_method" value="cash on delivery" />
</form>

@endif



<div class="modal fade" id="sadad-payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- sadad-payment --}}
        {{-- <form action="https://sadadqa.com/webpurchase" method="post"> --}}
        <form action="{{route('sadad-payment')}}" method="post">
          @csrf
          <span class="tagline"><h4>Merchant Id</h4></span>
          <input type="text" class="form-control"  name="merchant_id" id="merchant_id" value="7648426">
              
          <span class="tagline"><h4>Secret Key</h4></span>
          <input type="text" class="form-control"  name="secret_key" id="secret_key" value="MVFd65askQv7oVIT">
      
          <span class="tagline"><h4>Amount</h4></span>
          <input type="text" class="form-control"  name="TXN_AMOUNT" id="amount" value="10">
              
          <span class="tagline"><h4>Order Id</h4></span>
          <input type="text"  class="form-control" name="ORDER_ID" id="order_id" value="10001">
      
      
          <span class="tagline"><h4>MOBILE_NO</h4></span>
          <input type="text"  class="form-control" name="MOBILE_NO" id="MOBILE_NO" value="03147637613">
      
          <span class="tagline"><h4>Callback URL</h4></span>
          <input type="text"  class="form-control" name="CALLBACK_URL" id="callback_url" value="http://127.0.0.1:8000/checkout">
          
          <div class="col-md-6 col-sm-6">
              <span class="tagline"><h4>Product 1</h4></span>
              <div class="input-group">
                  Quantity
                  <input type="hidden" id="itemname" class="form-control" name="productdetail[0][itemname]" value="Product One">
                  <input type="text" id="quantity" value="1"  class="form-control" name="productdetail[0][quantity]" >
                  Price
                  <input type="text" id="price" value="10"  class="form-control" name="productdetail[0][amount]">
              </div>
          </div>
          <div class="col-md-6 col-sm-6">
              <span class="tagline"><h4>Product 2</h4></span>
              <div class="input-group">
                  Quantity
                  <input type="hidden"  class="form-control"  id="itemname" name="productdetail[1][itemname]" value="Product Two">
                  <input type="text"  class="form-control" value="1"  id="quantity" name="productdetail[1][quantity]" >
                  Price
                  <input type="text" class="form-control" value="10"  id="price" name="productdetail[1][amount]">
              </div>
          </div>
          <input type="submit" value="Submit">
        </div>
        <div class="modal-footer">
          {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
          <button type="submit" class="btn btn-secondary">Save changes</button>
        </div>
      </form>    
    </div>
  </div>
</div>


@endsection


@section('scripts')
<script>
  
  function submit_order_form(){

    $('#sadad-payment').modal('show');
    
    $.ajax({
      url: "https://sandboxapi.sadadqa.com/api/",
      type: "POST",
      data: {
          merchant_id: '7803971',
          secret_key: 'BCYJENm9z6hxyeaD',
          order_id: 'asdasd11001203120',
          website: 'www.vquick.qa',
          txn_amount: '1.5',
          mobile_no: '12345',
          productdetail: [ { "order_id": "85", "itemname": "Product 2", "amount": "800", "totalamount": "800", "quantity": "1", "type": "line_item", "item_meta": null } ],
        },
      dataType: "json",
      success: function(res){
        console.log(res)
        swal("{{__('Request success')}}");
      }

    });
    
    //$('.place-order-form').submit();

  }

  function ask_how_to_checkout(){
  

    swal("{{__('How would you link to continue')}}", {
      buttons: {
      login: {
        text: "{{__('Login')}}",
        value: "login",
      },
      
      guest : {
        text: "{{__('Guest')}}",
        value: "guest",
      },
    },
  })
  .then((value) => {
    switch (value) {
   
      case "login":
        $('.place-order-form').submit();
        break;
        
      case "guest":
      $('#guest-signup-form').submit();
      break;
   
        default:
      }
    });
  }
  
  $('#payment_method').on('change' , function(){
    $('#payment_method_guest').val( $('#payment_method').val() )
  })

  function delete_cart(id){
    console.log(id)
    document.getElementById("delete-cart-"+id).submit();    
  }


  function increaseCount(e, el , cart_id) {
    
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    
    //el.onclick = false;


    $.ajax({
      url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value + 1) ,
      success: function(res){

        $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
        $('.total-amount').html( res.total_amount);
        $('.total-amount').val( res.total_amount);
        $('.sub-total').html('QR ' +  res.sub_total);
        value++;
        input.value = value;
  

    }
    }).fail(function(errors){
      //swal(errors.responseJSON.message);

      if(errors.responseJSON.error_code == 1){

        swal({text: '{{__("Category restriction exceeded")}}' , timer:5000});
  
      }
      if(errors.responseJSON.error_code == 2){
  
        swal({ text: '{{__("This item stock is not enough")}}', timer:5000});
  
      }
  
    });
    
    //el.onclick = true;
    //$(el).on('click',increaseCount(event , this , cart_id)); 

  }
  function decreaseCount(e, el , cart_id) {
  var input = el.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 1) {
  value = isNaN(value) ? 0 : value;

  //el.onclick = false;


  $.ajax({
    url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value - 1) ,
    success: function(res){

      $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
      $('.total-amount').html( res.total_amount);
      $('.total-amount').val( res.total_amount);

      $('.sub-total').html('QR ' +  res.sub_total);

      value--;
      input.value = value;


  }
  }).fail(function(errors){

    if(errors.responseJSON.error_code == 1){

      swal({ text: '{{__("Category restriction exceeded")}}' , timer:5000 });

    }
    if(errors.responseJSON.error_code == 2){

      swal( { text: '{{__("This item stock is not enough")}}' , timer:5000 });

    }

  });

  }
  }
  </script>
  <script>
  function openNav(){
    document.getElementById("mySidenav").style.width="300px";
  }
  function closeNav(){
    document.getElementById("mySidenav").style.width="0px";
  }
</script>
<?php
  if(isset($_SESSION['message'])) 
  { 
    $login_message= $_SESSION['message'];?>
    <script>
        swal("{{ $login_message }}");
        setTimeout(() => {  window.location.href = "{{ url('/checkout') }}"; }, 1000);
    </script><?php
    unset($_SESSION['message']);
  }
?>
  </script>
@endsection