@extends('categories.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Category</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('categories.update',$category->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Name:</strong>
                <input type="text" class="form-control" name="name" value="{{ $category->name }}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Arabic Name:</strong>
                <input type="text" class="form-control" name="arabic_name" value="{{ $category->arabic_name }}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>English Description:</strong>
                  <textarea class="form-control" name="description" placeholder="Enter Category English" >{{ $category->description }}</textarea>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Arabic Description:</strong>
                  <textarea  class="form-control" name="description_ar" placeholder="Enter Category Arabic" >{{ $category->description_ar }}</textarea>
                </div>
              </div>
        
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Restriction Level:</strong>
                <input type="text" class="form-control" value="{{ $category->restriction_level }}" name="restriction_level" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Enable/Disable:</strong>
                    <select class="form-control" name="is_approved">
                        <?php
                        $a=0;$a1=0;$b=0;$b1=0;
                        if($category->is_approved == '1') {
                        $a="Enabled";
                        $a1='1';
                        $b="Disable";
                        $b1='0';
                        } else {
                        $a="Disabled";
                        $a1='0';
                        $b="Enable";
                        $b1='1';
                        }?>
                        <option selected value="<?= $a1 ?>"><?= $a ?></option>
                        <option value="<?= $b1 ?>"><?= $b ?></option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection