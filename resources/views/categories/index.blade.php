@extends('categories.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Categories</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Arabic Name</th>
          <th>Restriction Level</th>
          <th>Image</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($categories as $category)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $category->name }}</td>
          <td>{{ $category->arabic_name }}</td>
          <td>{{ $category->restriction_level }}</td>
          <td><img style="width:100px;height: 100px;" src="files/<?= $category->image ?>" /></td>
          <td>
            <form id="delete-form-{{$category->id}}" action="{{ route('categories.destroy',$category->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('categories.show',$category->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$category->id}})" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
    @if($categories->links())
    
    {{$categories->links()}}
    
    @endif
      
@endsection