@extends('categories.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Categories</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $category->name }}<br>
                <strong>Arabic Name:</strong>
                {{ $category->arabic_name }}<br>
                <strong>Restriction Level:</strong>
                {{ $category->restriction_level }}<br>
                <strong>Image:</strong>
                <?php $path= '/files/'. $category->image; ?>
                <img style="width:200px;height: 200px;" src="<?= $path ?>" />
            </div>
        </div>
    </div>
@endsection