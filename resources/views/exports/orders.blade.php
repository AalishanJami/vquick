<table>
    <thead>
    <tr>
        <th style="width: 20px;" >Order Number</th>
        <th style="width: 20px;" >Customer Name</th>
        <th style="width: 20px;" >Status</th>
        <th style="width: 20px;" >Price</th>
        <th style="width: 20px;" >Payment Status</th>
        <th style="width: 20px;" >Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->customer->full_name }}</td>
            <td>{{ $order->status }}</td>
            <td>{{ $order->total_price }}</td>
            <td>{{ $order->payment_method }}</td>
            <td>{{ $order->created_at->toDateString() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>