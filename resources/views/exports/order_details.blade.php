<table>
    <thead>
        
    <tr>
        <th style="width: 20px;" >Order Number</th>
        <th style="width: 20px;" >Customer Name</th>
        <th style="width: 20px;" >Status</th>
        <th style="width: 20px;" >Price</th>
        <th style="width: 20px;" >Payment Status</th>
        <th style="width: 20px;" >Date</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->customer->full_name }}</td>
            <td>{{ $order->status }}</td>
            <td>{{ $order->total_price }}</td>
            <td>{{ $order->payment_method }}</td>
            <td>{{ $order->created_at->toDateString() }}</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        
    <tr>
        <th style="width: 20px;" >#</th>
        <th style="width: 20px;" >Product Name</th>
        <th style="width: 20px;" >Description</th>
        <th style="width: 20px;" >Quantity</th>
        <th style="width: 20px;" >Stock Available</th>
        <th style="width: 20px;" >Price</th>
    </tr>
    </thead>
    <tbody>
        @foreach($order->carts as $k => $cart)
        <tr>
            <td>{{ $k + 1 }}</td>
            <td>{{ $cart->name }}</td>
            <td>{{ $cart->description }}</td>
            <td>{{ $cart->quantity }}</td>
            <td>{{ $cart->product->stock_available }}</td>
            <td>{{ $cart->price }}</td>
        </tr>
        @endforeach
    </tbody>
</table>