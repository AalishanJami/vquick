<table>
    <thead>
    <tr>
        <th style="width: 20px;" >#</th>
        <th style="width: 20px;" >Name</th>
        <th style="width: 20px;" >Email</th>
        <th style="width: 20px;" >Mobile Number</th>
        <th style="width: 20px;" >Address</th>
        <th style="width: 20px;" >Zone</th>
        <th style="width: 20px;" >Street</th>
        <th style="width: 20px;" >Building</th>
  </tr>
    </thead>
    <tbody>
    @foreach($customers as $k => $customer)
        <tr>
            <td>{{ $k+1 }}</td>
            <td>{{ $customer->full_name }}</td>
            <td> {{ $customer->email }} </td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->address }}</td>
            <td>{{ $customer->zone }}</td>
            <td>{{ $customer->street }}</td>
            <td>{{ $customer->building }}</td>
          </tr>
    @endforeach
    </tbody>
</table>