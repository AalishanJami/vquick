<table>
    <tr>
        <th style="width: 5px;" >#</th>
        <th style="width: 20px;" >Name</th>
        <th style="width: 20px;" >Arabic Name</th>
        <th style="width: 20px;" >Description</th>
        <th style="width: 20px;" >Arabic Description</th>
        <th style="width: 20px;" >Price</th>
        <th style="width: 20px;" >Stock Available</th>
        <th style="width: 20px;" >Category</th>
      </tr>
      @foreach ($products as $k => $product)
      <tr>
        <td>{{ ++$k }}</td>
        <td>{{ $product->name }}</td>
        <td>{{ $product->arabic_name }}</td>
        <td>{{ $product->description }}</td>
        <td>{{ $product->description_ar }}</td>
        <td>{{ $product->price }} QAR</td>
        <td>{{ $product->stock_available }}</td>
        <td>{{ $product->category }}</td>
    </tr>
    @endforeach
</table>