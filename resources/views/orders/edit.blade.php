@extends('orders.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Category</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('orders.update', $order->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
         <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Order Number:</strong>
          <input type="text" class="form-control" name="order_number" value="{{ $order->order_number }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Payment Status:</strong>
          <select class="form-control" name="payment_method">
            <option selected value="{{ $order->payment_method }}">{{ $order->payment_method }}</option>
            <option value="cash">Cash on delivery</option>
            <option value="paid">Paid</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Quantity:</strong>
          <input type="text" class="form-control" name="quantity" value="{{ $order->quantity }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Available:</strong>
          <input type="text" class="form-control" name="available" value="{{ $order->available }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Status:</strong>
          <input type="text" class="form-control" name="status" value="{{ $order->status }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Full Name:</strong>
          <input type="text" class="form-control" value="{{ $order->full_name }}" name="full_name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Address:</strong>
          <input type="text" class="form-control" value="{{ $order->address }}" name="address" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Date Time:</strong>
          <input type="text" class="form-control" name="date" value="{{ $order->date }}" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Product Name:</strong>
          <input readonly type="text" class="form-control" value="{{ $order->product_name }}" name="product_name" required="required"/>
        </div>
      </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection