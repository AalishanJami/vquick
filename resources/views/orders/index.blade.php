@extends('orders.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('orders.create') }}"> Create New Orders</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <form action="/order-filter" method="GET">
    @csrf
			<div class="col-12" style="display:grid;grid-template-columns:20% 20% 20% 20% 10%;grid-column-gap:1%;">
				<div class="form-group">
					<input type="text" class="form-control" name="search_order_number" placeholder="Search Order Number">
				</div>
				<div class="form-group">
          <select class="form-control" name="search_payment_status">
            <option selected disabled>Search Payment Status</option>
            <option value="cash">Cash on Delivery</option>
            <option value="paid">Paid</option>
          </select>
				</div>
        <div class="form-group">
          <select class="form-control" name="status">
            <option selected disabled>Search Status</option>
            <option value="cash">Pending</option>
            <option value="paid">Confirmed</option>
            <option value="out of delivery">Out of Delivery</option>
            <option value="delivered">Delivered</option>
            <option value="cancelled">Cancelled</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="product_name">
            <option selected disabled>Select Product</option>
            <?php use App\products;
              $products = products::all(); ?>
              @foreach ($products as $product)
                <option value="{{ $product->name}}">{{ $product->name}}</option>
              @endforeach
          </select>
        </div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Search</button>
				</div>
			</div>
    </form>
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Order Number</th>
          <th>Payment Status</th>
          <th>Quantity</th>
          <th>Available</th>
          <th>Status</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Date Time</th>
          <th>Product Name</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($orders as $order)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $order->order_number }}</td>
          <td>{{ $order->payment_method }}</td>
          <td>{{ $order->quantity }}</td>
          <td>{{ $order->available }}</td>
          <td>{{ $order->status }}</td>
          <td>{{ $order->full_name }}</td>
          <td>{{ $order->address }}</td>
          <td>{{ $order->date }}</td>
          <td>{{ $order->product_name }}</td>
          <td>
            <form id="delete-form-{{$order->id}}" action="{{ route('orders.destroy',$order->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('orders.show',$order->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('orders.edit',$order->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$order->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
      
@endsection