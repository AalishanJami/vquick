@extends('homes.layout') 
@section('content')

   



<script>
    swal("Here's the title!", "...and here's the text!");

</script>

   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
      

<div class="row">

    <div class="col-12">
      @if($orderstatuses->count() == 0)
        <h3 class="text-success"><i class="fa fa-sticky-note"></i> No pending orders</h3>

      @endif
      @if($orderstatuses->count() > 0)
        <h3 class="text-success"><i class="fa fa-sticky-note"></i> Pending Orders</h3>

        <table class="table table-bordered">
            <tr>
              <th>Order Number</th>
              <th>Total Price</th>
              <th>Payment Status</th>
              <th>Status</th>
              <th>Customer Name</th>
              <th>Address</th>
              <th>Date Time</th>
              <th width="280px">Action</th>
            </tr>
            @foreach ($orderstatuses as $orderstatus)
            <tr>
              <td>{{ $orderstatus->id }}</td>
              <td>{{ $orderstatus->total_price }} QAR</td>
              <td>{{ $orderstatus->payment_method }}</td>
              <td>
                @if($orderstatus->status == 'pending')
                <span class="label label-danger">Pending</span>
                
                @elseif($orderstatus->status == 'confirmed')
    
                <span class="label label-primary">Confirmed</span>
                
                @elseif($orderstatus->status == 'out of delivery')
                
                <span class="label label-warning">Out Of Delivery</span>
                
                @elseif($orderstatus->status == 'delivered')
                
                <span class="label label-success">Delivered</span>
    
                @elseif($orderstatus->status == 'cancelled')
                
                <span class="label label-danger">Cancelled</span>
                
                @endif
              
              </td>
              <?php $cid=$orderstatus->cid;
              $regusers = App\regusers::all()->where('id',$cid); ?>
              @foreach ($regusers as $reguser)
              <?php $cname=$reguser->full_name;
              $caddress=$reguser->address;
              $czone=$reguser->zone;
              $cstreet=$reguser->street;
              $cbuilding=$reguser->building;
              $cdate=$reguser->created_at; ?>
              @endforeach
              <td>{{ $cname }}</td>
              <td>Building: {{ $cbuilding }}, Street: {{ $cstreet }}, Zone: {{ $czone }}, {{ $caddress }} </td>
              <td>{{ $orderstatus->created_at }}</td>
              <td>
                <form id="delete-order-form-{{$orderstatus->id}}" action="{{ route('orderstatuses.destroy',$orderstatus->id) }}" method="POST">
                  <a class="btn btn-info btn-sm" href="{{ route('orderstatuses.show',$orderstatus->id) }}">View</a>
                  <a class="btn btn-primary btn-sm" href="{{ route('orderstatuses.edit',$orderstatus->id) }}">Edit</a>
                  @csrf
                  @method('DELETE')
                  <button type="button" onclick="ask_order_delete({{$orderstatus->id}})" class="btn btn-danger btn-sm">Delete</button>
    
                </form>
              </td>
            </tr>
            @endforeach
        </table>
        
        @endif

      @if($products->count() ==  0)
      <h3 class="text-success"><i class="fa fa-sticky-note"></i> No low stock notifications</h3>
      @endif
      @if($products->count() > 0)

        <hr>
        <h3 class="text-danger"><i class="fa fa-exclamation-circle"></i> Low Stock Nottification</h3>
        <table class="table table-hover">
            <tr>
              <th>Name</th>
              <th>Image</th>
              <th>Stock Available</th>
              <th>Category</th>
              <th width="280px">Action</th>
            </tr>
            @foreach ($products as $k => $product)
            <tr>
              <td>{{ $product->name }}</td>
              <td><img style="width:100px;height: 100px;" src="files/<?= $product->image ?>" /></td>
              <td>{{ $product->stock_available }}</td>
              <td>{{ $product->category }}</td>
              <td>
                <form id="delete-prodcut-form-{{$product->id}}" action="{{ route('products.destroy',$product->id) }}" method="POST">
                  <a class="btn btn-sm btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
                  <a class="btn btn-sm btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
                  @csrf
                  @method('DELETE')
                  <button type="button" onclick="ask_product_delete({{$product->id}})" class="btn btn-danger btn-sm">Delete</button>
                  
    
                </form>
              </td>
            </tr>
            @endforeach
        </table>
        @endif

        </div>
        
     
</div>
    
    

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    function ask_product_delete(id){
        
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
                $('#delete-prodcut-form-'+id).submit();
            }
          });   
    }
    
    function ask_order_delete(id){
        
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
                $('#delete-order-form-'+id).submit();
            }
          });   
    }

</script>


@endsection