<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>VQuick | Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">

       
    
    </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('/header');
    @include('/aside');
    <div class="content-wrapper">
        <section class="content-header">
            <h1 style="float:left;">
                Dashboard
            </h1>
        </section>
        <div style="padding:5%;">
            @yield('content')
        </div>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#">vQuick</a>.</strong> All rights reserved.
    </footer>
</div>


<script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/adminlte.js')}}"></script>
    <script src="{{asset('js/demo.js')}}"></script>
    <script src="{{asset('js/dashboard3.js')}}"></script>
    <script src="{{asset('js/Chart.min.js')}}"></script>



</body>
</html>