@extends('orderstatuses.layout') 
@section('content')


<style>
  .badge-danger{
    background-color: red;
    color:white;
  }
  .badge-success{

  }
  .badge-primary{

  }
  .badge-warning{

  }
</style>

    <?php use App\regusers;?>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <form action="/order-filter" method="GET">
    @csrf
			<div class="col-12" style="display:grid;grid-template-columns: 20% 20% 20% 20%;grid-column-gap:3%;">
				<div class="form-group">
					<input type="text" class="form-control" @if(isset($filters) && isset($filters['search_order_number'])) value="{{$filters['search_order_number']}}" @endif name="search_order_number" placeholder="Search Order Number">
				</div>
				<div class="form-group">
           
          <input type="text" class="form-control" name="name"  @if(isset($filters) && isset($filters['name'])) value="{{$filters['name']}}" @endif placeholder="Customer Name">

        </div>
        
        <div class="form-group">
          <select class="form-control" name="search_payment_status">
            <option selected disabled>Payment Status</option>
            <option value="Cash on Delivery" @if(isset($filters['search_payment_status']) && $filters['search_payment_status'] == 'pending' ) selected @endif >pending</option>
            <option value="paid" @if(isset($filters['search_payment_status']) && $filters['search_payment_status'] == 'paid' ) selected @endif >Paid</option>
          </select>
				</div>
        <div class="form-group">
          <select class="form-control" name="status">
            <option selected disabled>Status</option>
            <option value="pending" @if(isset($filters['status']) && $filters['status'] == 'pending' ) selected @endif >Pending</option>
            <option value="confirmed"  @if(isset($filters['status']) && $filters['status'] == 'confirmed' ) selected @endif >Confirmed</option>
            <option value="out for delivery"  @if(isset($filters['status']) && $filters['status'] == 'out for delivery' ) selected @endif >Out for Delivery</option>
            <option value="delivered"  @if(isset($filters['status']) && $filters['status'] == 'delivered' ) selected @endif >Delivered</option>
            <option value="cancelled"  @if(isset($filters['status']) && $filters['status'] == 'cancelled' ) selected @endif >Cancelled</option>
          </select>
        </div>
        
	
        <div class="form-group">
        </div>
        <div class="form-group">
          <input type="date" name="lower_date" class="form-control"  @if(isset($filters) && isset($filters['lower_date'])) value="{{$filters['lower_date']}}" @endif style="line-height: 1.375rem;" />
        </div>
        <div class="form-group">
          <input type="date" name="upper_date" class="form-control"  @if(isset($filters) && isset($filters['upper_date'])) value="{{$filters['upper_date']}}" @endif style="line-height: 1.375rem;" />
        </div>
        <div class="form-group">
					<button type="submit" class="btn btn-primary">Search</button>
          <a class="btn btn-danger" href="{{route('orderstatuses.index')}}">X Clear</a>
        </div>
			</div>
    </form>

    @php
        if(isset($filtered_orders)){
          $orderstatuses = $filtered_orders;
        }
    @endphp
    <a class="btn btn-success float-right mb-2" href="{{route('export-orders' , json_encode($orderstatuses->pluck('id')->toArray()) )}}" >Export Orders</a>

    <a class="btn btn-success float-right mb-2 mr-3" href="{{route('export-orders-products' , json_encode($orderstatuses->pluck('id')->toArray()) )}}" >Export Products Report</a>
    
    
    <table class="table table-bordered">
        <tr>
          <th>Order Number</th>
          <th>Customer Name</th>
          <th>Payment Status</th>
          <th>Status</th>
          <th>Address</th>
          <th>Date Time</th>
          <th>Total Price</th>
          <th width="280px">Action</th>
        </tr>
        
        @foreach ($orderstatuses->sortByDesc('created_at') as $orderstatus)
        <tr>
          <td>{{ $orderstatus->id }}</td>

          <td>{{ $orderstatus->customer->full_name }}</td>

          <td>{{ $orderstatus->payment_method }}</td>
          <td>
            @if($orderstatus->status == 'pending')
            <span class="label label-danger">Pending</span>
            
            @elseif($orderstatus->status == 'confirmed')

            <span class="label label-primary">Confirmed</span>
            
            @elseif($orderstatus->status == 'out for delivery')
            
            <span class="label label-warning">Out For Delivery</span>
            
            @elseif($orderstatus->status == 'delivered')
            
            <span class="label label-success">Delivered</span>

            @elseif($orderstatus->status == 'cancelled')
            
            <span class="label label-danger">Cancelled</span>
            
            @endif
          
          </td>
          <?php $cid=$orderstatus->cid;
          $regusers = regusers::all()->where('id',$cid); ?>
          @foreach ($regusers as $reguser)
          <?php $cname=$reguser->full_name;
          $caddress=$reguser->address;
          $czone=$reguser->zone;
          $cstreet=$reguser->street;
          $cbuilding=$reguser->building;
          $cdate=$reguser->created_at; ?>
          @endforeach
          <td>Building: {{ $cbuilding }}, Street: {{ $cstreet }}, Zone: {{ $czone }}, {{ $caddress }} </td>
          <td>{{ $orderstatus->created_at }}</td>
          <td>{{ $orderstatus->total_price }} QAR</td>

          <td>
            <form id="delete-form-{{$orderstatus->id}}" action="{{ route('orderstatuses.destroy',$orderstatus->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('orderstatuses.show',$orderstatus->id) }}">View</a>
              <a class="btn btn-primary" href="{{ route('orderstatuses.edit',$orderstatus->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$orderstatus->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endforeach
    </table>

    @if(
    $orderstatuses instanceof \Illuminate\Pagination\Paginator ||
    $orderstatuses instanceof Illuminate\Pagination\LengthAwarePaginator
  )

    {{$orderstatuses->links() ?? ''}}
@endif



@endsection



@section('scripts')

<script>
  
</script>

@endsection