@extends('orderstatuses.layout') 
@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> View Order Details</h2>
            </div>
            <div class="pull-right" style="width: 175px;">
                @if($carts->first()->order_id)
                <a class="btn btn-success mr-2" href="{{ route('export-order' , $carts->first()->order_id) }}" > Export</a>
                @endif
                <a class="btn btn-primary" href="{{ url()->previous() }}"> Back</a>
            </div>
        </div>
    </div>
    {{--  @foreach ($carts as $cart)
    <?php $oid=$cart->order_id; ?>
    @endforeach  --}}
    <form action="/product-filter" method="GET">
    @csrf
        <div class="col-12" style="display:grid;grid-template-columns:70% 20%;grid-column-gap:5%;">
            <div class="form-group">
                <select class="form-control" name="product_name">
                    <option selected disabled>Select Product</option>
                    <?php use App\products;
                    $products = products::all(); ?>
                    @foreach ($products as $product)
                        <option  @if(isset($product_name) && $product_name == $product->name) selected @endif value="{{ $product->name}}">{{ $product->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="hidden" name="oid" value="{{ $orderstatus->id }}" >
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="{{url('orderstatuses' , $orderstatus->id)}} " class="btn btn-danger">Clear</a>
            </div>
        </div>
    </form>
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Product Image</th>
          <th>Product Name</th>
          <th>Quantity</th>
          <th>Price</th>
        </tr>

        @foreach ($carts as $cart)
        <tr>
          <td>{{ ++$i }}</td>
          <td><img style="width: 150px;height: 100px;" src="/files/{{ $cart->image }}"></td>
          <td>{{ $cart->name }}</td>
          <td>{{ $cart->quantity }}</td>
          <td>{{ $cart->price }} QAR</td>
        </tr>
        @endforeach
    </table>
@endsection