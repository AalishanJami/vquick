@extends('orders.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New Orders</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  

  <form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Order Number:</strong>
          <input type="text" class="form-control" name="order_number" placeholder="Enter Order Number" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Payment Status:</strong>
          <select class="form-control" name="payment_method">
            <option value="cash">Cash on delivery</option>
            <option value="paid">Paid</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Quantity:</strong>
          <input type="text" class="form-control" name="quantity" placeholder="Enter Quantity" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Available:</strong>
          <input type="text" class="form-control" name="available" placeholder="Enter  Available" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Status:</strong>
          <select class="form-control" name="status">
            <option value="cash">Pending</option>
            <option value="paid">Confirmed</option>
            <option value="out for delivery">Out for Delivery</option>
            <option value="delivered">Delivered</option>
            <option value="cancelled">Cancelled</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Full Name:</strong>
          <input type="text" class="form-control" placeholder="Enter Full Name" name="full_name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Address:</strong>
          <input type="text" class="form-control" placeholder="Enter Address" name="address" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Date Time:</strong>
          <input type="date" class="form-control" name="date" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Product Name:</strong>
          <select class="form-control" name="product_name">
            <option selected disabled>Select Product</option>
            <?php use App\products;
              $products = products::all(); ?>
              @foreach ($products as $product)
                <option value="{{ $product->name}}">{{ $product->name}}</option>
              @endforeach
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>
  @endsection