
@extends('layouts.main')



@section('title')
<title>About US</title>
@endsection


@section('styles')
    
<style>
  .textarea-line{
    white-space: pre-wrap;
    {{--  white-space: pre-line;  --}}
}


</style>
@endsection

@section('content')

@php
    
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}



use App\aboutuses;
$aboutuses = aboutuses::all();
@endphp


<!-- section -->
@foreach ($aboutuses as $aboutus)
<div class="container text-center my-5">
  <div class="row">
    <div class="col-lg-6  col-md-6">
        <h1 class="text-left our-product about-heading" >{{ session('locale') == 'ar' ? $aboutus->title_ar : $aboutus->title }}</h1>
        
        <h4 class="d-sm-block d-xs-block d-md-none d-lg-none d-xl-none " >{{ session('locale') == 'ar' ? $aboutus->title_ar : $aboutus->title }}</h4>

        <p class="about-para textarea-line">{{ session('locale') == 'ar' ? $aboutus->description_ar : $aboutus->description }}</p>
    </div>
    <div class=" col-lg-6 col-md-6 px-3">
      <img style="width:80%" src="files/{{ $aboutus->image}}" alt="about-us-image">
    </div>
  </div>
</div>
@endforeach
<div class="container-fluid abdiv mt-5" >
  <div class="container text-center about-container" class="divclr">
  <div class="row">
    <div class="col-md-4 about-div" >
      <img src="assets/images/ic1.PNG" class="  h-50 aaaa" alt="">
      <img src="assets/images/ic3.PNG" class="  bbbb"  style="width: 100px;height: auto;" alt="">
      <p class="ban2p2 mt-3 about-bottom-p" >
        {{__('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi')}}.
      </p>
    </div>
    <div class="col-md-4 about-div" >
      <img src="assets/images/ic2.PNG" class="  h-50 aaaa" alt="">
      <img src="assets/images/ic4.PNG" class="  bbbb" style="width: 140px;height: auto;" alt="">
      <p class="ban2p2 mt-3 about-bottom-p" >
        {{__('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi')}}.
      </p>
    </div>
    <div class="col-md-4 about-div" >
      <img src="assets/images/ic1.PNG" class="  h-50 aaaa" alt="">
      <img src="assets/images/ic3.PNG" class=" bbbb"  style="width: 100px;height: auto;" alt="">
      <p class="ban2p2 mt-3 about-bottom-p">
        {{__('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste doloribus earum porro ipsa architecto, sunt quisquam esse at suscipit deleniti dicta, excepturi ab assumenda ullam voluptatibus quam laudantium fugiat animi')}}.
      </p>
    </div>
  </div>
</div>
</div>


@endsection
