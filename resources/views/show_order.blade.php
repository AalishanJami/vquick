@extends('layouts.main')



@section('title')
<title>Checkout</title>
@endsection


@section('styles')
    
<style>

  .counter1 {
    width:90px !important;
  }

  .btn:focus {
    border: none;
    box-shadow: none;
  }

  .btn-danger:focus {
    border: none;
    box-shadow: none;

  }
  .remove-cart-button{
    background-color: white;
    border: none;
    box-shadow: none;
  }
  .remove-cart-button:focus {
    border: none;
    box-shadow: none;
  }
  .checkout-list-p{
    line-height: 18px !important;
  }
</style>
  @endsection

@section('content')
    

<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if(isset($_SESSION["id"])){
  $id = $_SESSION["id"];
}
$price=0;
$z=0;
?>
<!-- Header -->

       <div class="container mt-5 badip">
      <div class="row no1">
        <div class="col-md-6">
          <a href="{{route('order-history')}}" class="chch2 checkout-h2" >
          <h1 class="chch1 checkout-h1"># {{$order->id}}</h1></a>
        </div>
        <div class="col-md-6">
        </div>
      </div>
    </div>
    <div class="container mb-4 mt-3 badip">
      <div class="row no2">
        <div class="col-md-6">
          <p class="chcp1 checkout-main-row">{{__('Items')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Quantity')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Price')}}</p>
        </div>

      </div>
      {{--  @dd($carts)  --}}
      <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
      @foreach ($order->carts as $cart)
      <div class="row">
        <div class="col-md-2">
          <img src="{{asset('files/'.$cart->image )}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
        </div>
        <div class="col-md-4 pt-5">
          <h2 class="chch2 checkout-list-h2 ">{{ $cart->product->proper_name}}</h2>
          <p class="chcp1 checkout-list-p " style="line-height: 18px !important" >{{  Str::limit($cart->product->proper_description,100) }}</p>
        </div>
        <div class="col-md-3 d-flex align-items-center" >
          {{$cart->quantity}}
          </div>
          <div class="col-md-2 d-flex align-items-center" >
            <h2 class="chch2 c-price cart-price-{{$cart->id}}" >QR {{ $cart->price}}</h2>
          </div>
          <div class="col-md-1">  
           
          </div>
        </div>
        <hr class ="no3" style="background-color: #d9d9d9;">
        <?php
        $cart_price=$cart->price;
        $price+= $cart_price;
        ?>
        @endforeach
      </div>
    </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  @foreach($order->carts as $cart)
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->product->proper_name}}</h2>
			<p class="chcp1" style="font-size: 12px;">{{ Str::limit($cart->product->proper_description , 200)}} </p>
      <h2 class="chch2 m-price "><strong class="cart-price-{{$cart->id}}" >QR {{$cart->price}}</strong></h2>
      {{__("Quantity")}} : {{$cart->quantity}}
			
		</div>
  </div>
  @php
  $cart_price=$cart->price;
  $price+= $cart_price;

  @endphp
  <hr style="background-color: #d9d9d9;">
  @endforeach
	
  {{--  carts end here  --}}

</div>

  @if($order->carts->count() > 0)
  <div class="container px-4 mt-2">
    <div class="row mt-5">
      <div class="col-md-12">
        <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
        <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shipping->fee}}</h2>
        
      </div>
      <div class="col-xs-12 col-12 col-sm-12 col-md-3 col-lg-3">
        <select disabled class="form-control">
          <option disabled>{{__("Select Payment Option")}}</option>
          <option>{{__('Cash On Delivery')}}</option>
          <option disabled>{{__("Online (Not Availble)")}}</option>
        </select>
      </div>
 
      @php
          
      $total_price = $shipping->fee + array_sum($order->carts->pluck('price')->toArray());
      @endphp
    </div>
	  <hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right sub-total ">QR {{ array_sum($order->carts->pluck('price')->toArray()) }}</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v total-amount">QR  <span class="total-amount"> {{$total_price}}</span></span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v " >QR <span class="total-amount">{{$total_price}} </span> </h2>
		</div>
    <div class="col-md-12 mt-4" >  
    </div>
	</div>
</div>
@else
<div class="container px-4 mt-2">
	<div class="row mt-5">
		<div class="col-md-6">
		  <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
		</div>
		<div class="col-md-6 ">
		  <h2 id="ddddd" class="shiping-fee text-right">QR 0</h2>
		</div>
	</div>
	<hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right">QR 0</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v">QR 0</span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v">QR 0</h2>
		</div>
    <div class="col-md-12 mt-4" >
      @if($order->carts->count() > 0)
      <button class="btn-block btnchch py-3 checkout-btn" id="placebutn">{{__('PLACE ORDER')}}</button>
      @endif
      </form>
    </div>
	</div>
</div>
@endif

<!-- MOBILE DESIGN ENDS -->
<div class="col-md-12 mt-4">
     
        </div>


@endsection


@section('scripts')
<script>


  function delete_cart(id){
    console.log(id)
    document.getElementById("delete-cart-"+id).submit();    
  }


  function increaseCount(e, el , cart_id) {
    
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    
    //el.onclick = false;


    $.ajax({
      url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value + 1) ,
      success: function(res){

        $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
        $('.total-amount').html( res.total_amount);
        $('.total-amount').val( res.total_amount);
        $('.sub-total').html('QR ' +  res.sub_total);
        value++;
        input.value = value;
  

    }
    }).fail(function(errors){
      //swal(errors.responseJSON.message);

      if(errors.responseJSON.error_code == 1){

        swal({text: '{{__("Category restriction exceeded")}}' , timer:5000});
  
      }
      if(errors.responseJSON.error_code == 2){
  
        swal({ text: '{{__("This item stock is not enough")}}', timer:5000});
  
      }
  
    });
    
    //el.onclick = true;
    //$(el).on('click',increaseCount(event , this , cart_id)); 

  }
  function decreaseCount(e, el , cart_id) {
  var input = el.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 1) {
  value = isNaN(value) ? 0 : value;

  //el.onclick = false;


  $.ajax({
    url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value - 1) ,
    success: function(res){

      $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
      $('.total-amount').html( res.total_amount);
      $('.total-amount').val( res.total_amount);

      $('.sub-total').html('QR ' +  res.sub_total);

      value--;
      input.value = value;


  }
  }).fail(function(errors){

    if(errors.responseJSON.error_code == 1){

      swal({ text: '{{__("Category restriction exceeded")}}' , timer:5000 });

    }
    if(errors.responseJSON.error_code == 2){

      swal( { text: '{{__("This item stock is not enough")}}' , timer:5000 });

    }

  });

  }
  }
  </script>
  <script>
  function openNav(){
    document.getElementById("mySidenav").style.width="300px";
  }
  function closeNav(){
    document.getElementById("mySidenav").style.width="0px";
  }
</script>
<?php
  if(isset($_SESSION['message'])) 
  { 
    $login_message= $_SESSION['message'];?>
    <script>
        swal("{{ $login_message }}");
        setTimeout(() => {  window.location.href = "{{ url('/checkout') }}"; }, 1000);
    </script><?php
    unset($_SESSION['message']);
  }
?>
  </script>
@endsection