<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected $fillable = [
        'image', 'name','arabic_name','restriction_level','is_approved' , 'description' , 'description_ar'
    ];



    public function getProperNameAttribute($value){

        if(session('locale') == 'ar'){
            return $this->arabic_name;
        }else{
            return $this->name;
        }
    }

    public function getProperDescriptionAttribute($value){

        if(session('locale') == 'ar'){
            return $this->description_ar;
        }else{
            return $this->description;
        }
    }



    
}