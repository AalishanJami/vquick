<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class regusers extends Model
{
    protected $fillable = [
        'email','password','full_name','phone','address','zone','street','building' , 'is_guest'
    ];


    public function orders(){
        return $this->hasMany(orderstatuses::class , 'cid');
    }
}
