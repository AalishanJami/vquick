<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productsections extends Model
{
    protected $fillable = [
        'title','content' , 'title_ar',
    ];



    
    public function getProperTitleAttribute($value){

        if(session('locale') == 'ar'){
            return $this->title_ar;
        }else{
            return $this->title;
        }
    }



}
