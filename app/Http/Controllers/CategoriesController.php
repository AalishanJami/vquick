<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\categories;
class CategoriesController extends Controller
{
    public function index()
    {
        //$makes = makes::latest()->paginate(5);
        $categories = categories::latest()->simplePaginate(5);
        return view('categories.index',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('categories.create');
    }
  
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $file = $request->file('image');
        $name=$file->getClientOriginalName();
        $file->move(public_path().'/files/', $name);
        $categories= new categories();
        $categories->image=$name;
        $categories->description = request('description');
        $categories->description_ar = request('description_ar');
        $categories->restriction_level = request('restriction_level');
        $categories->name = request('name');
        $categories->arabic_name = request('arabic_name');
        $categories->save();
        return redirect()->route('categories.index')
                        ->with('success','Categories created successfully.');
    }

    public function show(categories $category)
    {
        return view('categories.show',compact('category'));
    }
   
    public function edit(categories $category)
    {
        return view('categories.edit',compact('category'));
    }
    public function update(Request $request, categories $category)
    {
        // dd($request->all());
        $category->update($request->all());
        return redirect()->route('categories.index')
                        ->with('success','Categories updated successfully');
    }

    public function destroy(categories $category)
    {
        $category->delete();
  
        return redirect()->route('categories.index')
                        ->with('success','Categories deleted successfully');
    }
}
