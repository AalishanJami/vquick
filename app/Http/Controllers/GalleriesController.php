<?php

namespace App\Http\Controllers;

use App\galleries;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleriesController extends Controller
{
    public function index()
    {
        $galleries = galleries::latest()->simplePaginate(5);
        return view('galleries.index',compact('galleries'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('galleries.create');
    }
  
    public function store(Request $request)
    {

        // dd($request->all());

        foreach($request->images as $image){

            $file = $image;
            $name=$file->getClientOriginalName();
            $file->move(public_path().'/files/', time(). $name);
            $galleries= new galleries();
            $galleries->title = time().$name;
            $galleries->image = time().$name;
            $galleries->save();
        }
        return redirect()->route('gallery-images')
                        ->with('success','Gallery created successfully.');
    }

    public function show(galleries $gallery)
    {
        return view('galleries.show',compact('gallery'));
    }

    public function destroy(galleries $gallery)
    {
        $gallery->delete();
  
        return redirect()->route('galleries.index')
                        ->with('success','Gallery deleted successfully');
    }


    public function showGallery(){
        $galleries = galleries::all();

        return view('galleries.gallery' , compact('galleries'));
    }


    public function saveDropzoneImage(Request $request){

        // $image = $request->file('file');
        // $extension = ".".$image->getClientOriginalExtension();
        // $imageName = basename($image->getClientOriginalName(), $extension).time();
        // $fileName = $imageName.$extension;

        // $image->move(public_path('images/company_images'),$fileName);
        
        // $imageUpload = Company_Gallery::create([
        //    'company_id' => auth()->user()->company->id,
        //    'name' => $fileName, 
        // ]);

        $file = $request->file('file');
        $name=$file->getClientOriginalName();
        $file->move(public_path().'/files/', $name);
        $galleries= new galleries();
        // $galleries->title = request('title');
        $galleries->title = $file->getClientOriginalName();
        $galleries->image = $name;
        $galleries->save();


        if($galleries){
            return $galleries;
            // return response()->json(['success'=>$imageName]);
        }else{
            return response()->json(['error'=> 'Problem while uploading']);
        }

    }

        public function deleteDropzoneImage($id)
        {
            // dd($id);
            $file = galleries::findOrFail($id);

            $path=public_path().'/files/'.$file->image;
            if (file_exists($path)) {
                File::delete($path);
                // unlink($path);
            }
            $file->delete();

            return 'success';  
        }



        public function showGalleryImages(){
            $images = galleries::all();
            return view('gallery' , compact('images'));
        }
        
}
