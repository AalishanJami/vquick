<?php

namespace App\Http\Controllers;

use App\products;
use App\categories;
use App\productsections;
use Illuminate\Http\Request;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    public function index()
    {
        $products = products::latest()->simplePaginate(15);
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('products.create');
    }
  
    public function store(Request $request)
    {

        
        
        $request->validate([
            'name' => 'required',
        ]);

        $products= new products();
        $products->description = request('description');
        $products->description_ar = request('description_ar');
        $products->name = request('name');
        $products->arabic_name = request('arabic_name');
        $products->price = request('price');
        $products->stock_available = request('stock_available');
        $products->category = request('category');
        $products->is_approved = request('is_approved');
        
        if($request->has('image')){
            $file = $request->file('image');
            // $name=$file->getClientOriginalName();
            $name = time() . '_' .$products->id . '.'. $request->image->getClientOriginalExtension();
            $file->move(public_path().'/files/', $name);
            
            $products->image=$name;
        }
        
        $products->save();

        return redirect()->route('products.index')
                        ->with('success','Products created successfully.');
    }

    public function show(products $product)
    {
        return view('products.show',compact('product'));
    }


    public function showProducts(Request $request){

        $products = products::where('is_approved', '1')->orderBy('created_at' , 'desc')->take(3)->get();

        // dd($request->all());
        
        if ($request->ajax()) {

            
        $productsection = productsections::first(); 

        if( $productsection && $productsection->content == 'categories'){

            $categories = categories::orderBy('created_at' , 'desc')->skip($request->skip)->take(4)->get();

            foreach($categories as $category){
                if( session('locale') == 'ar' ){
                    $category->proper_name = $category->arabic_name;
                    $category->proper_description = $category->arabic_name;
                }else{
                    $category->proper_name = $category->name;
                    $category->proper_description = $category->description;
                }
            }
            return [
                'categories' => $categories ,
                // 'products' => view('product')->with(compact('products'))->render(),
                // 'next_page' => $products->nextPageUrl()
            ];

        }else{


            $products = products::skip($request->skip)->orderBy('created_at' , 'desc')->take(4)->get();

            foreach($products as $product){
                if( session('locale') == 'ar' ){
                    $product->proper_name = $product->arabic_name;
                    $product->proper_description = $product->arabic_name;
                }else{
                    $product->proper_name = $product->name;
                    $product->proper_description = $product->description;
                }
            }
            return [
                'products' => $products ,
                // 'products' => view('product')->with(compact('products'))->render(),
                // 'next_page' => $products->nextPageUrl()
            ];

        }

    
        }

        $productsection = productsections::first(); 

        if( $productsection && $productsection->content == 'categories'){
            $categories = categories::orderBy('created_at' , 'desc')->get()->take(3);
            if($request->show_all){
                
                $categories = categories::orderBy('created_at' , 'desc')->get();
                $categories->show_all = 1;
            }

            $productsection = productsections::first();

            return view('product',compact('categories' , 'productsection'));
            
        }else{

            // $products = products::all()->where('is_approved', '1');
            if($request->show_all){
                $products = products::where('is_approved', '1')->orderBy('created_at' , 'desc')->get();
                $products->show_all = 1;
            }else{
                $products = products::where('is_approved', '1')->orderBy('created_at' , 'desc')->get()->take(3) ;
            }

            $productsection = productsections::first();

            return view('product',compact('products' , 'productsection'));

        }

        
        return redirect()->back()->with('error' , 'Some Problem ');

        // return view('product',compact('products'));
        // $category_name=request('category');
        // $products = products::all()->where('category', $category_name)->where('is_approved', '1');

    }
    
    public function show1(Request $request)
    {
        
        $category_name=request('category');
        $products = products::all()->where('category', $category_name)->where('is_approved', '1');

        return view('product',compact('products'));
    }
   
    public function showCategoryProducts(Request $request)
    {
        
        $category_name=request('category');
        $products = products::all()->where('category', $category_name)->where('is_approved', '1');
        // dd($products , $category_name );

        return view('product',compact('products'));
    }
   
    public function edit(products $product)
    {
        return view('products.edit',compact('product'));
    }
    
    public function update(Request $request, products $product)
    {
  
        // dump($request->image->getClientOriginalExtension() );
        // dump($request->image->getClientOriginalName() );
        // dump( time()  );
        // dump(  );

        // dd($request->all());

        if($request->has('image')){

            $file = $request->file('image');
            $name = time() . '_' .$product->id . '.'. $request->image->getClientOriginalExtension();
            // $name = $file->getClientOriginalName();
            $file->move(public_path().'/files/', $name);
            $product->image = $name;
            $product->save();
        }

        $product->description = request('description');
        $product->description_ar = request('description_ar');
        $product->name = request('name');
        $product->arabic_name = request('arabic_name');
        $product->price = request('price');
        $product->stock_available = request('stock_available');
        $product->category = request('category');
        $product->is_approved = request('is_approved');
        $product->save();


        // $product->update($request->all());

  
        
        return redirect()->route('products.index')
                        ->with('success','Products updated successfully');
    }

    public function destroy(products $product)
    {
        $product->delete();
        return redirect()->route('products.index')
                        ->with('success','Products deleted successfully');
    }


    public function exportProducts($ids)
    {
        $ids = json_decode($ids);

        $products = products::whereIn('id' , $ids )->get();

        return Excel::download(new ProductExport($products), 'products.xlsx');
    }
}
