<?php

namespace App\Http\Controllers;

use App\carts;
use App\orders;
use App\shippings;
use App\orderstatuses;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = orders::latest()->simplePaginate(15);
        return view('orders.index',compact('orders'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function search(Request $request)
    {
        $order_number = request('search_order_number');
        $payment_method = request('search_payment_status');
        $status = request('status');
        $product_name = request('product_name');
        if($order_number ==null && $status== null && $product_name ==null) {
            $orders = orders::all()->where('payment_method', $payment_method);
        }
        else if($payment_method ==null && $status== null && $product_name ==null) {
            $orders = orders::all()->where('order_number', $order_number);
        }
        else if($payment_method ==null && $status== null && $order_number ==null) {
            $orders = orders::all()->where('product_name', $product_name);
        }
        else {
            $orders = orders::all()->where('status', $status);
        }
        return view('orders.index',compact('orders'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('orders.create');
    }
  
    public function store(Request $request)
    {
        $orders= new orders();
        $orders->order_number = request('order_number');
        $orders->payment_method = request('payment_method');
        $orders->quantity = request('quantity');
        $orders->available = request('available');
        $orders->status = request('status');
        $orders->full_name = request('full_name');
        $orders->address = request('address');
        $orders->date = request('date');
        $orders->product_name = request('product_name');
        $orders->save();
        return redirect()->route('orders.index')
                        ->with('success','Orders created successfully.');
    }

    public function show(orders $order)
    {
        return view('orders.show',compact('order'));
    }
   
    public function edit(orders $order)
    {
        return view('orders.edit',compact('order'));
    }
    
    public function update(Request $request, orders $order)
    {
  
        $order->update($request->all());
  
        return redirect()->route('orders.index')
                        ->with('success','order updated successfully');
    }

    public function destroy(orders $order)
    {
        $order->delete();
  
        return redirect()->route('orders.index')
                        ->with('success','order deleted successfully');
    }


    public function showHistory(){
        if(isset($_SESSION['id'])){

        $carts = carts::where('cid' , $_SESSION['id'] )->where('is_approved' , 1)->orderBy('created_at' , 'desc') ->get();

        $orders = orderstatuses::where('cid' ,$_SESSION['id'] )->orderBy('created_at' , 'desc') ->get();
        // dd($carts->pluck('is_approved') );
        if( isset($_SESSION["logged_in"])) {
            
        }

        return view('order_history' , compact('carts' , 'orders'));
        }else{
            return redirect('/');
        }

    }
    public function showOrderDetails($order_id){
        
        if(!isset($_SESSION['id'])){
            return redirect()->back()->with('error' , 'You Are Not Logged in');
        }

        $carts = carts::where('cid' , $_SESSION['id'] )->where('is_approved' , 1)->orderBy('created_at' , 'desc') ->get();

        $order = orderstatuses::findOrFail($order_id);
        $shipping = shippings::first();
        
        if($order && $order->cid ==  $_SESSION['id'] ){
            return view('show_order' , compact('order' , 'shipping'));

        }else{
            return redirect()->back()->with('error' , 'Record Not Found');

        }
        // dd($order);

    }
}
