<?php

namespace App\Http\Controllers;

use App\carts;
use App\regusers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\CustomerExport;
use Maatwebsite\Excel\Facades\Excel;

class RegusersController extends Controller
{
    public function index()
    {
        $regusers = regusers::latest()->simplePaginate(10);
        return view('regusers.index',compact('regusers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function login()
    {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        $email = request('email');
        $password = request('password');
        $regusers = regusers::all()->where('email', $email)->first();


        if($regusers == null) {
            $_SESSION['message'] = "User with that email doesn't exist!";
            return redirect('userlogin')->with('error' , "User with that email doesn't exist" );
        }
        else if($regusers != null && $regusers->password == $password) {
            $_SESSION['logged_in'] = true;
            $_SESSION['id'] = $regusers->id;
            // $_SESSION['message'] = "Logged in successfully";
            if(session('checkout') == 'checkout' ){
                session()->forget('checkout');
                return redirect('/checkout')->with('success' , 'Logged in successfully' );

            }else{
                return redirect('/')->with('success' , 'Logged in successfully' );

            }
        }
        else {
            $_SESSION['message'] = "Your Password is not correct. Try again";
            return redirect('userlogin')->with('error' , 'Your Password is not correct. Try again' ) ;
        }
    }
    
    public function logout()
    {
        $session_carts = carts::where('session' , session()->getId() )->where('is_approved' , 0) ->get();
        
        foreach($session_carts as $cart){
            $cart->delete();
        }

        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        session_destroy();
        // $_SESSION['message'] = "Logged out successfully";
        return redirect('/')->with('success' , 'Logged out successfully');
        // return view('welcome');
    }
   
    public function create()
    {
        return view('regusers.create');
    }
  
    public function store(Request $request)
    {

        
        $guest_check = regusers::where('email' , $request->email)->first();
        // dd($request->all() , $guest_check);
        if(isset($guest_check) && $guest_check->is_guest == 1){
            $guest_check->password = request('password');
            $guest_check->full_name = request('full_name');
            $guest_check->phone = request('phone');
            $guest_check->address = request('address');
            $guest_check->zone = request('zone');
            $guest_check->street = request('street');
            $guest_check->building = request('building');
    
            $guest_check->save();
            
            if(!isset($_SESSION)) 
            { 
                session_start(); 
            }

            $_SESSION['logged_in'] = true;
            $_SESSION['id'] = $guest_check->id;


            return redirect('/')->with('success' , 'Logged in successfully' );

        }

        $request->validate([
            'email' => 'email | unique:regusers,email',
        ]);

        // dd($request->all());
        $page=0;
        $regusers= new regusers();
        $regusers->email = request('email');
        $page = request('page');
        $regusers->password = request('password');
        $regusers->full_name = request('full_name');
        $regusers->phone = request('phone');
        $regusers->address = request('address');
        $regusers->zone = request('zone');
        $regusers->street = request('street');
        $regusers->building = request('building');
        $regusers->save();
        if($page == 0) {
            return redirect()->route('regusers.index')
            ->with('success','User created successfully.');
        }
        else {
            if(!isset($_SESSION)) 
            { 
                session_start(); 
            }
            if($regusers){

                $_SESSION['logged_in'] = true;
                $_SESSION['id'] = $regusers->id;
            }

            if(session('checkout') == 'checkout' ){
                session()->forget('checkout');
                return redirect('/checkout')->with('success' , 'Logged in successfully' );

            }
            
            $_SESSION['message'] = "Signup Successful";
            return redirect('/');
        }
        
    }

    public function show(regusers $reguser)
    {
        return view('regusers.show',compact('reguser'));
    }
   
    public function edit(regusers $reguser)
    {
        return view('regusers.edit',compact('reguser'));
    }

    public function editProfile(){

        if(!isset($_SESSION['logged_in'])){
            return redirect()->back() ->with('error' , "Already Logged in" );
        }
        $user = regusers::find($_SESSION['id']);
        if($user){
            return view('edit_profile' , compact('user') );
        }else{
            return redirect()->back() ->with('error' , "Please login to continue" );
        }

    }
    
    public function update(Request $request, regusers $reguser)
    {

        // dd($request->all());
  
        // $reguser->update($request->all());
        $request->validate([
            'email' => 'email | unique:regusers,email,'.$reguser->id,
        ]);

        $reguser->full_name = $request->full_name;
        $reguser->email = $request->email;
        $reguser->phone = $request->phone;
        $reguser->address = $request->address;
        $reguser->street = $request->street;
        $reguser->building = $request->building;
        $reguser->zone = $request->zone;
        $reguser->save();

        if($request->password && $request->password != $request->conf_password){
            return redirect()->back() ->with('error' , "Password does not match" );
        }
  
        if($request->password && $request->password == $request->conf_password){
            $reguser->password = $request->password;
            $reguser->save();
        }
        
     
        return redirect()->back() ->with('success' , "User updated successfully" );

        // return redirect()->route('regusers.index')
                        // ->with('success','User updated successfully');
    }

    public function destroy(regusers $reguser)
    {
        $reguser->delete();
  
        return redirect()->route('regusers.index')
                        ->with('success','User deleted successfully');
    }




    public function filter(Request $request){

        $filters = $request->all();
        $filtered_regusers = regusers::all();

        if($request->name){

            $all_words = explode(' ' , $request->name );
            // dd($all_words);
            if($all_words){
            
                $filtered_regusers = regusers::where(function ($user) use ($all_words) {
                    foreach($all_words as $word){
                        $user->where('full_name','like', '%' . $word . '%');
                        }
                    })->get();
            }   

            
            // $filtered_regusers = $filtered_regusers->where('full_name' , $request->name );
        }
        
        if($request->email){
            $filtered_regusers = $filtered_regusers->where('email' , $request->email );
        }
        
        if($request->phone){
            $filtered_regusers = $filtered_regusers->where('phone' , $request->phone );
        }

        return view('regusers.index',compact('filtered_regusers' , 'filters' ));

    }



    public function viewOrders($user_id){
        $customer = regusers::findOrFail($user_id);

        return view('regusers.user_orders' , compact('customer'));
    }

    public function exportCustomers($ids)
    {
        $ids = json_decode($ids);

        $customers = regusers::whereIn('id' , $ids )->get();

        return Excel::download(new CustomerExport($customers), 'customers.xlsx');
    }





    public function resetEmailPassword(){
        if(isset($_SESSION['logged_in'])){
            return redirect()->back() ->with('error' , "Already Logged in" );
        }


        return view('user_auth.reset_email');
    }


    public function sentPasswordResetLink(Request $request){
        
        if(isset($_SESSION['logged_in'])){
            return redirect('/')->with('error' , "Already Logged in" );
        }

        $customer = regusers::where('email' , $request->email)->first();

        if($customer){

            $subject = 'VQuick Password Reset Email';

            $token = md5(Str::random(6));

            $customer->reset_password_token =  $token;
            $customer->save();
            
            $data['msg'] = 'Dear '. $customer->full_name .' <br> You have requested for password  reset. <a href="'.route('password.reset.token' ,[ $request->email , $token ]).'">Click Here</a> to reset password' ;
    
            \Mail::send('emails.text_email', $data, function ($message) use ($customer , $subject) {
                // $message->from($contact_email, 'Test');
                // $message->sender($email, $name);
                $message->to($customer->email, $customer->full_name );
                $message->subject($subject);
            });
    
            if (\Mail::failures()) {
                // return response showing failed emails
                return redirect()->back()->with('error' , 'Some Problem Occurred') ;
            }
            return redirect('/') ->with('success' , 'Password Reset Email Sent Successfully' ) ;


        }else{

            return redirect()->back()->with('error' , "User with that email doesn't exist" ) ;
        }
    }


    public function resetPassword($email , $token){
        // dd($email , $token);

        if(isset($_SESSION['logged_in'])){
            return redirect('/')->with('error' , "Already Logged in" );
        }

        $customer = regusers::where('email' , $email)->first();

        if( is_null($customer->reset_password_token)){
            return redirect('/')->with('error' , "Some Problem Occurred" );
        }

        if($customer && $customer->reset_password_token == $token ){
                // $customer->reset_password_token = null;
                // $customer->save();
                return view('user_auth.new_password' , compact('email'));
        }else{
            return redirect('/reset/password')->with('error' , "User with that email doesn't exist" );

        }
    }

    public function sentNewPassword(Request $request){
        
        
        if(isset($_SESSION['logged_in'])){
            return redirect('/')->with('error' , "Already Logged in" );
        }

        $customer = regusers::where('email' , $request->email)->first();


        if($customer){
            if($request->password == $request->conf_password){

                $customer->reset_password_token = null;
                $customer->password = $request->password;
                
                $customer->save();

                return redirect('/userlogin')->with('success' , 'Password changed successfully please login again');
                
            }else{
            return redirect() -> bacK() ->with('error' , "Password does not match" );
        }

        }else{
            return redirect('/reset/password')->with('error' , "User with that email doesn't exist" );

        }
    }



    public function guestSignup(Request $request){
        // dd($request->all());
        
        $carts = carts::where('session', session()->getId() )->where('is_approved', '0')->get();

        foreach($carts as $cart){
            if($cart->product && $cart->product->stock_available < $cart->quantity ){
                return redirect()->back()->with('error' , $cart->name .' stock is not enough');
            }
        }

        $total_price = $request->total_price;
        $payment_method = $request->payment_method;
        
        return view('guest_signup' , compact('total_price' , 'payment_method'));
    }



    public function guestSignupStore(Request $request){


        // dd($request->all());
        $guest_check = regusers::where('email' , $request->email)->first();

        // firstOrCreate
        $guest = regusers::updateOrCreate(
            ['email' => $request->email] ,
            [
            'email' => $request->email, 
            'is_guest' => 1, 
            'password' =>  isset($guest_check) ? $guest_check->password : md5(Str::random(10)) , 
            'full_name' => $request->full_name, 
            'phone' => $request->phone, 
            'address' => $request->address, 
            'zone' => $request->zone, 
            'street' => $request->street, 
            'building' => $request->building, 
        ]);

        if($guest){
            return redirect('/order-cart?total_price='.$request->total_price.'&payment_method='.$request->payment_method.' &guest_purchase=1&id='.$guest->id);
        }else{
            return redirect()->back()->with('error' , 'Signup Failed');
        }



    }



}
