<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\shippings;
class ShippingsController extends Controller
{
    public function index()
    {
        $shippings = shippings::latest()->simplePaginate(5);
        return view('shippings.index',compact('shippings'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('shippings.create');
    }
  
    public function store(Request $request)
    {

        
        $request->validate([
            'fee' => 'required|numeric'
        ]);

        $shippings= new shippings();
        $shippings->fee = request('fee');
        $shippings->save();
        return redirect()->route('shippings.index')
                        ->with('success','shipping created successfully.');
    }

    public function show(shippings $shipping)
    {
        return view('shippings.show',compact('shipping'));
    }
   
    public function edit(shippings $shipping)
    {
        return view('shippings.edit',compact('shipping'));
    }
    
    public function update(Request $request, shippings $shipping)
    {
  
        $request->validate([
            'fee' => 'required|numeric'
        ]);
        
        $shipping->update($request->all());
  
        return redirect()->route('shippings.index')
                        ->with('success','shipping updated successfully');
    }

    public function destroy(shippings $shipping)
    {
        $shipping->delete();
  
        return redirect()->route('shippings.index')
                        ->with('success','shipping deleted successfully');
    }
}
