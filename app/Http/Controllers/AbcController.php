<?php

namespace App\Http\Controllers;

use App\products;
use Carbon\Carbon;
use App\orderstatuses;
use Illuminate\Http\Request;

class AbcController extends Controller
{
    public function index()
    {

        $products = products::where('stock_available' , '<' , 3 )->get();
        $orderstatuses = orderstatuses::orderBy('created_at' , 'desc')-> where('status', 'pending' )->get(); 

        return view('homes.index' , compact('products' , 'orderstatuses'));
    }
}
