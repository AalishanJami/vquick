<?php

namespace App\Http\Controllers;

use App\User;
use App\carts;
use App\products;
use App\regusers;
use App\shippings;
use App\contactuses;
use App\Order_History;
use App\orderstatuses;
use App\Jobs\SendEmail;
use App\Exports\CartsExport;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use App\Jobs\SendCustomerEmail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrdersProductsExport;
use Omnipay\Omnipay;
use App\Payment;

use Sadad\Laravel\Facade\Sadad;


/** Paypal Details classes **/
// use PayPal\Rest\ApiContext;
// use PayPal\Auth\OAuthTokenCredential;
// use PayPal\Api\Amount;
// use PayPal\Api\Details;
// use PayPal\Api\Item;
// use PayPal\Api\ItemList;
// use PayPal\Api\Payer;
// use PayPal\Api\Payment;
// use PayPal\Api\RedirectUrls;
// use PayPal\Api\PaymentExecution;
// use PayPal\Api\Transaction;
// use PayPal\Exception\PayPalConnectionException;





class OrderstatusesController extends Controller
{
    public $gateway;
 
    public function __construct()
    {
        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId(env('PAYPAL_CLIENT_ID'));
        // $this->gateway->setSecret(env('PAYPAL_CLIENT_SECRET'));
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->setClientId('AVxP-jU0lumxBdqgWJGwc-GydC3vPpRx2X3SrYY_j_b7gsSRy0DCflw6a7w7YA-7YTgxJMpJqAx68H5R');
        $this->gateway->setSecret('EP0YEH1RwnV1qIw8Vgn21K9vaSEIk7gh1Ybp54kayQwUygeJQoA4-SqWCGIIm3f4B9mrNHVr0ZH1Q28v');
        $this->gateway->setTestMode(true); //set it to 'false' when go live

    }



    public function index()
    {
        $orderstatuses = orderstatuses::latest()->simplePaginate(15);
        return view('orderstatuses.index',compact('orderstatuses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function search(Request $request)
    {

        // dd($request->all());
        $order_number = request('search_order_number');
        $payment_method = request('search_payment_status');
        $status = request('status');
        $name = request('name');

        $filtered_orders = orderstatuses::all();
        $filters = $request->all();

          
        if($filters['lower_date'] && !$filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::whereDate('created_at', '>=',  $filters['lower_date'])->get();

        }elseif(!$filters['lower_date'] && $filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::whereDate('created_at', '<=',  $filters['upper_date'])->get();

        }elseif($filters['lower_date'] && $filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::where('created_at', '>=', $filters['lower_date'] )->whereDate('created_at' ,'<=', $filters['upper_date'] )->get();

        }else{
            $filtered_orders = orderstatuses::all();
        }



        if($request->name){
            $all_words = explode(' ' , $request->name );

            if($all_words){
            
                $filtered_regusers = regusers::where(function ($user) use ($all_words) {
                    foreach($all_words as $word){
                        $user->where('full_name','like', '%' . $word . '%');
                        }
                    })->get();
            }   
        }
        


        if($name){
            $filtered_orders = $filtered_orders->whereIn('cid', $filtered_regusers->pluck('id')->toArray() );
        }

        if($order_number){
            $filtered_orders = $filtered_orders->where('id', $order_number);
        }
        if($payment_method){
            $filtered_orders = $filtered_orders->where('payment_method', $payment_method);
        }
        if($status){
            $filtered_orders = $filtered_orders->where('status', $status);
        }

     
        // if($order_number ==null && $status== null ) {
        //     $orderstatuses = orderstatuses::all()->where('payment_method', $payment_method);
        // }
        // else if($payment_method ==null && $status== null) {
        //     $orderstatuses = orderstatuses::all()->where('id', $order_number);
        // }
        // else {
        //     $orderstatuses = orderstatuses::all()->where('status', $status);
        // }
        return view('orderstatuses.index',compact('filtered_orders' , 'filters'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function search1(Request $request)
    {

        // dd($request->all());

        $orderstatus = orderstatuses::findOrFail(request('oid'));


        $product_name = request('product_name');
        $oid = request('oid');
        $carts= new carts();
        $carts = carts::all()->where('is_approved', '1')->where('order_id', $oid)->where('name', $product_name);
        
        return view('orderstatuses.show',compact('orderstatuses','carts' , 'product_name' , 'orderstatus'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function show(orderstatuses $orderstatus)
    {


        $carts= new carts();
        $carts = carts::with('product')->where('is_approved', '1')->where('order_id', $orderstatus->id)->get();

        $products = products::whereIn('name' , $carts->pluck('name')->toArray() )->get() ;
        // dd($carts);

        return view('orderstatuses.show',compact('orderstatuses','carts' ,'products' ,'orderstatus'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function SadadPayment(Request $request){

        // $secretkey = '5rQRZSC4t7Ze42Cb'; // Secret key of the merchant
        // $sadadid = 7648426; // Sadad id of the merchant
        // $registered_domain = 'www.some.com'; // Registered domain of the merchant
        // $type = 'sandbox'; // 'live' - For live SDK,'sandbox' - For Sandbox

        // $wspath = 'https://sandboxapi.sadadqa.com/api/';

        // $requestdata = array();
        // $requestip = $_SERVER['REMOTE_ADDR']; // Ip of app sdk user
        // // dd($requestip);
        // $requestdata = '{
        //             "sadadId": "7803971",
        //             "secretKey": "BCYJENm9z6hxyeaD",
        //             "domain": "www.vquick.qa" 
        //         }';


        $dateTime =  \Carbon\Carbon::now();

        $results = Sadad::request(
            '/sadad-payment-success',          //required
            10,                                  //required
            $dateTime,                           //required format=>m/d/Y g:i:s a
            '10001'                    
        );

        
        // $results = Sadad::request(
        //     "example.com/testVerify.php",          //required
        //     1000,                                  //required
        //     'YOUR_DATE'                            //required format=>m/d/Y g:i:s a
        // );
        dd($results);

        // save $results['Authority'] for verifying step
        Sadad::redirect(); // redirect user to sadad
        
        // after that verify transaction by that $results['Authority']
        Sadad::verify('token');
        
    }

    public function SadadPaymentSuccess(Request $request){    
        dd($request->all());
    }

    public function store(Request $request)
    {
        // $carts = carts::where('cid', $_SESSION['id'])->where('is_approved', '0')->get();

        // foreach($carts as $cart){

        //     $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;

        //     dump($cart->quantity , $cart->product->stock_available);
        //     if($cart->product && $cart->product->stock_available < $cart->quantity ){
        //     }
        // }

        
        if(!isset($_SESSION['id'] ) && !isset($request->guest_purchase) ){
            return redirect('userlogin')->with('error' , 'You must login to proceed');
            // return redirect()->back()->with('error' , 'You must login to proceed');
        }
        
        // dd($request->all());

        if(!isset($_SESSION['id'] ) && isset($request->guest_purchase) ){
            $carts = carts::where('session', session()->getId() )->where('is_approved', '0')->get();

            foreach($carts as $cart){
                $cart->cid = $request->id;
                $cart->save();
            }

        }elseif(isset($_SESSION['id'] )) {

            $carts = carts::where('cid', $_SESSION['id'])->where('is_approved', '0')->get();
        }else{
            return redirect('userlogin')->with('error' , 'You must login to proceed');
        }

        
        foreach($carts as $cart){
            if($cart->product && $cart->product->stock_available < $cart->quantity ){
                return redirect()->back()->with('error' , $cart->name .' stock is not enough');
            }
        }

        // dd($this->gateway->getDefaultParameters());

        // dd($request->all() , (int)number_format($request->total_price , 2));

        if($request->payment_method == 'PayPal'){



        try {
            $response = $this->gateway->purchase(array(
                'amount' =>  (int)number_format($request->total_price , 2) ,
                'currency' => 'USD' ,
                'returnUrl' => url('paymentsuccess') . '?id='.$request->id ,
                // 'returnUrl' => '/paymentsuccess?user_id=111&order_id=999',
                'cancelUrl' => url('paymenterror'),
            ))->send();
      
            if ($response->isRedirect()) {
                $response->redirect(); // this will automatically forward the customer
            } else {
                // not successful

                return $response->getMessage();
            }
        } catch(Exception $e) {
            
            return $e->getMessage();
        }
    
        }else{


        // dd('asd');
        // $shipping = shippings::first();

        // $total_price = $shipping->fee + request('total_price'); 

        $orderstatuses= new orderstatuses();
        // $orderstatuses->total_price = request('total_price');

        $last_order = orderstatuses::all() -> last();
    

        // $orderstatuses->total_price = $total_price;
        $orderstatuses->total_price = request('total_price');
        $orderstatuses->cid = request('id');
        $orderstatuses->status = 'pending';
        $orderstatuses->payment_method = 'pending';
        $orderstatuses->save();
        $last_id=$orderstatuses->id;
        
        
        $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->get();
        
        foreach($carts as $cart){
            if($cart->product){
                $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
                $cart->product->save();
                $cart->available = $cart->product->stock_available;
                $cart->save();
            }
        }

        
        $carts= new carts();
        $carts = carts::all();
        $carts->order_id = $last_id;
        $carts->is_approved = '1';

        
        
        
        $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->update(array('order_id' => $carts->order_id,'is_approved' => '1'));

        $carts = carts::all()->where('is_approved', '0')->where('cid', $orderstatuses->cid);
        $shippings = shippings::all();

        // $customer = regusers::findOrFail ($orderstatuses->cid );
        $customer = regusers::where('id' ,$orderstatuses->cid )->get();


        $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when weve shipped it.<br>';

        $subject = 'Your vQuick Order is submitted';

        dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatuses ));
        // dispatch(new SendEmail($customer , $message , $subject , 'emails.general' ));
        
        $admins = User::all();
        
        $subject = 'A new Order is pending';
        $message = 'Dear Admin <br> A new Order is pending. <br> ';
        // <a href="'.url('/').'/orderstatuses/'.$orderstatuses->id.'">View</a> 

        dispatch(new SendCustomerEmail($admins , $message , $subject , $orderstatuses ));
        // dispatch(new SendEmail($admins , $message , $subject , $orderstatuses ));
        
        // if(!isset($_SESSION)) 
        // { 
        //     session_start(); 
        // } 
        // $_SESSION['message'] = "Your Your Order has been submitted. Thank you";
        
        if(isset($_SESSION['id'] )){
            return redirect('order-history')->with('success' , 'Your Your Order has been submitted. Thank you');
        }else{
            return redirect('/')->with('success' , 'Your Your Order has been submitted. Thank you');
        }


        }

        return redirect('/')->with('error' , 'No Payment Method selected');



        // return view('checkout',compact('carts','shippings'));
    }




    public function payment_success(Request $request)
    {

        // dd($request->all());
        // Once the transaction has been approved, we need to complete it.
        if ($request->input('paymentId') && $request->input('PayerID'))
        {
            $transaction = $this->gateway->completePurchase(array(
                'payer_id'             => $request->input('PayerID'),
                'transactionReference' => $request->input('paymentId'),
            ));
            $response = $transaction->send();
         
            if ($response->isSuccessful())
            {
                // The customer has successfully paid.
                $arr_body = $response->getData();
                // dd($response , $arr_body['id']);
                // Insert transaction data into the database


                $orderstatuses= new orderstatuses();
                // $orderstatuses->total_price = request('total_price');
        
                $last_order = orderstatuses::all() -> last();
            
        
                // $orderstatuses->total_price = $total_price;
                $orderstatuses->total_price = $arr_body['transactions'][0]['amount']['total'];
                $orderstatuses->cid = request('id');
                $orderstatuses->status = 'pending';
                $orderstatuses->payment_method = 'Paid VIA PayPal';
                $orderstatuses->save();
                $last_id=$orderstatuses->id;
                
                $isPaymentExist = Payment::where('payment_id', $arr_body['id'])->first();
         
                if(!$isPaymentExist)
                {
                    $payment = new Payment;
                    $payment->payment_id = $arr_body['id'];
                    $payment->payer_id = $arr_body['payer']['payer_info']['payer_id'];
                    $payment->payer_email = $arr_body['payer']['payer_info']['email'];
                    $payment->amount = $arr_body['transactions'][0]['amount']['total'];
                    $payment->currency = 'QAR';
                    $payment->customer_id = $request->id;
                    $payment->order_id = $orderstatuses->id;
                    $payment->payment_status = $arr_body['state'];
                    $payment->response = json_encode($arr_body);
                    $payment->save();
                }
                
                $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->get();
                
                foreach($carts as $cart){
                    if($cart->product){
                        $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
                        $cart->product->save();
                        $cart->available = $cart->product->stock_available;
                        $cart->save();
                    }
                }
        
                
                $carts= new carts();
                $carts = carts::all();
                $carts->order_id = $last_id;
                $carts->is_approved = '1';
        
                
                
                $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->update(array('order_id' => $carts->order_id,'is_approved' => '1'));
        
                $carts = carts::all()->where('is_approved', '0')->where('cid', $orderstatuses->cid);
                $shippings = shippings::all();
        
                $customer = regusers::where('id' ,$orderstatuses->cid )->get();
        
                $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when weve shipped it.<br>';
        
                $subject = 'Your vQuick Order is submitted';
        
                dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatuses ));
                
                $admins = User::all();
                
                $subject = 'A new Order is pending';
                $message = 'Dear Admin <br> A new Order is pending. <br> ';
        
                dispatch(new SendCustomerEmail($admins , $message , $subject , $orderstatuses ));
                
                if(isset($_SESSION['id'] )){
                    return redirect('order-history')->with('success' , "Payment is successful. Your transaction id is: ". $arr_body['id']);
                }else{
                    return redirect('/')->with('success' , "Payment is successful. Your transaction id is: ". $arr_body['id']);
                }
         
                // return "Payment is successful. Your transaction id is: ". $arr_body['id'];
            } else {
                return $response->getMessage();
            }
        } else {
            return 'Transaction is declined';
        }
    }
 
    public function payment_error()
    {
        return 'User is canceled the payment.';
    }



    public function edit(orderstatuses $orderstatus)
    {
        return view('orderstatuses.edit',compact('orderstatus'));
    }
    
    public function update(Request $request, orderstatuses $orderstatus)
    {

        // dd($request->all());
        // $orderstatus->update($request->all());
  
        if($request->status == 'delivered'){
            
            $orderstatus->payment_method = $request->payment_method;
        }
        $orderstatus->status = $request->status;
        $orderstatus->save();

        // dd($request->all() , $orderstatus);


        if( $orderstatus) {

            // dd($orderstatus->carts);
            $customer = $orderstatus->customer;

            // $data['msg'] = 'Dear '.$customer->full_name .'<br>Your Order Status is now '.$request->status;

            if(!is_null(shippings::first())){
                $data['shipping'] =  shippings::first() ;
            }
            
            if($request->status == 'cancelled'){
                
                foreach($orderstatus->carts as $cart){
                    // dump($cart->product);
        
                    $cart->product->stock_available = $cart->quantity + $cart->product->stock_available;
                    $cart->available = $cart->quantity + $cart->product->stock_available;
                    $cart->save();
                    $cart->product->save();
                }

                $message = 'Dear '. $orderstatus->customer->full_name.', We have cancelled your order # ' . $orderstatus->id .'.<br></p><p style="color: #242424;">Please Contact for further details.<br>';
            }else{
                $message = 'Dear '. $orderstatus->customer->full_name.', we have received order # ' . $orderstatus->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when weve shipped it.<br>';
            }

            $subject = 'You Order Status is '. ucwords($orderstatus->status)  ;
            
            $customer = regusers::where('id' ,$orderstatus->cid )->get();
 
            dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatus ));

        }

        return redirect()->route('orderstatuses.index')
                        ->with('success','Order updated successfully');
    }

    public function destroy(orderstatuses $orderstatus)
    {

        // dd($orderstatus);

        foreach($orderstatus->carts as $cart){
            // dump($cart->product);

            $cart->product->stock_available = $cart->quantity + $cart->product->stock_available;
            $cart->product->save();
        }
        $orderstatus->delete();
  
        return redirect()->back()
        ->with('success','Order deleted successfully');

        // return redirect()->route('orderstatuses.index')
        //                 ->with('success','Order deleted successfully');
    }


    public function exportOrders($ids)
    {
        $ids = json_decode($ids);

        $orders = orderstatuses::whereIn('id' , $ids )->get();

        return Excel::download(new OrdersExport($orders), 'orders.xlsx');
    }



    public function exportOrdersProducts($ids)
    {
        $ids = json_decode($ids);

        $orders = orderstatuses::whereIn('id' , $ids )->get();

        $sold_products = collect();

        foreach($orders as $order){
            // dump($order->carts->pluck('name')->toArray() , $order->carts->pluck('quantity')->toArray());
            foreach($order->carts as $cart){
                if($sold_products->contains( 'name' , $cart->name)){
                    
                    $sold_products->where( 'name' , $cart->name )->first()->total_quantity += $cart->quantity;
                }else{
                    $sold_products->push( $cart->product );
                    $sold_products->last()->total_quantity = $cart->quantity;
                }
                }
        }

        foreach($sold_products as $product){
            $product->total_price = $product->total_quantity * $product->price;
        }
        // dd($sold_products);
        
        return Excel::download(new OrdersProductsExport($sold_products), 'products_report.xlsx');
    }

    public function exportOrder($id)
    {
        // $ids = json_decode($ids);

        $order = orderstatuses::find( $id );

        return Excel::download(new CartsExport($order), 'order_details.xlsx');
    }


}
