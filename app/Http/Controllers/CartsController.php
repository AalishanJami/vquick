<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\categories;
use App\carts;
use App\shippings;
use App\orderstatuses;

class CartsController extends Controller

{
    public function store(Request $request)
    {

        // dd($request->all());

        $request->validate([
            'name' => 'required',
        ]);

        $product = products::findOrFail($request->pid);

        
        if($request->quantity == 0 ){
            return redirect()->back()->with('error' , 'Please select quantity');
        }
        if($product->stock_available < $request->quantity ){
            return redirect()->back()->with('error' , 'You have exceeded the allowed quantity of these products');
        }
        
        $all_products_count = array_sum(carts::where('session' , session()->getId() )->where('is_approved' , 0)->get()->pluck('quantity')->toArray());

        // dd($all_products_count , isset($category->restriction_level) && $quantity > $cart->quantity && $category->restriction_level <= $all_products_count ) ;

        // if( isset($category->restriction_level) && $quantity > $cart->quantity && $category->restriction_level <= $all_products_count ){
        //     return response()->json(['message' => 'Category restriction exceeded'  , 'error_code' => 1 ] , 500);

        // }

        // dd($request->all());
        // dd(session()->getId());

        $category = categories::where('name', request('category'))->first() ;
        // $categories = categories::all()->where('name', request('category'));
        // dd($category);

        // foreach($categories as $category) {
        //     $restriction_level = $category->restriction_level;
        // }
        
        $carts1 = carts::all()->where('is_approved', '0')->where('session', session()->getId() )->where('category', request('category'));
        
        

        $restriction_level = 0; 
        $count=0;

        foreach($carts1 as $cart) {
            $count+=$cart->quantity;

            if($cart->name == $product->name && $cart->description == $product->description ){
                
                if( (isset($category->restriction_level) && $category->restriction_level >= $cart->quantity + $request->quantity && $category->restriction_level <=$all_products_count ) || !isset($category->restriction_level) || isset($category->restriction_level) && $category->restriction_level == 0  ) {
                    
                    $cart->quantity = $cart->quantity + $request->quantity;
                    $cart->price = ($cart->product->price * $cart->quantity);
                    $cart->save();
                    session()->put('added-pid' , $request->pid);
                    return redirect()->back()->with('success' , 'Product added successfully') ;
                }else{
                    return redirect()->back()->with('error' , 'You cannot add more products from this category due to products') ;
                }
    
            }
        }
        
        // dd($_SESSION['id'] , request('id'));
        // $carts= new carts();
        // $carts->category = request('category');
        // $carts->cid = request('id');

        

        $carts= new carts();
        $carts->image = request('image');
        $carts->quantity = request('quantity');
        $count += $carts->quantity;
        $available = request('stock_available');
        $carts->available = $available- $carts->quantity;
        $carts->description = request('description');
        $carts->name = request('name');
        $carts->category = request('category');
        $pid = request('pid');
        $carts->price = request('price')*$carts->quantity;
        $carts->is_approved = '0';
        $carts->cid = request('id') ?? null;
        $carts->order_id = '0';
        $carts->session = session()->getId();

        if( (isset($category->restriction_level) && $category->restriction_level >= $count) || !isset($category->restriction_level) || isset($category->restriction_level) && $category->restriction_level == 0  ) {
            $carts->save();
            session()->put('added-pid' , $pid);
            
            // dd($carts);
            // products::where('id', $pid)->update(array('stock_available' => $carts->available));
            // $carts = carts::all()->where('is_approved', '0')->where('cid', $carts->cid);
            // $shippings = shippings::all();
            // return view('checkout',compact('carts','shippings'));
            return redirect()->back()->with('success' , 'Product added successfully') ;

        }
        else {
            // products::where('id', $pid)->update(array('stock_available' => $carts->available));
            $carts = carts::all()->where('is_approved', '0')->where('cid', $carts->cid);
            $shippings = shippings::all();
            if(!isset($_SESSION)) 
            { 
                session_start(); 
            }
            // $_SESSION['message'] = "You cannot add more products from this category due to products";
            return redirect()->back()->with('error' , 'You cannot add more products from this category due to products') ;

            // return view('checkout',compact('carts','shippings'));
        }
    }

    public function destroy(carts $cart)
    {

        // $product_price = $cart->price/$cart->quantity;

        // $product = products::where( 'image' , $cart->image)->where( 'name' , $cart->name)->where( 'description' , $cart->description)->where( 'price' , $product_price)->first(); ;

        // $product->stock_available = $product->stock_available + $cart->quantity;
        // $product->save();
        $cart->delete();
        return redirect()->route('first.index')
                        ->with('success','Your Order has been submitted. Thank you.');
    }


    
    
    
    public function updateCartQuantityAjax($cart_id , $quantity ){
        


        // dd($cart_id , $quantity);

        if($quantity > 0){

            $cart = carts::findOrFail($cart_id);
            
            // dd($cart);

            // $product = products::where( 'image' , $cart->image)->where( 'name' , $cart->name)->where( 'description' , $cart->description)->where( 'price' , $product_price)->first(); ;

            $category = categories::where('name' , $cart->category)->first();
            
            // if( $cart->quantity < $quantity && $product->stock_available < $quantity ){
            //     return response()->json(['message' => 'Availble Stock for the product is not enough' ] , 500);

            // dd(carts::where('session' , session()->getId() )->where('is_approved' , 0)->get()->pluck('quantity')->toArray() );
            // }
            $all_products_count = array_sum(carts::where('session' , session()->getId() )->where('is_approved' , 0)->where('category' , $category->name )->get()->pluck('quantity')->toArray());

            if(isset($_SESSION['id'])){
            $all_products_count1 = array_sum(carts::where('session' , '!=' ,session()->getId() )->where('cid' , $_SESSION['id'] )->where('is_approved' , 0)->where('category' , $category->name )->get()->pluck('quantity')->toArray());

            $all_products_count += $all_products_count1;
            
            }
        
            if( isset($category->restriction_level) && $quantity > $cart->quantity && $category->restriction_level <= $all_products_count ){
                return response()->json(['message' => 'Category restriction exceeded'  , 'error_code' => 1 ] , 500);

            }
            
            // dd($product_price);
            if($cart->product->stock_available < $quantity && $quantity > $cart->quantity ){
                return response()->json(['message' => 'This item stock is not enough' , 'error_code' => 2  ] , 500);
            }

            $shipping = shippings::first();

            $product_price = $cart->price/$cart->quantity;

            $cart->quantity = $quantity;
            $cart->price = $quantity * $product_price;
            $cart->save();
            // dd($cart);
            // $cart->refresh();

            // $current_carts = carts::where('session' , session()->getId() )->where('is_approved' , 0) ->get();

            // $current_carts = carts::where('session' , session()->getId() )->where('is_approved' , 0) ->get();
            // $current_carts = carts::where(function($cart){
                // $cart-> where('session' , session()->getId() )->where('id' , $_SESSION['id'] ?? null );
            // })->where('is_approved' , 0) ->get() ;
            
            $current_carts = carts::where('is_approved' , 0)->where('session' , session()->getId())->get();

            if(isset($_SESSION['id'])){
                $stored_carts = carts::where('is_approved' , 0)->where('cid' , $_SESSION['id'] )->get();
                    
                    $current_carts = $current_carts->merge($stored_carts);
            }

            $total_amount = 0;
            $sub_total = 0;
            
            if(isset($shipping)){
                $total_amount = $shipping->fee;
            }
            foreach($current_carts as $current_cart){
                $total_amount += $current_cart->price;
                $sub_total += $current_cart->price;
            }

            return response()->json(['cart' => $cart , 'total_amount' => $total_amount , 'sub_total' => $sub_total ]);
        }else{
            return response()->json(['message' => '0 quantity selected' ] , 500);

        }

    }


    public function updateCartQuantity(Request $request){
        
        if($request->quantity > 0){

            $cart = carts::findOrFail($request->cart_id);
            
            // dd($cart);
            $product_price = $cart->price/$cart->quantity;

            $product = products::where( 'image' , $cart->image)->where( 'name' , $cart->name)->where( 'description' , $cart->description)->where( 'price' , $product_price)->first(); ;

            $category = categories::where('name' , $cart->category)->first();
            
            // dd($category , $product , $cart);
            if($product->stock_available < $request->quantity ){
                return redirect()->back()->with('error' , 'Availble Stock for the product is not enough');
            }
            
            if($category->restriction_level < $request->quantity ){
                return redirect()->back()->with('error' , 'Category restriction exceeded');
            }
            
            // dd($product_price);
            $cart->quantity = $request->quantity;
            $cart->price = $request->quantity * $product_price;
            $cart->save();

            return redirect()->back()->with('success' , 'Quantity Updated successfully');
        }else{
            return redirect()->back()->with('error' , '0 quantity selected');

        }

    }

}
