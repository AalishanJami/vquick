<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderstatuses extends Model
{
    protected $fillable = [
        'total_price','cid','payment_method','status'
    ];




    public function customer(){
        return $this->belongsTo( regusers::class, 'cid');
    }

    public function carts(){
        return $this->hasMany(carts::class , 'order_id');
    }

    
}
