<?php

namespace App\Exports;

use App\Invoice;
use App\orderstatuses;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class CartsExport implements FromView
{    
    
    private $order;

    public function __construct($order = [], $headings = []){
        $this->order = $order;
        
    }

    public function view(): View
    {
        return view('exports.order_details', [
            'order' => $this->order
        ]);
    }

   
}