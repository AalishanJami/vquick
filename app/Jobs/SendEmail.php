<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class SendEmail implements ShouldQueue
{

    // 
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    // public $users;

    public $message;
    public $users;
    public $user;
    public $subject;
    public $order;

    public function __construct($users , $message , $subject , $order  )
    {
        $this->users = $users;
        $this->message = $message;
        $this->subject = $subject;
        $this->order = $order;

        // dd($order);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      
        $data['msg'] = $this->message ;
        $data['order'] = $this->order ;


        foreach($this->users as $user){
            
            $send['email'] = $user->email;
            $send['name'] = $user->name;

            // if($user->name){
                // $send['name'] = $user->name;
            // }
            // elseif($user->full_name){
            //     $send['name'] = $user->full_name;
            // }else{
            //     $send['name'] = 'User';
            // }

            
            \Mail::send( 'emails.text_email', $data, function ($message) use ($send) {
                $message->to( $send['email'] , $send['name']);
                $message->subject($this->subject);
            });
        }

        // dd('send mail');
    }
}
