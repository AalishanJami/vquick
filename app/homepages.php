<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class homepages extends Model
{
    protected $fillable = [
        'title', 'description', 'image'
    ];
}
