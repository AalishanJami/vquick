<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carts extends Model
{
    protected $fillable = [
        'image', 'name', 'description','price','quantity','available','category','cid','is_approved','order_id'
    ];



    public function order_status(){
        return $this->belongsTo(orderstatuses::class , 'order_id');
    }
    

    public function product(){
        return $this->belongsTo(products::class , 'name' , 'name' ) ;
    }
    



}