<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orderstatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderstatuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('order_number')->nullable() ;
            $table->String('total_price');
            $table->String('cid');
            $table->String('payment_method');
            $table->String('status');
            $table->timestamps();
        });

        DB::update('ALTER TABLE orderstatuses AUTO_INCREMENT = 10000');

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderstatuses');
    }
}
