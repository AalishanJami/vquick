<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Regusers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regusers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('email');
            $table->boolean('is_guest')->default(0) ;
            $table->String('password');
            $table->String('reset_password_token')->nullable() ;
            $table->String('full_name');
            $table->String('phone');
            $table->String('address');
            $table->String('zone');
            $table->String('street');
            $table->String('building');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regusers');
    }
}
