<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('image');
            $table->String('name');
            $table->String('arabic_name')->nullable();
            $table->String('restriction_level')->nullable() ;
            $table->String('is_approved')->nullable() ;
            $table->text('description')->nullable() ; 
            $table->text('description_ar')->nullable() ; 
            $table->String('is_approve')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
