<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Carts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('category_id')->nullable() ;
            $table->String('session')->nullable();
            $table->String('image');
            $table->String('name');
            $table->String('description');
            $table->String('price');
            $table->String('quantity');
            $table->String('available');
            $table->String('category');
            $table->String('is_approved');
            $table->String('cid')->nullable();
            $table->String('order_id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
