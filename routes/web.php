<?php

use App\carts;
Route::get('/', function () {
    return view('welcome');
});
Route::get('/contact', function () {
    return view('contact');
})->name('contact') ; 


Route::get('/about', function () {
    return view('about');
})->name('about') ;


  //  language change
  Route::get('change-language/{lang}' , function($lang){
    
    App::setLocale($lang);
    // if(App::isLocale('ar')){
    //     dd(App::getLocale());
    // }
    // \App::setLocale($lang);

    // dd(App::getLocale());
    // dump($lang ,session('locale') );

    session()->put('locale', $lang);

    if(session('locale') == 'ar'){

        $_SESSION['language'] = 'arabic' ;
    }else{
        $_SESSION['language'] = 'english' ;

    }
    return redirect()->back();

    
} ) ->name ('change-language') ;


Route::get('email', function () {

    $orderstatus = App\orderstatuses::find(38);
    $shipping = App\shippings::first();
    // , , 'shipping' => $shipping
    return view('emails.order_status' , ['msg' => 'Hello World' , 'order' => $orderstatus  ] );
})->name('email') ;


Route::get('/product', 'ProductsController@showProducts' )->name('product') ;

Route::get('/gallery', 'GalleriesController@showGalleryImages' )->name('gallery') ;

// Route::get('/product', function () {
//     return view('product');
// });
Route::get('/userlogin', function () {


    // dd(url()->previous());

    if(isset($_SESSION['logged_in'])){
        return redirect()->back()->with('error' , 'Already Logged in');
    }
    if(strpos( url()->previous() ,  'checkout') ){
        session()->put('checkout' , 'checkout');
    }

    return view('userlogin');

})->name('userlogin') ;
Route::get('/signup', function () {

    if(isset($_SESSION['logged_in'])){
        return redirect()->back()->with('error' , 'Already Logged in');
    }

    if(strpos( url()->previous() ,  'checkout') ){
        session()->put('checkout' , 'checkout');
    }

    return view('signup');
});


Route::get('reset/password','RegusersController@resetEmailPassword')->name('reset.password');
Route::post('send-reset-password-link','RegusersController@sentPasswordResetLink')->name('send-reset-password-link');

Route::post('save-new-password','RegusersController@sentNewPassword')->name('save-new-password');


Route::get('password/reset/{email}/token/{token}','RegusersController@resetPassword')->name('password.reset.token');


Route::get('edit-profile' , 'RegusersController@editProfile')->name('edit-profile');


if(!isset($_SESSION)) 
{ 
    session_start(); 
}
use App\shippings;
use App\contactuses;
use App\orderstatuses;
use Illuminate\Support\Facades\Route;
Route::get('/checkout', function () {

    if( isset($_SESSION["logged_in"])) {
        $id = $_SESSION["id"];
    }
    //$carts = carts::where(['cid', $id],['is_approved', '0']);
    // $carts = carts::all()->where('is_approved', '0')->where('cid', $id);

   

    // if( isset($_SESSION["logged_in"])) {
    // }
        
        $session_carts = carts::where('session' , session()->getId() )->where('is_approved' , 0) ->get();

        // dd($session_carts);

        if( isset($_SESSION['id']) && $session_carts->count() > 0){

            foreach($session_carts->where('cid' , null ) as $cart){
                $cart->cid =  $_SESSION['id'];
                $cart->save();
            }

            $carts = carts::where('cid' , $id )->where('is_approved' , 0) ->get();
            
        }elseif(isset($_SESSION['id']) && $session_carts->count() == 0){
            
            $carts = carts::where('cid' , $id )->where('is_approved' , 0) ->get();
        }
        else{
            $carts = $session_carts;
        }

        // dd($carts);

        $shippings = shippings::all();
        return view('checkout',compact('carts','shippings'));

    // else{
    //     return redirect()->back()->with('error' , 'You must login before checkout');    
    // }
    
    
})->name('checkout') ;
Auth::routes();

Route::get('/home', 'AbcController@index')->middleware('auth');
Route::get('/header', 'HeaderController@index')->name('header');
Route::get('/aside', 'AsideController@index')->name('aside');
Route::resource('homes','HomeController')->middleware('auth');
Route::resource('categories','CategoriesController')->middleware('auth');
Route::get('/manage-category', 'CategoriesController@approve')->name('category');
Route::resource('products','ProductsController')->middleware('auth');

Route::get('products/exports/{ids}','ProductsController@exportProducts')->name('products.exports');

Route::get('/manage-products', 'ProductsController@approve')->name('products');
Route::resource('aboutuses','AboutusesController')->middleware('auth');
Route::resource('contactuses','ContactusesController')->middleware('auth');
Route::resource('orders','OrdersController')->middleware('auth');
Route::resource('homepages','HomepagesController')->middleware('auth');
Route::resource('galleries','GalleriesController')->middleware('auth');
Route::resource('shippings','ShippingsController')->middleware('auth');

Route::resource('regusers','RegusersController');

Route::get('guest-signup' ,'RegusersController@guestSignup' )->name('guest-signup') ;

Route::get('reguser/{user}/orders','RegusersController@viewOrders')->name('reguser-orders');


Route::get('filter-users' , 'RegusersController@filter' )->name('filter-users') ;
Route::get('/export-customers/{ids}', 'RegusersController@exportCustomers')->name('export-customers');

Route::resource('carts','CartsController');
Route::resource('productsections','ProductsectionsController')->middleware('auth');
Route::get('/user-login', 'RegusersController@login')->name('orders');
Route::get('/user-logout', 'RegusersController@logout')->name('orders');
Route::get('/contact-form', 'ContactFormController@store')->name('orders');

Route::get('/carts', 'CartsController@store')->name('carts');
Route::get('/update-cart-quantity', 'CartsController@updateCartQuantity')->name('update-cart-quantity');

Route::get('/update-cart-quantity-ajax/{cart_id}/{quantity}', 'CartsController@updateCartQuantityAjax')->name('update-cart-quantity-ajax');

Route::post('register-guest', 'RegusersController@guestSignupStore')->name('register-guest') ;

Route::get('/order-cart', 'OrderstatusesController@store')->name('orderstatuses');

Route::post('sadad-payment', 'OrderstatusesController@SadadPayment')->name('sadad-payment');

Route::get('sadad-payment-success', 'OrderstatusesController@SadadPaymentSuccess')->name('sadad-payment-success');

Route::get('paymentsuccess', 'OrderstatusesController@payment_success');
Route::get('paymenterror', 'OrderstatusesController@payment_error');

Route::get('/delete-cart', 'CartsController@delete')->name('carts');
Route::resource('admins','AdminsController')->middleware('auth');

Route::get('/contact-form','ContactFormController@create');
Route::post('/contact-form','ContactFormController@store');
Route::post('/user-logout','RegusersController@logout');
Route::post('/user-login', 'RegusersController@login');
// Route::get('/product-category', 'ProductsController@show1')->name('products');
Route::get('/product-category', 'ProductsController@showCategoryProducts')->name('products');

Route::get('/gallery-images', 'GalleriesController@showGallery')->name('gallery-images');

Route::get('/order-history', 'OrdersController@showHistory')->name('order-history');
Route::get('/order-history/{order}/details', 'OrdersController@showOrderDetails')->name('order-history.details');

Route::resource('orderstatuses','OrderstatusesController')->middleware('auth');

Route::get('/export-orders/{ids}', 'OrderstatusesController@exportOrders')->name('export-orders');

Route::get('/export-orders-products/{ids}', 'OrderstatusesController@exportOrdersProducts')->name('export-orders-products');

Route::get('/export-order/{ids}', 'OrderstatusesController@exportOrder')->name('export-order');

Route::get('/order-filter', 'OrderstatusesController@search')->name('orderstatuses');

Route::get('/product-filter', 'OrderstatusesController@search1')->name('orderstatuses');
Route::resource('first','FirstController');



// dropzone images
Route::post('/images-save', 'GalleriesController@saveDropzoneImage')->name('images-save') ;
Route::get('/images-delete/{id}', 'GalleriesController@deleteDropzoneImage');


Route::get('index', function(){
    return view('layouts.main');
});



Route::get('email-test/{id}', function($id){

    $orderstatus  = orderstatuses::find($id);
    $customer = $orderstatus->customer;

    $data['msg'] = 'Dear '.$customer->full_name .'<br>Your Order Status is now';
    $data['order'] = $orderstatus;

    $contact_email = contactuses::latest()->first()->email ;

    
    // return view('emails.order_status' , ['order' => $orderstatus , 'msg' => $data['msg']] );
    if($contact_email){

        // $view = 'emails.text_email';
        $view = 'emails.order_status';
        \Mail::send($view, $data, function ($message) use ($contact_email,$customer) {
            $message->to( 'saadanjum047@gmail.com', $customer->full_name);
            $message->from( env('MAIL_FROM_ADDRESS') , 'vQuick');
            $message->subject('Order Status Confirmation');
        });
    }

    
    

});